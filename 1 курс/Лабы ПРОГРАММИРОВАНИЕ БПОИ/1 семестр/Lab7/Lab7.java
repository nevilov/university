import static java.lang.Math.*;

public class Lab7{
    public static void main(){
        System.out.println("Начало табулирования");
        Tabulation(10,0.5,1.2,0.1);
        System.out.println("Конец табулирования");
    }
    
    public static double SqrFun(double x){
        return pow(x,1.0/5.0);
    }
    
    public static double Formula(double x, int n){
        double y = x;
        int i=0;
        do{
            i++;
            y = y - (pow(y,5) - x)/(5*pow(y,4));
            System.out.printf("y=%10.6f, x=%10.6f, i=%4d\n", y, x, i);
        }while(i<=n);
        return y;
    }
    
    public static void Tabulation(int n, double startX, double endX, double deltaX){
        double x = startX, yt,yn,e;
        
        for(;x<=endX;){
            yt = SqrFun(x);
            yn = Formula(x,n);
            e = abs(yt-yn);
            System.out.printf ("x=%10.6f, yt=%10.6f, yn=%10.6f, e=%10.6f\n", x, yt, yn,e);
            x =x + deltaX;
        }
        
    }
    
}
