public class Lab3{
    public static void main(){
        int a= 125;
        int b = 49;
        int c = (a+b)/b-a+100%b;

        printRow("a",a); printRow("b",b); printRow("c",c);
        printRow("-a", -a); printRow("a+b", a+b); printRow("a-b",a-b);
        printRow("a*b",a*b); printRow("a/b",a/b);
        printRow("a%%b",a%b);
        printRow("a++",a++); printRow("b--",b--);
    }

    private static void printRow(String nameVar, int var){
        System.out.printf(nameVar + " = " + Integer.toBinaryString(var) + "(2) = %x(16) = %o(8) = %d(10) \n",
            var,var,var);
    }
    
}