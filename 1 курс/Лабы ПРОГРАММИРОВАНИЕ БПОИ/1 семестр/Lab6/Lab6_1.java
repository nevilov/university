import static java.lang.Math.*;
import java.util.Scanner;
public class Lab6_1{
    public static void main(){
        Scanner input = new Scanner(System.in);
        double x = input.nextDouble();
        double g = 0,y,z = 0;
        if(x<=0){
            g = (3+pow((sin (2*x)),2))/(1+pow(cos(x), 2));
        }
        else{
            g = 2*(sqrt(1+2*x));
        }
        
        if(x<0){
            z = sqrt(1+ (pow(x,2))/(1+pow(x,2)));
        }
        else if(x>=0 && x<=1){
            z = 2*pow(cos (x),2);
        }
        else{
            z = sqrt(1+pow(abs(2*sin(3*x)),1.0/3.0));
        }
        y = g + z;
        System.out.println("Значение = " + y);
    }
}
    