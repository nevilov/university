public class MethodsLibrary{
    public static void main(String[] argc){
        int[] a = {-3,-4,-2,-5,-2,-5, -10};
        int[] b = {5,-3,-6540,10032,3,123};

        System.out.println("Первый Массив:");
        for(int i=0; i<a.length;i++)
            System.out.print(a[i] + " ");
        System.out.println();
        
        System.out.println("Второй Массив:");
        for(int i=0; i<b.length;i++)
            System.out.print(b[i] + " ");
        System.out.println();
        
        if(Recurs.Recurs(a, 0, a.length-1) == -1){
            System.out.println("Максимальный среди положительных элементов в первом массиве рекурсивным методом не найден");
        }
        else{
         System.out.println("Максимальный среди положительных элементов в первом массиве рекурсивным методом: "
        + Recurs.Recurs(a, 0, a.length-1));
        }   
        System.out.println("Количество отрицательных элементов в первом массиве "
        + Recurs.CntNegativeElements(a));
        
        if(Recurs.Recurs(b, 0, b.length-1) == -1){
            System.out.println("Максимальный среди положительных элементов в первом массиве рекурсивным методом не найден");
        }
        else{
        System.out.println("Максимальный среди положительных элементов в первом массиве рекурсивным методом: "
        + Recurs.Recurs(b, 0, b.length-1));
        }
        System.out.println("Количество отрицательных элементов во втором массиве "
        + Recurs.CntNegativeElements(b));
    }
}
