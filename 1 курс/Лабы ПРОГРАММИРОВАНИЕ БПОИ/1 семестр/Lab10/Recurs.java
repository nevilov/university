public class Recurs{

    public static int Recurs(int[] a, int start, int end){
        int maxElem = -1;
        if(start<=end){       
            if(start==end){
                if(a[start]>=maxElem && a[start]>0){
                    maxElem=a[start];
                }

            }
            else{
                maxElem = Recurs(a,start+1,end);
                if(a[start]>=maxElem && a[start]>0){
                    maxElem = a[start];
                }
            }
        }
        return maxElem;
    }

    public static int CntNegativeElements(int[] a){
        int cnt = 0;
        for(int i=0 ; i<a.length;++i){
            if(a[i]<0)
                cnt++;
        }
        return cnt;
    }

}