class Car{
    public int countWheels;
    public int countSeat;
    
    public String toString(){
        return String.format("Количество колес %d, количество сидений %d",countWheels,countSeat  );
    }
    
    public boolean equals(Car obj){
        if(this == obj){return true;}
        if(this != obj){return false;}
        return false;
    }
    
}