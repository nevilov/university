public class Socket
{
   private final static String SOCKET_FORMAT_STRING ="Socket: код: %d, Евро? %b, цвет: %s, мощность: %d";
    
   private int code;
   private boolean euro;
   private String color;
   private int power;
   
   public Socket(){
       code = 0;
       color = "";
       power = 0;
   }
   
   public Socket(int code, boolean euro, String color, int power){
       this.code = code;
       this.euro = euro;
       this.color = color;
       this.power = power;
   }
   
   public void SetCode(int code){this.code = code;}
   public void SetEuro(boolean euro){this.euro = euro;}
   public void SetColor(String color){this.color = color;}
   public void SetPower(int power){this.power = power;}
   
   public int GetCode(){return code;}
   public boolean GetEuro(){return euro;}
   public String GetColor(){return color;}
   public int GetPower(){return power;}
   
   public String toString(){
       return String.format(SOCKET_FORMAT_STRING, code, euro, color, power);
   }
   
   public boolean equals(Socket obj){
       if(obj == null) return false;
       if(this == obj) return true;
       
       return code == obj.code && euro == obj.euro && color == obj.color && power == obj.power; 
    }
}
