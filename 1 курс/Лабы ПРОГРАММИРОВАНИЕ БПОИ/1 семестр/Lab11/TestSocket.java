public class TestSocket{
    public static void main(){
        Socket s1 = new Socket(123, true, "Белый", 220);
        Socket s2 = s1;
        Socket s3 = new Socket();
        
        s3.SetCode(s2.GetCode());
        s3.SetEuro(s2.GetEuro());
        s3.SetColor(s2.GetColor());
        s3.SetPower(s2.GetPower());
        
        Socket s4 = new Socket(829, false, "Черный", 200);
        Socket s5 = null;
        
        System.out.println("s1 = " +s1);
        System.out.println("s2 = " +s2);
        System.out.println("s3 = " +s3);
        System.out.println("s4 = " +s4);
        System.out.println("s5 = " +s5);
        
        System.out.println("s1==s2: "+ s1.equals(s2));
        System.out.println("s1==s3: "+ s1.equals(s3));
        System.out.println("s1==s4: "+ s1.equals(s4));
        System.out.println("s2==s3: "+ s2.equals(s3));
        System.out.println("s2==s4: "+ s2.equals(s4));
        System.out.println("s3==s4: "+ s3.equals(s4)); 
        System.out.println("s3==s5: "+ s3.equals(s5));
        
        System.out.printf ("s2==(Object)s3: %s\n", s2.equals((Object)s3)); 
    }
}