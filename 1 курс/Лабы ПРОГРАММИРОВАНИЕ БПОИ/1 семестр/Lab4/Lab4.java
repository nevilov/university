import static java.lang.Math.*;
public class Lab4
{
    public static void main(){
        double a,x,k;
        int y;
        x = -3.06; y = 3;
        System.out.printf("Первые исходные данные x = %-1.2f y = %d%n", x,y);
        k = (y+11)/2;
        a = ((exp(-tan(PI*k))) - log(abs(x)))/(k*x+pow(10,5));
        System.out.println(a);
        System.out.println("Значение переменной а в формате IEEE 754: "); 
        System.out.println(Long.toBinaryString(Double.doubleToLongBits(a)));
        
        System.out.printf("Вторые исходные данные x = %-1.2f y = %d%n", x,y);
        x =0.215; y=-11;
        k = (y+11)/2;
        a = ((exp(-tan(PI*k))) - log(abs(x)))/(k*x+pow(10,5));
        System.out.println(a);
        System.out.println("Значение переменной а в формате IEEE 754: "); 
        System.out.println(Long.toBinaryString(Double.doubleToLongBits(a)));
    }
}
