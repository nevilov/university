public class Lab9 {
    public static void main(String[] argc){
        final float m = 2;

        float[][] A={
                {-3,6,8,-3},
                {3,4,-2,-6},
                {-18,20,1,-7},
                {4,-14,7,-2}
        };

        float[][] B ={
                {7,3,6,9},
                {2,3,8,3},
                {1,7,2,8},
                {1,13,24,3}
        };
        // 1
        UnderMainDiagonal(A);

        // 2
        System.out.println("Вектор, i-ый элемент которого равен 1, " +
                "если в i-омстолбце матрицы есть неотрицательныеэлементы, 0 впротивном случае. ");
        int[] vec = Vector(A);
        for(int i=0;i<vec.length;i++){
            System.out.print(vec[i] +" ");
        }
        System.out.println();

        // 3
        System.out.println("Матрица: m(A+B)");
        float[][] NewMatrix = MultiMatrix(m,A,B);
        DisplayMatrix(NewMatrix);


    }

    public static void DisplayMatrix(float[][] A){
        for(int i=0; i<A.length;i++){
            for (int j = 0; j< A[0].length;j++)
                System.out.print(A[i][j]+" ");
            System.out.println();
        }
    }

    public static void UnderMainDiagonal(float[][] A){
        int counter=0;
        for(int i=0; i<A.length;i++){
            for (int j = 0; j< A[0].length;j++){
                if(i>j && A[i][j]>0)
                    counter++;
            }
        }
        System.out.println("Число положительных элементов под глвной диагональю " + counter);
    }

    public static int[] Vector(float[][] A){
        int[] vec = new int[A[0].length];
        boolean havePosetiveNumber = false;

        for(int i=0 ; i<A.length;++i){
            for(int j =0 ; j<A[0].length;++j){
                if(A[j][i]>0){
                    havePosetiveNumber=true;
                }
            }
            if(havePosetiveNumber)
                vec[i]=1;
            else
                vec[i]=0;
            havePosetiveNumber = false;
        }
        return vec;
    }

    public static float[][] MultiMatrix(float m, float[][] A, float[][] B){
        float[][] newMatrix = new float[A.length][A[0].length];
        for(int i =0;i<A.length;i++){
            for(int j =0; j<A[0].length;++j){
                newMatrix[i][j] = A[i][j] + B[i][j];
                newMatrix[i][j] *=m;
            }
        }
        return newMatrix;
    }

}
