package com.company.Практика2;

import java.util.Comparator;

public class CompCodeAscVacationerCodeDesc implements Comparator<VoucherKey> {

    @Override
    public int compare(VoucherKey o1, VoucherKey o2) {
        int code1 = o1.getCode();
        int code2 = o2.getCode();
        String vacationerCode = o1.getVacationerCode();
        String vacationerCode2 = o2.getVacationerCode();

        if(code1<code2){
            return -1;
        }
        if(code1>code2){
            return 1;
        }

        if(vacationerCode.compareTo(vacationerCode2)>0){
            return -1;
        }
        if(vacationerCode.compareTo(vacationerCode2)<0){
            return 1;
        }

        return 0;
    }
}
