package com.company.Практика2;

public class CatalogDemo {
    public static void main(String[] args) {
        System.out.println("Оздоровительные лагеря \"Горизонт\" и \"Артек\"");
        Catalog gorizontCatalog = new Catalog("Горизонт");
        Catalog artekCatalog = new Catalog("Артек");


        System.out.println("Добавление кодов прейскуранта");
        VoucherKey v1 = new VoucherKey(1, "Отдых выходного дня", "Одноэтажный котедж");
        VoucherKey v2 = new VoucherKey(2, "Двухнедельный отдых", "Многоэтажный корпус");
        VoucherKey v3 = new VoucherKey(3, "Полная смена", "Одноэтажный котедж");
        VoucherKey v4 = new VoucherKey(4, "Отдых выходного дня", "Многоэтажный корпус");
        VoucherKey v5 = new VoucherKey(5, "Отдых выходного дня", "Двухэтажный котедж");
        VoucherKey v6 = new VoucherKey(6, "Полная смена", "Двухэтажный котедж");
        VoucherKey v7 = new VoucherKey(7, "Отдых выходного дня", "Одноэтажный котедж");
        VoucherKey v8 = new VoucherKey(8, "Полная смена", "Многоэтажный корпус");
        VoucherKey v9 = new VoucherKey(9, "Двухнедельный отдых", "Одноэтажный котедж");
        VoucherKey v10 = new VoucherKey(10, "Полная смена", "Двухэтажный котедж");

        System.out.println("Добавление прейскурантов в каталог Горизонта");
        gorizontCatalog.addVoucher(v1, 1000);
        gorizontCatalog.addVoucher(v2, 1500);
        gorizontCatalog.addVoucher(v3, 1499);
        gorizontCatalog.addVoucher(v4, 1000);
        gorizontCatalog.addVoucher(v5, 1000);
        gorizontCatalog.addVoucher(new VoucherKey(1, "Полная смена", "Одноэтажный котетдж"), 1000);
        gorizontCatalog.addVoucher(new VoucherKey(1, "Полная смена", "Многоэтажный корпус"), 1200);

        gorizontCatalog.putCatalog();

        System.out.println("Попытка добавить прейскурант в каталог, ключ которого уже присутствует в каталоге");
        gorizontCatalog.addVoucher(v1, 1500);

        System.out.println("Запрос на обновление");
        gorizontCatalog.updateCatalog(v1, 1200);
        gorizontCatalog.updateCatalog(v4, 1500);

        gorizontCatalog.putCatalog();

        System.out.println("Запросы на выборку");
        gorizontCatalog.selectVoucherData(1).putCatalog();
        gorizontCatalog.selectVoucherData("Одноэтажный котедж").sortTypeHomeAscVacationerCodeDesc().putCatalog();
        System.out.println(
        "Сумма всех цен на двухэтажный котетдж " +
        gorizontCatalog.selectSumPriceByTypeHome("Многоэтажный корпус")
        );

        System.out.println("Запросы на удаление");
        gorizontCatalog.deleteVoucher(v2, 1500);
        gorizontCatalog.deleteVoucher(v6, 5);
        gorizontCatalog.putCatalog();


        System.out.println("Добавление прейскурантов в каталог Артека");
        artekCatalog.addVoucher(v5, 1000);
        artekCatalog.addVoucher(v6, 1200);
        artekCatalog.addVoucher(v7, 1399);
        artekCatalog.addVoucher(v8, 1000);
        artekCatalog.addVoucher(v9, 1200);
        artekCatalog.addVoucher(v10, 1400);

        artekCatalog.sortTypeHomeAscVacationerCodeDesc().putCatalog();
        artekCatalog.putCatalog();
        System.out.println("Запрос на обновление");
        artekCatalog.updateCatalog(v7, 2000);
        artekCatalog.updateCatalog(v9, 2000);

        System.out.println("Запросы на удаление");
        artekCatalog.deleteVoucher(v2, 1500);
        artekCatalog.deleteVoucher(v6, 5);
        artekCatalog.putCatalog();

        artekCatalog.putCatalog();

        artekCatalog.sortCodeAscVacationerCodeDesc().putCatalog();

    }
}
