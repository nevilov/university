package com.company.Практика2;

import java.util.Comparator;

public class CompTypeHomeAscVacationerCodeDesc implements Comparator<VoucherKey> {
    @Override
    public int compare(VoucherKey o1, VoucherKey o2) {
        String typeHome1 = o1.getTypeHome();
        String typeHome2 = o2.getTypeHome();
        String vacationerCode1 = o1.getVacationerCode();
        String vacationerCode2 = o2.getVacationerCode();

        if(typeHome1.compareTo(typeHome2)<0){
            return -1;
        }
        if(typeHome1.compareTo(typeHome2)>0){
            return 1;
        }

        if(vacationerCode1.compareTo(vacationerCode2)>0){
            return -1;
        }
        if(vacationerCode1.compareTo(vacationerCode2)<0){
            return 1;
        }

        return 0;
    }
}
