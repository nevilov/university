package com.company.Практика2;

import java.util.*;

public class Catalog {
    private final static String CATALOG_FORMAT_STRING = "| Каталог %s | Количество %d |";
    private String name;
    private Map<VoucherKey, Double> catalog;

    public Catalog(Map<VoucherKey, Double> catalog, String name){
        this.name = name;
        this.catalog = catalog;
    }

    public Catalog(String name){
        this.name = name;
        catalog = new TreeMap<>();
    }

    public Catalog(String name, Comparator comparator){
        this.name = name;
        catalog = new TreeMap<>(comparator);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format(CATALOG_FORMAT_STRING, name, catalog.size());
    }

    public int getSize(){
        return catalog.size();
    }

    public boolean addVoucher(VoucherKey voucherKey, double price){
        if(catalog.containsKey(voucherKey)){
            System.out.println("Невозможно добавить прейскурант, так как в каталоге уже содержиться прейскурант с таким же ключем");
            return false;
        }

        catalog.put(voucherKey, price);
        return true;
    }

    public boolean deleteVoucher(VoucherKey voucherKey, double price){
        if(!catalog.containsKey(voucherKey)){
            System.out.println("Невозможно удалить прейскурант, так как в каталог не содержиться прейскуранта с таким ключем");
            return false;
        }

        catalog.remove(voucherKey, price);
        return true;
    }

    public boolean deleteVoucher(int code){
        Set<Map.Entry<VoucherKey, Double>> setCatalog = catalog.entrySet();
        Iterator<Map.Entry<VoucherKey,Double>> iterator = setCatalog.iterator();
        while(iterator.hasNext()){
            Map.Entry<VoucherKey, Double> keyVal = iterator.next();
            if(keyVal.getKey().getCode() == code){
                catalog.remove(keyVal);
            }
        }
        return true;
    }

    public boolean updateCatalog(VoucherKey voucherKey, double price){
        if(!catalog.containsKey(voucherKey)){
            System.out.println("Невозможно обновить прейскурант, так как каталог не содержит прейскурант с данным ключем");
            return false;
        }
        catalog.put(voucherKey, price);
        return true;
    }

    public Catalog selectVoucherData(int code){
        StringBuilder name = new StringBuilder(this.name);
        name.append(" выборка по коду ");
        name.append(code);

        Catalog selectedCatalogs = new Catalog(name.toString());
        Set<Map.Entry<VoucherKey, Double>> setCatalog = catalog.entrySet();
        for(var catalogElement: setCatalog){
            if(catalogElement.getKey().getCode() == code){
                selectedCatalogs.catalog.put(catalogElement.getKey(), catalogElement.getValue());
            }
        }
        return selectedCatalogs;
    }

    public Catalog selectVoucherData(String typeHome){
        StringBuilder name = new StringBuilder(this.name);
        name.append(" выборка по типу дома ");
        name.append(typeHome);

        Catalog selectedCatalogs = new Catalog(name.toString());
        Set<Map.Entry<VoucherKey, Double>> setCatalog = catalog.entrySet();
        for(var catalogElement: setCatalog){
            if(catalogElement.getKey().getTypeHome().equals(typeHome)){
                selectedCatalogs.catalog.put(catalogElement.getKey(), catalogElement.getValue());
            }
        }
        return selectedCatalogs;
    }

    public Catalog selectVoucherData(String typeHome, String vacationerCode){
        StringBuilder name = new StringBuilder(this.name);
        name.append(" выборка по типу дома и коду потребителя");
        name.append(typeHome);

        Catalog selectedCatalogs = new Catalog(name.toString());
        Set<Map.Entry<VoucherKey, Double>> setCatalog = catalog.entrySet();
        for(var catalogElement: setCatalog){
            if(catalogElement.getKey().getTypeHome().equals(typeHome) &&
            catalogElement.getKey().getVacationerCode().equals(vacationerCode)){
                selectedCatalogs.catalog.put(catalogElement.getKey(), catalogElement.getValue());
            }
        }

        return selectedCatalogs;
    }

    public double selectSumPriceByTypeHome(String typeHome){
        int sum = 0;
        var selectedDataByTypeHome = selectVoucherData(typeHome);
        Collection<Double> values = selectedDataByTypeHome.catalog.values();
        for(var value: values){
            sum += value;
        }

        return sum;
    }

    public void putCatalog(){
        System.out.println(name);
        System.out.printf("%5s%17s%19s%25s \n", "Код", "Код потребителя", "Тип дома", "Цена");
        Set<Map.Entry<VoucherKey, Double>> setCatalog = catalog.entrySet();
        for(var element: setCatalog){
            System.out.printf(" %2d %23s %24s %20f\n"
                    , element.getKey().getCode()
                    , element.getKey().getVacationerCode()
                    , element.getKey().getTypeHome()
                    , element.getValue());
        }
    }

    public Catalog sortCodeAscVacationerCodeDesc(){
        Catalog sortCatalog = new Catalog(this.name
                + " отсортированная по возрастанию кода и по убыванию потребительского кода"
                , new CompCodeAscVacationerCodeDesc());
        Set<Map.Entry<VoucherKey, Double>> setCatalog = catalog.entrySet();
        for(var element: setCatalog){
            sortCatalog.addVoucher(element.getKey(), element.getValue());
        }

        return sortCatalog;
    }

    public Catalog sortTypeHomeAscVacationerCodeDesc(){
        Catalog sortCatalog = new Catalog(this.name
                + " отсортированная по возрастанию типа дома и по убыванию потребительского кода"
                , new CompTypeHomeAscVacationerCodeDesc());
        Set<Map.Entry<VoucherKey, Double>> setCatalog = catalog.entrySet();
        for(var element: setCatalog){
            sortCatalog.addVoucher(element.getKey(), element.getValue());
        }

        return sortCatalog;
    }
}
