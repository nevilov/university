package com.company.Практика2;

import java.util.Objects;

public class VoucherKey implements Comparable<VoucherKey> {
    private int code;
    private String vacationerCode;
    private String typeHome;

    public VoucherKey(int code, String vacationerCode, String typeHome) {
        this.code = code;
        this.vacationerCode = vacationerCode;
        this.typeHome = typeHome;
    }

    public VoucherKey(){
        this.code = 0;
        this.vacationerCode = "";
        this.typeHome = "";
    }

    public int getCode(){
        return code;
    }

    public String getVacationerCode(){
        return vacationerCode;
    }

    public String getTypeHome(){
        return typeHome;
    }

    public void setCode(int code){
        this.code = code;
    }

    public void setVacationerCode(String vacationerCode){
        this.vacationerCode = vacationerCode;
    }

    public void setTypeHome(String typeHome){
        this.typeHome = typeHome;
    }

    @Override
    public int compareTo(VoucherKey o) {
        if(code < o.code){
            return -1;
        }
        if(code > o.code){
            return 1;
        }
        if(vacationerCode.compareTo(o.vacationerCode) <0){
            return -1;
        }
        if(vacationerCode.compareTo(o.vacationerCode)>0){
            return 1;
        }
        if(typeHome.compareTo(o.typeHome)<0){
            return -1;
        }
        if(typeHome.compareTo(o.typeHome)>1){
            return 1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "VoucherKey{" +
                "code=" + code +
                ", vacationerCode='" + vacationerCode + '\'' +
                ", typeHome='" + typeHome + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VoucherKey that = (VoucherKey) o;
        return code == that.code && Objects.equals(vacationerCode, that.vacationerCode) && Objects.equals(typeHome, that.typeHome);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, vacationerCode, typeHome);
    }
}
