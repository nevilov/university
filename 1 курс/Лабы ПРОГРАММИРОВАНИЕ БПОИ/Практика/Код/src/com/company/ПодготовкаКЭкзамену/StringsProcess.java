package com.company.ПодготовкаКЭкзамену;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class StringsProcess {
    public static void main(String[] args) throws IOException {

        BufferedReader bf = new BufferedReader(new FileReader("src/test.txt"));
        String a; String b;
        while ((a = bf.readLine()) != null){
            b = bf.readLine();

            System.out.println(compSubString(a,b));
        }

        System.out.println(compWords(" раз два три четыре пять"));


    }

    public static int compSubString(String s, String p){
        if(s == null || p == null){
            return -1;
        }
        // sd sdsds sdsd
        // sd
        int cnt = 0;
        int position = s.indexOf(p);
        while (position>=0){
            cnt++;
            position = s.indexOf(p, p.length() + position);
        }

        return cnt;
    }

    public static int compWords(String s){
        if(s == null) {
            return -1;
        }
        int count = 1;
        s = s.trim();
        int position = s.indexOf(" ");

        while (position>=0){
            count++;
            position = s.indexOf(" ", 1+position);
        }

        return count;
    }
}
