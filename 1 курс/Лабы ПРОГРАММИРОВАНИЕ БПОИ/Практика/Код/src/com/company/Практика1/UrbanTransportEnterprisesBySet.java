package com.company.Практика1;

import java.util.*;
public class UrbanTransportEnterprisesBySet {
    final static String STRING_FORMAT = " | Предприятие %s | количество маршрутов %d |";

    private String name;
    private Set<CityTransportRoute> routesSet;

    public UrbanTransportEnterprisesBySet(String name, Set<CityTransportRoute> routesSet) {
        this.name = name;
        this.routesSet = routesSet;
    }

    public UrbanTransportEnterprisesBySet(String name){
        this.name = name;
        this.routesSet = new TreeSet<>();
    }

    public UrbanTransportEnterprisesBySet(String name, Comparator comparator){
        this.name = name;
        this.routesSet = new TreeSet<>(comparator);
    }

    public void setName(String name){
        this.name = name;
    }

    public int getCountCityTransportRoutes(){
        return routesSet.size();
    }

    public String getName() {
        return name;
    }

    public Set<CityTransportRoute> getRoutesSet() {
        return routesSet;
    }

    public boolean addCityTransportRoute(CityTransportRoute transportRoute){
        if(transportRoute == null){
            System.out.println("Не удалось добавить транспортный путь, так как объект пути был передан пустым");
            return false;
        }

        Iterator<CityTransportRoute> iterator = routesSet.iterator();
        while (iterator.hasNext()){
            if(iterator.next().getCode() == transportRoute.getCode()){
                System.out.println("Попытка добавить транспортный путь id которого уже присутствует в предприятии");
                return false;
            }
        }

        routesSet.add(transportRoute);
        return true;
    }

    public boolean removeCityTransportRoute(String code){
        var transportRoute = getCityTransportRouteByCode(code);
        if(transportRoute == null){
            System.out.println("Не удалось удалить транспортный путь, так как объект пути не был найден");
            return false;
        }

        if(routesSet.remove(transportRoute)){
            return true;
        }
        return false;
    }

    public boolean updateCityTransportRouteCode(String oldCode, String newCode){
        var transportRoute = getCityTransportRouteByCode(oldCode);
        if(transportRoute == null){
            System.out.println("Не удалось обновить код транспортного пути, так как объект пути был найден");
            return false;
        }
        if(getCityTransportRouteByCode(newCode) != null){
            System.out.println("Не удалось обновить траспортный путь, так как с данным кодом объект уже присутствует");
            return false;
        }
        routesSet.remove(transportRoute);
        transportRoute.setCode(newCode);
        routesSet.add(transportRoute);
        return true;
    }

    public double getAveragedExtentRoutes(){
        double routesLength = 0;
        int countRoutes = 0;

        for (var route: routesSet) {
            if(route !=null) {
                routesLength += route.getRouteLength();
                ++countRoutes;
            }
        }

        return (routesLength/countRoutes);
    }

    public UrbanTransportEnterprisesBySet getAboveAveragedExtentRoutes(){
        UrbanTransportEnterprisesBySet aboveAveragedExtentTransportRoutes = new UrbanTransportEnterprisesBySet(name + " транспортные пути, длина которых выше средней");
        var averagedRoute = getAveragedExtentRoutes();

        Iterator<CityTransportRoute> iterator = routesSet.iterator();
        CityTransportRoute iteration;
        while (iterator.hasNext()){
            if((iteration = iterator.next()).getRouteLength()>averagedRoute){
                aboveAveragedExtentTransportRoutes.addCityTransportRoute(iteration);
            }
        }

        return aboveAveragedExtentTransportRoutes;
    }

    public UrbanTransportEnterprisesBySet getCityTransportStopsOnRange(int left, int right){
        StringBuilder nameBuilder = new StringBuilder(this.name);
        nameBuilder.append(" транспортные пути на промежутке от ").append(left).append(" до ").append(right);
        UrbanTransportEnterprisesBySet transportEnterprisesOnRange = new UrbanTransportEnterprisesBySet(nameBuilder.toString());

        Iterator<CityTransportRoute> iterator = routesSet.iterator();
        CityTransportRoute iteration;
        while (iterator.hasNext()){
            iteration = iterator.next();
            if(iteration.getRouteLength()>=left && iteration.getRouteLength()<=right){
                transportEnterprisesOnRange.addCityTransportRoute(iteration);
            }
        }

        return transportEnterprisesOnRange;
    }

    public CityTransportRoute getCityTransportRouteByCode(String code){
        System.out.printf("В предприятии находим из соответствующий маршрут %s из списка маршрутов \n", code);
        Iterator<CityTransportRoute> cityTransportRouteIterator = routesSet.iterator();
        CityTransportRoute iteration;

        while (cityTransportRouteIterator.hasNext()){
            iteration = cityTransportRouteIterator.next();
            if(iteration.getCode().equals(code)){
                return iteration;
            }
        }
        return null;
    }

    public boolean removeCityTransportRoutesWhichLongerLowerThanAverage(){
        var averagedRoute =  getAveragedExtentRoutes();

        Set<CityTransportRoute> cityTransportRoutesToRemove = new TreeSet<>();

        for(var item : routesSet){
            if(item.getRouteLength()<averagedRoute){
                cityTransportRoutesToRemove.add(item);
            }
        }
        routesSet.removeAll(cityTransportRoutesToRemove);

        return true;
    }

    public UrbanTransportEnterprisesBySet sortTransportCityRoutesByCodeAsc(){
        UrbanTransportEnterprisesBySet urbanTransportEnterprisesBySetSorted = new UrbanTransportEnterprisesBySet(this.name + " отсортированный по возрастанию кода", new CompCodeAsc());
        for (var item: routesSet) {
            urbanTransportEnterprisesBySetSorted.addCityTransportRoute(item);
        }

        return urbanTransportEnterprisesBySetSorted;
    }

    public UrbanTransportEnterprisesBySet sortTransportCityRoutesByCodeDesc(){
        UrbanTransportEnterprisesBySet urbanTransportEnterprisesBySetSorted = new UrbanTransportEnterprisesBySet(this.name + " отсортированный по возрастанию кода", new CompCodeDesc());
        for (var item: routesSet) {
            urbanTransportEnterprisesBySetSorted.addCityTransportRoute(item);
        }

        return urbanTransportEnterprisesBySetSorted;
    }

    public void printCityTransportRoutes(){
        System.out.printf("%s %13s %38s %26s %26s \n", "| Код", "| Маршрут ", "| Тип ", "| Протяженность пути", "| Количество остановок |");
        for (var route: routesSet){
            System.out.println(route);
        }
    }

    @Override
    public String toString(){
        return String.format(STRING_FORMAT, name, getCountCityTransportRoutes());
    }


    /*Дополнительное задание*/
    /*Запрос на удаление*/
    public void removeCityTransportRoutesWhichNameLongestParam(int n, String letterCombination){
        Set<CityTransportRoute> cityTransportRoutesToRemove = new TreeSet<>();

        for (var item: routesSet) {
            if(item.getName().length()>n || !item.getName().contains(letterCombination)){
                cityTransportRoutesToRemove.add(item);
            }
        }

        for (var val: cityTransportRoutesToRemove) {
            routesSet.remove(val);
        }
        routesSet.removeAll(cityTransportRoutesToRemove);
    }
}