package com.company.Практика1;

import java.util.*;
public class UrbanTransportEnterprisesByList {
    final static String STRING_FORMAT = " | Предприятие %s | количество маршрутов %d |";

    private String name;
    private List<CityTransportRoute> routesList;


    public UrbanTransportEnterprisesByList(String name, List<CityTransportRoute> routesList) {
        this.name = name;
        this.routesList = routesList;
    }

    public UrbanTransportEnterprisesByList(String name){
        this.name = name;
        this.routesList = new ArrayList<>();
    }

    public void setName(String name){
        this.name = name;
    }

    public int getCountCityTransportRoutes(){
        return routesList.size();
    }

    public String getName() {
        return name;
    }

    public List<CityTransportRoute> getRoutesList() {
        return routesList;
    }

    public boolean addCityTransportRoute(CityTransportRoute transportRoute){
        if(transportRoute == null){
            System.out.println("Не удалось добавить транспортный путь, так как объект пути был передан пустым");
            return false;
        }

        Iterator<CityTransportRoute> iterator = routesList.listIterator();
        while (iterator.hasNext()){
            if(iterator.next().getCode() == transportRoute.getCode()){
                System.out.println("Попытка добавить транспортный путь id которого уже присутствует в предприятии");
                return false;
            }
        }

        routesList.add(transportRoute);
        return true;
    }

    public boolean removeCityTransportRoute(String code){
        var transportRoute = getCityTransportRouteByCode(code);
        if(transportRoute == null){
            System.out.println("Не удалось удалить транспортный путь, так как объект пути не был найден");
            return false;
        }

        routesList.remove(transportRoute);
        return true;
    }

    public boolean updateCityTransportRouteCode(String oldCode, String newCode){
        var transportRoute = getCityTransportRouteByCode(oldCode);
        if(transportRoute == null){
            System.out.println("Не удалось обновить код транспортного пути, так как объект пути был найден");
            return false;
        }
        if(getCityTransportRouteByCode(newCode) != null){
            System.out.println("Не удалось обновить траспортный путь, так как с данным кодом объект уже присутствует");
            return false;
        }

        var transportRouteIndex = routesList.indexOf(transportRoute);
        transportRoute.setCode(newCode);
        routesList.set(transportRouteIndex, transportRoute);
        return true;
    }

    public double getAveragedExtentRoutes(){
        double routesLength = 0;
        int countRoutes = 0;

        for (var route: routesList) {
            if(route !=null) {
                routesLength += route.getRouteLength();
                ++countRoutes;
            }
        }

        return (routesLength/countRoutes);
    }

    public UrbanTransportEnterprisesByList getAboveAveragedExtentRoutes(){
        UrbanTransportEnterprisesByList aboveAveragedExtentTransportRoutes = new UrbanTransportEnterprisesByList(name + " транспортные пути, длина которых выше средней");
        var averagedRoute = getAveragedExtentRoutes();

        Iterator<CityTransportRoute> iterator = routesList.listIterator();
        CityTransportRoute iteration;
        while (iterator.hasNext()){
            if((iteration = iterator.next()).getRouteLength()>averagedRoute){
                aboveAveragedExtentTransportRoutes.addCityTransportRoute(iteration);
            }
        }

        return aboveAveragedExtentTransportRoutes;
    }

    public UrbanTransportEnterprisesByList getCityTransportStopsOnRange(int left, int right){
        StringBuilder nameBuilder = new StringBuilder(this.name);
        nameBuilder.append(" транспортные пути на промежутке от ").append(left).append(" до ").append(right);
        UrbanTransportEnterprisesByList transportEnterprisesOnRange = new UrbanTransportEnterprisesByList(nameBuilder.toString());

        Iterator<CityTransportRoute> iterator = routesList.listIterator();
        CityTransportRoute iteration;
        while (iterator.hasNext()){
            iteration = iterator.next();
            if(iteration.getRouteLength()>=left && iteration.getRouteLength()<=right){
                transportEnterprisesOnRange.addCityTransportRoute(iteration);
            }
        }

        return transportEnterprisesOnRange;
    }

    public CityTransportRoute getCityTransportRouteByCode(String code){
        System.out.printf("В предприятии находим из соответствующий маршрут %s из списка маршрутов \n", code);
        Iterator<CityTransportRoute> cityTransportRouteIterator = routesList.iterator();
        CityTransportRoute iteration;

        while (cityTransportRouteIterator.hasNext()){
            iteration = cityTransportRouteIterator.next();
            if(iteration.getCode().equals(code)){
                return iteration;
            }
        }
        return null;
    }

    public boolean removeCityTransportRoutesWhichLongerLowerThanAverage(){
        var averagedRoute =  getAveragedExtentRoutes();

        List<CityTransportRoute> cityTransportRoutesToRemove = new ArrayList<>();

        for(var item : routesList){
            if(item.getRouteLength()<averagedRoute){
                cityTransportRoutesToRemove.add(item);
            }
        }
        routesList.removeAll(cityTransportRoutesToRemove);

        return true;
    }

    public UrbanTransportEnterprisesByList sort(Comparator comparator){
        UrbanTransportEnterprisesByList sortingList = new UrbanTransportEnterprisesByList(name + " сортировка по возрастанию кода", routesList);
        Collections.sort(sortingList.routesList ,comparator);

        return sortingList;
    }

    public void printCityTransportRoutes(){
        System.out.printf("%s %13s %38s %26s %26s \n", "| Код", "| Маршрут ", "| Тип ", "| Протяженность пути", "| Количество остановок |");
        for (var route: routesList){
            System.out.println(route);
        }
    }

    @Override
    public String toString(){
        return String.format(STRING_FORMAT, name, getCountCityTransportRoutes());
    }

    /*Дополнительное задание*/
    /*Запрос на выборку*/
    public UrbanTransportEnterprisesByList getRandomCityTransportRoutes(){
        StringBuilder stringBuilder = new StringBuilder(name);
        stringBuilder.append(" транспортные пути выбранные случайным образом");
        Random random = new Random();
        UrbanTransportEnterprisesByList randomCityTransportRoutes = new UrbanTransportEnterprisesByList(stringBuilder.toString());

        for(int i=0; i<routesList.size(); ++i){
            randomCityTransportRoutes.addCityTransportRoute(routesList.get(random.nextInt(routesList.size() - 1)));
        }

        return randomCityTransportRoutes;
    }

    /*Дополнительное задание*/
    /*Запрос на удаление*/
    public void removeCityTransportRoutesWhichNameLongestParam(int n, String letterCombination){
        List<CityTransportRoute> cityTransportRoutesToRemove = new ArrayList<>();

        for (var item: routesList) {
            if(item.getName().length()>n || !item.getName().contains(letterCombination)){
                cityTransportRoutesToRemove.add(item);
            }
        }

        routesList.removeAll(cityTransportRoutesToRemove);
    }
}
