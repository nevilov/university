package com.company.Практика1;

import java.util.Objects;
public class CityTransportRoute implements Comparable<CityTransportRoute>{
    private final static String STRING_FORMAT = "| %s | %-40s | %-10s | %-20f | %-20d |";

    private String code;
    private String name;
    private String type;
    private double routeLength;
    private int numberOfStops;

    public CityTransportRoute(String code, String name, String type, double routeLength, int numberOfStops) {
        this.code = code;
        this.name = name;
        this.type = type;
        this.routeLength = routeLength;
        this.numberOfStops = numberOfStops;
    }

    public CityTransportRoute(){

    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public double getRouteLength() {
        return routeLength;
    }

    public int getNumberOfStops() {
        return numberOfStops;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setRouteLength(double routeLength) {
        this.routeLength = routeLength;
    }

    public void setNumberOfStops(int numberOfStops) {
        this.numberOfStops = numberOfStops;
    }

    @Override
    public String toString(){
        return String.format(STRING_FORMAT, code, name, type, routeLength, numberOfStops);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CityTransportRoute that = (CityTransportRoute) o;
        return Double.compare(that.routeLength, routeLength) == 0 && numberOfStops == that.numberOfStops && Objects.equals(code, that.code) && Objects.equals(name, that.name) && Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name, type, routeLength, numberOfStops);
    }


    @Override
    public int compareTo(CityTransportRoute o) {
        if(code.compareTo(o.code)<0){
            return -1;
        }
        else if(code.compareTo(o.code) == 0){
            return 0;
        }


        return 1;
    }
}
