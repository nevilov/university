package com.company.Практика1;

import java.util.Comparator;

public class CompCodeDesc implements Comparator {
    @Override
    public int compare(Object o1, Object o2) {
        CityTransportRoute cityTransportRoute1 = (CityTransportRoute) o1;
        CityTransportRoute cityTransportRoute2 = (CityTransportRoute) o2;
        if(cityTransportRoute1.getCode().compareTo(cityTransportRoute2.getCode()) < 1){
            return 1;
        }
        else if(cityTransportRoute1.equals(cityTransportRoute2)){
            return 0;
        }

        return -1;
    }
}
