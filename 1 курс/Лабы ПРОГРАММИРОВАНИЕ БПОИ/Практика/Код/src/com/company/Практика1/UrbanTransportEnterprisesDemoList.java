package com.company.Практика1;

public class UrbanTransportEnterprisesDemoList {
    public static void main(String[] args) {
        System.out.println("Транспортные предприятия на основе списка");
        UrbanTransportEnterprisesByList transportEnterprisesSevastopol = new UrbanTransportEnterprisesByList("Севастопольская транспортная компания");
        UrbanTransportEnterprisesByList transportEnterprisesSimferopol = new UrbanTransportEnterprisesByList("Симферопольская транспортная компания");

        System.out.println("Добавляем маршруты в севастопольскую транспортную компанию");
        transportEnterprisesSevastopol.addCityTransportRoute(new CityTransportRoute("245448", "Нахимова - Студ. городок №10", "Тролейбус", 12, 8 ));
        transportEnterprisesSevastopol.addCityTransportRoute(new CityTransportRoute("274392", "Карантинная балка - Студ. городок №14", "Автобус", 2, 3 ));
        transportEnterprisesSevastopol.addCityTransportRoute(new CityTransportRoute("645440", "Горпищенко - Степаняна №12", "Тролейбус", 20, 13 ));
        transportEnterprisesSevastopol.addCityTransportRoute(new CityTransportRoute("285441", "Лазарева - ЦУМ №16", "Автобус", 7, 5));
        transportEnterprisesSevastopol.addCityTransportRoute(new CityTransportRoute("285443", "Лазарева - Омега №100", "Автобус", 14, 9));
        transportEnterprisesSevastopol.addCityTransportRoute(new CityTransportRoute("285444", "Меньшикова - Нахимава №99", "Тролейбус", 8, 4));

        System.out.printf("Вывод транспортных путей %s \n", transportEnterprisesSevastopol);
        transportEnterprisesSevastopol.printCityTransportRoutes();

        System.out.printf("Средняя протяженность пути в севастопольской транспортной компании = %f \n", transportEnterprisesSevastopol.getAveragedExtentRoutes());
        System.out.println("Севастопольская транспортная компания, отсортированная по возрастанию кода");
        transportEnterprisesSevastopol.sort(new CompCodeAsc()).printCityTransportRoutes();
        System.out.println("Севастопольская транспортная компания, отсортированная по убыванию кода");
        transportEnterprisesSevastopol.sort(new CompCodeDesc()).printCityTransportRoutes();
        System.out.printf("Заменим код 645440 у маршрута Горпищенко - Степаняна №12 на 437463 \n");
        transportEnterprisesSevastopol.updateCityTransportRouteCode("645440", "437463");
        System.out.println("Удалим маршрут с кодом 285441");
        transportEnterprisesSevastopol.removeCityTransportRoute("285441");
        System.out.println("Вывод транспортных путей после проделанных операций");
        transportEnterprisesSevastopol.printCityTransportRoutes();

        System.out.printf("Получим транспортные пути, длина которых выше средней %f \n",transportEnterprisesSevastopol.getAveragedExtentRoutes());
        transportEnterprisesSevastopol.getAboveAveragedExtentRoutes().printCityTransportRoutes();

        System.out.println("Получим все транспортные пути, протяженность которых на промежутке от 2 до 12");
        transportEnterprisesSevastopol.getCityTransportStopsOnRange(2,13).printCityTransportRoutes();


        System.out.println("Добавляем маршруты в симферопольскую транспортную компанию");
        transportEnterprisesSimferopol.addCityTransportRoute(new CityTransportRoute("1253", "Героев Сталинграда - Меганом №15", "Тролейбус", 30, 10 ));
        transportEnterprisesSimferopol.addCityTransportRoute(new CityTransportRoute("2131", "Маршала Жукова - Центральный рыной №5", "Автобус", 10, 5));
        transportEnterprisesSimferopol.addCityTransportRoute(new CityTransportRoute("1233", "Дмитрия Ульянова - Центральный рынок №1", "Тролейбус", 4, 3));

        System.out.printf("Вывод транспортных путей %s \n", transportEnterprisesSimferopol);
        transportEnterprisesSimferopol.printCityTransportRoutes();

        System.out.printf("Средняя протяженность пути в симферопольской транспортной компании = %f \n", transportEnterprisesSimferopol.getAveragedExtentRoutes());
        System.out.println("Симферопольская транспортная компания, отсортированная по возрастанию кода");
        transportEnterprisesSimferopol.sort(new CompCodeAsc()).printCityTransportRoutes();
        System.out.println("Симферопольская транспортная компания, отсортированная по убыванию кода");
        transportEnterprisesSimferopol.sort(new CompCodeDesc()).printCityTransportRoutes();

        System.out.println("Удалим все симферопольские маршруты, длина которых ниже средней");
        transportEnterprisesSimferopol.removeCityTransportRoutesWhichLongerLowerThanAverage();
        transportEnterprisesSimferopol.printCityTransportRoutes();


        /*Дополнительное задание*/
        /*Запрос на выборку*/
        System.out.println("Запрос на выборку");
        System.out.println("Вернуть группу объектов, состоящих из n выбранных случайных элементов");
        transportEnterprisesSevastopol.getRandomCityTransportRoutes().printCityTransportRoutes();

        System.out.println("Запрос на удаление");
        transportEnterprisesSevastopol.printCityTransportRoutes();
        System.out.println("Удалить из группы все объекты с длиной String-поля, превышающей n символов или не включающие заданное буквосочетание городок.");
        transportEnterprisesSevastopol.removeCityTransportRoutesWhichNameLongestParam(40, "городок");
        transportEnterprisesSevastopol.printCityTransportRoutes();
    }
}
