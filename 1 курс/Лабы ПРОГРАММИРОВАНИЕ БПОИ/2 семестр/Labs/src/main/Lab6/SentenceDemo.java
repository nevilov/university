package main.Lab6;

public class SentenceDemo {
    public static void main(String[] args) {
        Sentence sent = new Sentence("Не говори мне больше ничего, я не желаю знать ответа, ведь лучше промолчать всерьез, чем получить вот это: error[traseId: 4214dWe23s]");
        putWords(sent.StartedOnLetter('н'));

        Sentence secsent = new Sentence("казенная каракульная дорога караблестроительная фабрика");
        System.out.println(secsent.StartedOnLetterEndedOnLetter('к','я'));

        //concat method
        System.out.println(StringProcessLib.concat("Это пример"," который должен вернуть слитную строку"));
        System.out.println(StringProcessLib.concat("Мы должны вернуть - !","Мы должны вернуть - !"));

    }

    public static void putWords(String[] words){
        for(int i=0 ;i< words.length;++i){
            System.out.println(words[i] + " ");
        }
    }
}
