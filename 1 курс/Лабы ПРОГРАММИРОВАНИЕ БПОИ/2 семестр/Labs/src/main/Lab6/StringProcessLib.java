package main.Lab6;

public class StringProcessLib {
    public static String concat(String s1, String s2){
        StringBuilder resultString = new StringBuilder();

        if(s1.equals(s2)){
            resultString.append("!");
        }
        else{
            resultString.append(s1).append(s2);
        }

        return resultString.toString();
    }
}
