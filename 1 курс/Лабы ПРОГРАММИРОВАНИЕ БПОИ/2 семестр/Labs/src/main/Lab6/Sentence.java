package main.Lab6;

public class Sentence {
    private final static int MAX_NUM_OF_WORDS = 40;

    private String sourceStr;
    private String[] words;
    private int counter; // Счетчик слов

    public Sentence(){
        sourceStr = "";
        words = null;
        counter = 0;
    }

    public Sentence(String str){
        sourceStr = str.trim(); //Удалить пробелы в начале и в конце
        if(str == "") {
            words = null;
            counter = 0;
        }
        else{
            splitIntoWords();
        }
    }

    private void splitIntoWords(){
        words = new String[MAX_NUM_OF_WORDS];
        int start;
        int end = sourceStr.indexOf(" ");
        counter = 0; //счетчик слов - слов еще не нашли
        start = 0; //начало первого слова - самый левый символ строки  end = sourceStr.indexOf(" "); //найти конец первого слова
        try { //а вдруг индекс выйдет за границу массива?
        while (end > 0){ //пока есть слова
            words[counter] = sourceStr.substring(start, end); //очередное слово  counter++; //увеличиваем для следующего слова
            while (sourceStr.charAt(end) == ' ') end++;
            counter++;
            start = end; //теперь лишние пробелы будут пропущены  end = sourceStr.indexOf(" ", start);
            end = sourceStr.indexOf(" ", start);
        } //while
        words[counter] = sourceStr.substring(start); //последнее слово
        }
        catch (ArrayIndexOutOfBoundsException e){
            System.out.println(
                    "В предложении слишком много слов, лишние придется обрезать: "   + "\n"+e);
            System.out.println();
            counter--;
        }
        counter++; //число слов
    }

    public String[] StartedOnLetter(char letter){
        splitIntoWords();
        String[] wordsStartedOnLetterWithNullElements = new String[counter];
        int j =0;
        for(int i=0; i<counter; ++i){
            if(words[i].substring(0,1).equals(String.valueOf(letter))){
                wordsStartedOnLetterWithNullElements[i] = words[i];
                j++;
            }
        }

        String[] wordStartedOnLetter = new String[j];
        var cnt = 0;
        for(int i=0;i<counter;++i){
            if(wordsStartedOnLetterWithNullElements[i] != null){
                wordStartedOnLetter[cnt] = wordsStartedOnLetterWithNullElements[i];
                cnt++;
            }
        }
        return  wordStartedOnLetter;
    }

    public String StartedOnLetterEndedOnLetter(char startLetter, char endLetter){
        StringBuilder result = new StringBuilder();
        splitIntoWords();
        var test = words[0].substring(words[0].length()-1);

        for(int i=0; i< counter;++i){
            if(words[i].substring(0,1).equals(String.valueOf(startLetter))
                    && words[i].substring(words[i].length()-1).equals(String.valueOf(endLetter))){
                result.append(words[i]);
                result.append(" ");
            }
        }
        return result.toString();

    }
}
