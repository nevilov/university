package main.Lab4;
///Партия деталей
public class Batch
{

    private static final String BATCH_FORMAT_STRING = "Id: %d, Имя: %s, Количество: %d, Цена %f";
    private int id;
    private String name;
    private int countDetails;
    private double price;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return countDetails;
    }

    public double getPrice() {
        return price;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCount(int count) {
        this.countDetails = count;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Batch(){
        id = 0;
        name = "";
        countDetails = 0;
        price = 0;
    }

    public Batch(int id, String name, int count, double price){
        this.id = id;
        this.name = name;
        this.countDetails = count;
        this.price = price;
    }

    public Batch(Batch batch){
        id = batch.id;
        name = batch.name;
        countDetails = batch.countDetails;
        price = batch.price;
    }

    public int compareTo(Batch batch){
        if(id<batch.id) return -1;
        if((id==batch.id) && (name.compareTo(batch.name)<0)&& (countDetails!=batch.countDetails) && price != batch.price) return -1;
        if((id==batch.id) && (name.compareTo(batch.name)==0) && countDetails==batch.countDetails && price == batch.price) return 0;

        else return 1;
    }

    public String toString(){
        return String.format(BATCH_FORMAT_STRING, id, name, countDetails, price);
    }

}

