package main.Lab2;

public class Fork implements Comparable<Fork> {
    private final static String FORK_FORMAT_STRING ="Вилка: код: %d, Заземление? %b, цвет: %s, мощность: %d";

    private int code;
    private boolean grounding;
    private String color;
    private int power;

    public Fork(){
        code = 0;
        color = "";
        power = 0;
    }

    public Fork(int code, boolean grounding, String color, int power)
    {
        this.code = code;
        this.grounding = grounding;
        this.color = color;
        this.power = power;
    }

    public Fork(Fork obj)
    {
        code = obj.code;
        grounding = obj.grounding;
        color = obj.color;
        power = obj.power;
    }

    public void setCode(int code){ this.code = code; }
    public void setGrounding(boolean grounding){ this.grounding = grounding; }
    public void setColor(String color){ this.color = color; }
    public void setPower(int power){ this.power = power; }

    public int getCode(){ return code;}
    public boolean getGrounding(){ return  grounding;}
    public String getColor(){ return color; }
    public int getPower(){ return  power; }

    @Override
    public boolean equals(Object obj)
    {
        if(obj == null) return false;
        if(this == obj) return true;
        if(getClass() != obj.getClass()) return false;

        Fork f = (Fork) obj;
        return (code == f.code && color.equals(f.color) && grounding == f.grounding && power == f.power);
    }

    public int hashCode()
    {
        return 7* new Integer(code)
                + 13 * color.hashCode()
                + 15 * new Boolean(grounding).hashCode()
                + 18 * new Integer(power);
    }

    @Override
    public int compareTo(Fork o) {
        if(code != o.code) return -1;
        if(code == o.code && color.compareTo(o.color)<0) return -1;
        if(code == o.code && color.compareTo(o.color)==0 && grounding != o.grounding) return -1;
        if(code == o.code && color.compareTo(o.color)==0 && grounding == o.grounding && power != o.power) return -1;
        if(code == o.code && color.compareTo(o.color)==0 && grounding == o.grounding && power == o.power) return 0;
        else return 1;
    }

    public boolean accord (Socket socket){
        return (color.equals(socket.getColor())
                && (power == socket.getPower())
                && ((grounding == socket.getEuro()) || (!grounding == !socket.getEuro())));
    }

    @Override
    public String toString() {
        return String.format(FORK_FORMAT_STRING, code, grounding, color, power);
    }
}
