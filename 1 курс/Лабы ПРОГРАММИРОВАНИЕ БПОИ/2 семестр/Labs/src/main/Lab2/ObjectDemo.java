package main.Lab2;

public class ObjectDemo {
    public static void main(String[] argc){
       Socket sockets[] = new  Socket[5];
        sockets[0] = new Socket(123,false, "Синий", 13, 220);
        sockets[1] = new Socket(124,true, "Красный", 14, 220);
        sockets[2] = new Socket(125,true, "Зеленый", 15, 220);
        sockets[3] = new Socket(123,false, "Розовый", 13, 220);
        sockets[4] = new Socket(127,false, "Белый", 13, 220);

        SocketArr(sockets);
        System.out.printf("Сортировка массива розеток\n");
        BubbleSort(sockets);
        SocketArr(sockets);

        System.out.println();

        Fork[] forks = new Fork[5];
        forks[0] = new Fork(123, true, "Розовый", 13);
        forks[1] = new Fork(122, false, "Синий", 13);
        forks[2] = new Fork(125, true, "Белый", 13);
        forks[3] = new Fork(123, true, "Розовый", 15);
        forks[4] = new Fork(127, false, "Розовый", 13);

        SocketArr(forks);
        System.out.printf("Сортировка массива вилок\n");
        BubbleSort(forks);
        SocketArr(forks);

        System.out.println();

        Socket s1 = new Socket(1, true, "Белый", 16, 220 );
        Socket s2 = s1; // Создание псевдонима
        Socket s3 = new Socket(s1); // Создание клона
        Socket s4 = new Socket(5, false, "Коричневый", 13, 220);
        Socket s5 = null;

        //Вывод информации о созданных объектах
        System.out.printf("Розетка 1 %s, Хэш-код %d \n", s1, s1.hashCode());
        System.out.printf("Розетка 2 %s, Хэш-код %d \n", s2, s2.hashCode());
        System.out.printf("Розетка 3 %s, Хэш-код %d \n", s3, s3.hashCode());
        System.out.printf("Розетка 4 %s, Хэш-код %d \n", s4, s4.hashCode());
        System.out.printf("Розетка 5%s \n", s5);

        Fork f1 = new Fork(1, true, "Белый", 16);
        Fork f2 = f1;
        Fork f3 = new Fork(f1);
        Fork f4 = new Fork(2, false, "Зеленый", 13);
        Fork f5 = null;

        System.out.printf("f1: %s, Хэш-код %d\n", f1, f1.hashCode());
        System.out.printf("f2: %s, Хэш-код %d\n", f2, f2.hashCode());
        System.out.printf("f3: %s, Хэш-код %d\n", f3, f3.hashCode());
        System.out.printf("f4: %s, Хэш-код %d\n", f4, f4.hashCode());
        System.out.printf("f5: %s, \n", f5);

        System.out.println();

        Fork fork = new Fork(111, true, "Зеленый", 13);
        System.out.printf("Вилка %s \n", fork);

        System.out.printf("s1.accord(f1): %b \n", s1.accord(f1));
        System.out.printf("f1.accord(s1): %b \n", f1.accord(s1));

        System.out.printf("s2.accord(f1): %b \n", s2.accord(f2));
        System.out.printf("f2.accord(s1): %b \n", f2.accord(s2));

        System.out.printf("s3.accord(f3): %b \n", s3.accord(f3));
        System.out.printf("f3.accord(s3): %b \n", f3.accord(s3));

        System.out.printf("s4.accord(f4): %b \n", s4.accord(f4));
        System.out.printf("f4.accord(s4): %b \n", f4.accord(s4));

        f5 = new Fork();
        f5.setCode(13);
        f5.setColor("Черный");
        f5.setGrounding(false);

        s5 = new Socket();
        s5.setCode(13);
        s5.setColor("Черный");
        s5.setEuro(false);

        if(s5.accord(f5))
        {
            System.out.printf("Объект %s \n соответствует объекту \n %s", s5, f5);
        }
        else
        {
            System.out.printf("Объект %s \n не соответствует объекту \n %s", s5, f5);
        }





    }

    public static void SocketArr(Comparable[] s){
        for(int i = 0;i<s.length;++i){
            System.out.printf("%s,Хэш-код %d \n",s[i], s[i].hashCode());
        }

    }

    public static void BubbleSort(Comparable[] arr){
        boolean flag;
        for (int m = arr.length-1; m > 0; m--){
            flag = true;
            for (int j = 0; j < m; j++)
                if (arr[j].compareTo(arr[j+1]) > 0) {
                    Comparable socket = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = socket;
                    flag = false;
                }
            if (flag) break;
        }
    }
}
