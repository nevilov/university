package main.Lab1.Proj2;

public class SocketDemo {

    public static void main(String[] argc){
        Socket s0 = new Socket();
        s0.SetCode(0);
        s0.SetColor("Оранжевый");
        s0.SetEuro(true);
        s0.SetPower(16);
        s0.SetVoltage(220);
        System.out.printf("Розетка с кодом %d, Евро? %b, Количество ампер в разетки = %d, Хэш-код: %d \n", s0.GetCode(), s0.GetEuro(), s0.GetAmperage(), s0.hashCode());

        Socket s1 = new Socket(1, true, "Желтый", 16, 220 );
        Socket s2 = s0; // Создание псевдонима
        Socket s3 = new Socket(s1); // Создание клона
        Socket s4 = new Socket(5, false, "Коричневый", 13, 220);
        Socket s5 = null;

        //Вывод информации о созданных объектах
        System.out.printf("Розетка 1 %s, Хэш-код %d \n", s1, s1.hashCode());
        System.out.printf("Розетка 2 %s, Хэш-код %d \n", s2, s2.hashCode());
        System.out.printf("Розетка 3 %s, Хэш-код %d \n", s3, s3.hashCode());
        System.out.printf("Розетка 4 %s, Хэш-код %d \n", s4, s4.hashCode());
        System.out.printf("Розетка 5%s \n", s5);

        //Сравнения
        System.out.printf("Розетка 1 == Розетка 2: %s \n", s1.equals(s2));
        System.out.printf("Розетка 1 == Розетка 3: %s \n", s1.equals(s3));
        System.out.printf("Розетка 1 == Розетка 4: %s \n", s1.equals(s4));
        System.out.printf("Розетка 4 == Розетка 5: %s \n", s1.equals(s5));

        Socket sockets[] = new Socket[5];
        sockets[0] = new Socket(123,false, "Синий", 13, 220);
        sockets[1] = new Socket(124,true, "Красный", 14, 220);
        sockets[2] = new Socket(125,true, "Зеленый", 15, 220);
        sockets[3] = new Socket(123,false, "Синий", 13, 220);
        sockets[4] = new Socket(127,false, "Белый", 13, 220);

        System.out.printf("Массив розеток до сортировки \n");
        SocketArr(sockets);

        BubbleSort(sockets);

        System.out.printf("Массив розеток после сортировки \n");
        SocketArr(sockets);


        Grounding g0 = new Grounding();
        g0.SetCode(143);
        g0.SetColor("Кораловый");
        g0.SetEuro(true);
        g0.SetGrounding(true);
        g0.SetPower(13);
        g0.SetVoltage(220);
        System.out.printf("Розетка с кодом %d, Евро? %b, Количество ампер в разетки = %d, Заземление? %b, Хэш-код: %d \n", g0.GetCode(), g0.GetEuro(), g0.GetAmperage(), g0.GetGrounding(), g0.hashCode());

        Grounding g1 = new Grounding(1, true, "Желтый", 16, 220, true);
        Grounding g2 = g0; // Создание псевдонима
        Grounding g3 = new Grounding(g1); // Создание клона
        Grounding g4 = new Grounding(5, false, "Коричневый", 13, 220, false);
        Grounding g5 = null;

        //Вывод информации о созданных объектах
        System.out.printf("Розетка 1 %s, Хэш-код %d \n", g1, g1.hashCode());
        System.out.printf("Розетка 2 %s, Хэш-код %d \n", g2, g2.hashCode());
        System.out.printf("Розетка 3 %s, Хэш-код %d \n", g3, g3.hashCode());
        System.out.printf("Розетка 4 %s, Хэш-код %d \n", g4, g4.hashCode());
        System.out.printf("Розетка 5%s \n ", g5);

        //Сравнения
        System.out.printf("Розетка 1 == Розетка 2: %s \n", g1.equals(g2));
        System.out.printf("Розетка 1 == Розетка 3: %s \n", g1.equals(g3));
        System.out.printf("Розетка 1 == Розетка 4: %s \n", g1.equals(g4));
        System.out.printf("Розетка 4 == Розетка 5: %s \n", s1.equals(g5));

        //3.Демонстрируем обращение к объекту подкласса с помощью
        //ссылочной переменной суперкласса
        Socket soc = g1;
        System.out.println ("Выполнен оператор Socket soc = g1");
        System.out.printf ("soc==g3: %s\n", soc.equals(g3));

        Grounding groundings[] = new Grounding[5];
        groundings[0] = new Grounding(123,false, "Синий", 13, 220, true);
        groundings[1] = new Grounding(124,true, "Красный", 14, 220, false);
        groundings[2] = new Grounding(125,true, "Зеленый", 15, 220, true);
        groundings[3] = new Grounding(123,false, "Синий", 13, 220, true);
        groundings[4] = new Grounding(127,false, "Белый", 13, 220, true);

        System.out.printf("Массив розеток с заземлением до сортировки \n");
        SocketArr(groundings);

        BubbleSort(groundings);

        System.out.printf("Массив розеток с заземлением после сортировки \n");
        SocketArr(groundings);

    }

    //Вывод массива объектов
    public static void SocketArr(Socket[] s){
        for(int i = 0;i<s.length;++i){
            System.out.printf("%s,Хэш-код %d \n",s[i], s[i].hashCode());
        }

    }

    public static void BubbleSort(Socket[] arr){
        boolean flag;
        for (int m = arr.length-1; m > 0; m--){
            flag = true;
            for (int j = 0; j < m; j++)
                if (arr[j].compareTo(arr[j+1]) > 0) {
                    Socket socket = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = socket;
                    flag = false;
                }
            if (flag) break;
    }
    }
}
