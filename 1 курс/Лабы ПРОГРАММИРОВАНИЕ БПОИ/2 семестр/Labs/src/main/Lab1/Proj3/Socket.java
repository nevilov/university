package main.Lab1.Proj3;
public class Socket implements Comparable<Socket>{
    private final static String SOCKET_FORMAT_STRING ="Socket: код: %d, Евро? %b, цвет: %s, мощность: %d, напряжение: %d";

    private int code;
    private boolean euro;
    private String color;
    private int power;
    private int voltage;

    public Socket(){
        code = 0;
        color = "";
        power = 0;
        voltage = 0;
    }

    public Socket(int code, boolean euro, String color, int power, int voltage){
        this.code = code;
        this.euro = euro;
        this.color = color;
        this.power = power;
        this.voltage = voltage;
    }

    public Socket(Socket obj) {
        code = obj.code;
        euro = obj.euro;
        color = obj.color;
        power = obj.power;
        voltage = obj.voltage;
    }


    public void SetCode(int code){this.code = code;}
    public void SetEuro(boolean euro){this.euro = euro;}
    public void SetColor(String color){this.color = color;}
    public void SetPower(int power){this.power = power;}
    public void SetVoltage(int voltage){this.voltage = voltage;}

    public int GetCode(){return code;}
    public boolean GetEuro(){return euro;}
    public String GetColor(){return color;}
    public int GetPower(){return power;}
    public int GetVoltage(){return voltage;}



    @Override
    public boolean equals(Object obj){
        if(obj == null) return false;
        if(this == obj) return true;

        Socket s = (Socket) obj;

        return code == s.code && euro == s.euro && color == s.color && power == s.power && voltage == s.voltage;
    }

    public int compareTo(Socket s){
        if(code<s.code) return -1;
        if((code==s.code) && (color.compareTo(s.color)<0)&& (euro!=s.euro) && power != s.power) return -1;
        if((code==s.code) && (color.compareTo(s.color)==0) && euro==s.euro && power == s.power) return 0;

        else return 1;
    }

    public int GetAmperage(){
        return (voltage/power);
    }

    public int hashCode(){
        return 7*(new Integer(code).hashCode())
                + 11 * color.hashCode()
                + 13 * (new Boolean(euro).hashCode());
    }

    public String toString(){
        return String.format(SOCKET_FORMAT_STRING,  code, euro, color, power, voltage);
    }

}
