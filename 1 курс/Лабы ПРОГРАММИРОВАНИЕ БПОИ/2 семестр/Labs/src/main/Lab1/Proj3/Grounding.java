package main.Lab1.Proj3;

public class Grounding extends Socket{
    private final static String GROUNDING_FORMAT_STRING ="Заземление? %b";
    private boolean grounding;

    public Grounding(){
        super();
    }

    public Grounding(int code, boolean euro, String color, int power, int voltage, boolean grounding){
        super(code, euro, color, power, voltage);
        this.grounding = grounding;
    }

    public Grounding(Grounding obj){
        super(obj);
    }

    public void SetGrounding(boolean grounding) {
        this.grounding = grounding;
    }

    public boolean GetGrounding(){
        return grounding;
    }

    public String toString() {
        return String.format(GROUNDING_FORMAT_STRING, grounding);
    }

    public boolean equals(Grounding obj){
        if(!(super.equals(obj)))return false;
        return true;
    }

    public int hashCode(){
        return super.hashCode()
                + 19 * (new Boolean(grounding)).hashCode();
    }

    public int compareTo(Grounding grd){
        int comp = super.compareTo(grd);
        if(comp<0) return -1;
        Grounding obj = (Grounding) grd;
        if(comp==0 && (new Boolean(grounding)).compareTo(obj.grounding)<1) return -1;
        if(comp==0 && (new Boolean(grounding)).compareTo(obj.grounding)==1) return 0;
        return 1;

    }




}
