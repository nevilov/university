package main.Lab5;

public class SupplyLinked implements SupplyInterface {
        public static final String SUPPLY_FORMAT_STRING = "Шифр поставки: %d, Количество поставок: %d";

        private int cipher;
        private String name;
        private SupplyLinked next;
        private Batch batches;

        SupplyLinked(){
            batches = null;
            next = null;
        }
        public SupplyLinked(String name){
            batches = new Batch();
            batches.setName(name);
            batches.setCount(0);
            batches.setId(0);
            batches.setPrice(0);

            next = null;
        }

        public SupplyLinked(Batch batch){
            batches = batch;
            next = null;
        }

        public int getCipher() {
            return cipher;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public int getCount() {
                int num = 0;
                SupplyLinked el = next;
                while (el != null){
                    num++;
                    el = el.next;
                }
                return num;
            }

        public void setCipher(int cipher) {
            this.cipher = cipher;
        }

        public Batch getBatchById(int id) {
            if(next == null) return null;
            SupplyLinked sp = next; // sp ссылка на очередной элемент
            while (sp !=null){
                if(sp.batches.getId() == id) return sp.batches;
                sp = sp.next;
            }
            return null;
        }

        public boolean addBatch(Batch batch) {
            if(getBatchById(batch.getId()) !=null) return false;
            SupplyLinked sp = new SupplyLinked(batch); // Создали новый элемент списка
            if(next == null) next = sp; //Вставили в пустой список
            else { // Вставить первым в непустой список
                SupplyLinked old = next;
                next = sp;
                sp.next = old;
                return true;
            }

            return false;
        }

        public boolean deleteBatchById(int id) {
            SupplyLinked sp = this; //sp.next - очередной элемент списка

            while(sp.next != null){
                if(sp.next.batches.getId() == id){
                    sp.next = sp.next.next;
                    return true;
                }
                sp = sp.next; // переход к следующему элементу
            }
            return false;
        }

        public double getAveragedPrice() {
            if(next == null) return 0;
            double avg = 0;
            int num = 0;
            SupplyLinked sp = next;
            while (sp != null){
                avg = avg + sp.batches.getPrice();
                num++;
                sp = sp.next;
            }
            return avg/num;
        }

        public SupplyLinked getAboveAveragePriceBatches() {
            double avg = getAveragedPrice();
            SupplyLinked head = new SupplyLinked(String.format("Поставки, цека которых вые средней %f", avg));
            SupplyLinked sp = next;
            while (sp != null){ // пока в списке есть элементы
                if (sp.batches.getPrice() > avg)
                    head.addBatch(sp.batches);
                sp=sp.next;
            }
            return head;
        }

        public SupplyLinked getIntervalBatches(double bottom, double top) {
            SupplyLinked head = new SupplyLinked (String.format (
                    "Поставки, которые находятся в диапазоне от %f до %f", bottom, top));
            SupplyLinked sp = next; // очередной элемент основного списка
            while (sp != null) { // пока в списке есть элементы
                double b = sp.batches.getPrice();
                if ((b >= bottom) && (b <= top))
                    head.addBatch(new Batch(sp.batches));
                // перейти к следующему элементу списка
                sp = sp.next;
            }
            return head;
        }

        public void batchOutput() {
            System.out.printf("%5s%17s%11s%17s\n",
                    "Id","Имя","Цена","Количество");
            SupplyLinked sp = next; // el - очередной элемент списка
            int i = 1; // номер студента
            while (sp != null){ // пока в списке есть элементы
                System.out.printf(" %-7d %-15s %-16f %-10d\n",
                        sp.batches.getId(),sp.batches.getName(), sp.batches.getPrice(), sp.batches.getCount());
        // перейти к следующему элементу списка
                sp = sp.next;
                i=i+1;
            }
        }

        public String toString(){
            return String.format(SUPPLY_FORMAT_STRING, cipher, getCount());
        }

}

