package main.Lab5;

public class SupplyDemo {
    public static void main(String[] args) {
        Batch[] batches = new Batch[]{
                new Batch(1, "Шуруп", 10, 1),
                new Batch(2, "Гайка", 10, 20),
                new Batch(3, "Винтик", 10, 30),
                new Batch(4, "Шуруп", 10, 4),
                new Batch(5, "Шестеренка", 10, 5)
        };

        SupplyInterface supply1 = new Supply("Шестеренки");
        SupplyInterface supply2 = new SupplyLinked("Болты");
        supply1.setName("Первая поствка ");
        supply2.setName("Вторая поствка ");

        supply2.setCipher(32);
        for(int i=0; i<5;++i){
            supply1.addBatch(batches[i]);
        }

        for(int i=4; i>=0;--i){
            supply2.addBatch(batches[i]);
        }

        System.out.printf("Сформированы следующие партии: \n");
        System.out.printf("supply1 %s \nsupply2 %s \n", supply1, supply2);
//--
        supply1.addBatch(new Batch(321, "Саморез 3мм", 100, 2));
        supply2.addBatch(new Batch(3546, "Саморез 23мм", 1200, 2));


        System.out.printf("Поставка 1---\n");
        supply1.batchOutput();

        System.out.printf("Средняя цена %f\n", supply1.getAveragedPrice());
        // Посколько методы возвращают ссылку на объект того же класса SupplyLinked, от имени возвращаемого объекта может быть запущен методо output
        System.out.println("Детали, цена которых выше средней");
        supply1.getAboveAveragePriceBatches().batchOutput();

        System.out.println("Детали, цена которых находится в диапазоне от 5 до 30");
        supply1.getIntervalBatches(5, 30).batchOutput();


        System.out.printf("Удаляем поставку с id = 5\n");
        supply1.deleteBatchById(5);
        supply1.batchOutput(); // Удалились поставки с кодом 5

        System.out.printf("Поставка 2---\n");
        System.out.println();
        supply2.batchOutput();

        System.out.printf("Средняя цена %f\n", supply2.getAveragedPrice());
        System.out.println("Детали, цена которых выше средней");
        supply2.getAboveAveragePriceBatches().batchOutput();

        System.out.printf("Удаляем поставку с id = 3\n");
        supply2.deleteBatchById(3);
        supply2.batchOutput(); // Удалились поставки с кодом 3

        System.out.printf("Проверка наличия поставок\n");
        int n = 1232;
        Batch b1 = supply1.getBatchById(n);
        if(b1 == null){
            System.out.printf("Такой поставки не существует\n");
        }
        else{
            System.out.println(b1);
        }
        SetSupplyStatus(supply1, Statuses.Открытый.toString());
        SetSupplyStatus(supply1, Statuses.Закрытый.toString());

        System.out.println(supply1.getName());
        System.out.println(supply2.getName());


    }

    public static void SetSupplyStatus(SupplyInterface supply, String status){
        supply.setName(supply.getName() + " " + status);
    }

    enum Statuses{
        Открытый,
        Закрытый,
        Купленный
    }
}
