package main.Lab5;

public class Supply implements SupplyInterface {
    public static final String SUPPLY_FORMAT_STRING = "Шифр поставки: %d, Количество поставок: %d";
    public static final int MIN_NUM = 6;

    private int cipher;
    private String name;
    private Batch[] batches;
    private int count; //Трекаем количество эл в массиве объектов

    public Supply(){
        cipher = 0;
        count = 0;
    }
    public int getCipher() {
        return cipher;
    }

    public int getCount() {
        return count;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }


    public void setCipher(int cipher) {
        this.cipher = cipher;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Batch getBatchById(int id) {
        for (int i = 0; i < count; ++i) {
            if (batches[i].getId() == id)
                return batches[i];
        }
        return null;
    }

    public boolean addBatch(Batch batch) {
        if (batches.length == count)
            return false;
        for (int i = 0; i < count; ++i) {
            if (batches[i].getId() == batch.getId())
                return false;
        }

        batches[count] = batch;
        ++count;

        return true;
    }

    public boolean deleteBatchById(int id) {
        int i;
        for (i = 0; i < batches.length; ++i) {
            if (batches[i].getId() == id)
                break;
        }
        if (i == count)
            return false;

        for (i = i + 1; i < count; ++i) {
            batches[i - 1] = batches[i];
        }
        batches[i - 1] = null;
        --count;
        return true;
    }

    public double getAveragedPrice() {
        if (count == 0)
            return 0;

        double summaryPrice = 0;
        for (int i = 0; i < count; ++i) {
            summaryPrice += batches[i].getPrice();
        }
        return (summaryPrice / count);
    }

    public Supply getAboveAveragePriceBatches() {
        if (count == 0)
            return null;

        var avg = getAveragedPrice();
        Supply list = new Supply(String.format
                ("Детали, цена которых выше средней %.2f: ", avg), count);
        for (int i = 0; i < count; i++)
            if (batches[i].getPrice() > avg) list.addBatch(batches[i]);
        return list;
    }

    public Supply getIntervalBatches(double bottom, double top) {
        var intervalBatches = new Batch[batches.length];
        int j = 0;
        for (int i = 0; i < batches.length; ++i) {
            if (batches[i].getPrice() >= bottom || batches[i].getPrice() <= top) {
                intervalBatches[i] = batches[i];
                ++j;
            }
        }

        var withoutNullReferences = new Batch[j];
        for (int i = 0; i < j; ++i) {
            if (intervalBatches != null) {
                withoutNullReferences[i] = intervalBatches[i];
            }
        }
        var sup = new Supply();
        return sup;

    }

    public void batchOutput() {
        for (int i = 0; i < count; ++i) {
            System.out.printf("Деталь: код = %d, имя = %s, цена = %f, количество = %d \n",
                    batches[i].getId(), batches[i].getName(), batches[i].getPrice(), batches[i].getCount());
        }
    }

    public Batch[] SortBatch(int type) {
        if (type == 0) {
            return batches;
        }
        Batch t;
        if (type < 0) {
            for (int i = 0; i < count; i++) {
                for (int j = 0; j < count - 1; j++) {
                    if (batches[j].compareTo(batches[j + 1]) < 0) {
                        t = batches[j];
                        batches[j] = batches[j + 1];
                        batches[j + 1] = t;
                    }
                }
            }
        } else {
            for (int i = 0; i < count; i++) {
                for (int j = 0; j < count - 1; j++) {
                    if (batches[j].compareTo(batches[j + 1]) > 0) {
                        t = batches[j];
                        batches[j] = batches[j + 1];
                        batches[j + 1] = t;
                    }
                }
            }
        }
        return batches;
    }
    public Supply(String name){
        this.name = name;
        batches = new Batch[MIN_NUM];
    }


    public Supply(String name, int count){
        this.name = name;
        this.count = 0;
        batches = new Batch[MIN_NUM];
    }

    public Supply(int cipher, int maxCount) {
        this.cipher = cipher;
        this.count = 0;
        batches = new Batch[maxCount];
    }

    public String toString(){
        return String.format(SUPPLY_FORMAT_STRING, cipher, count);
    }
}
