package main.Lab5;


public interface SupplyInterface {
    void setName(String name);

    String getName();

    int getCount();

    void setCipher(int cipher);

    Batch getBatchById(int id);

    boolean addBatch(Batch batch);

    boolean deleteBatchById(int id);

    double getAveragedPrice();

    SupplyInterface getAboveAveragePriceBatches();

    SupplyInterface getIntervalBatches(double bottom, double top);

    void batchOutput();
}
