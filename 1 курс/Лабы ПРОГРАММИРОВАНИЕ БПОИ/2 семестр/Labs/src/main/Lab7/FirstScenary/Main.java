package main.Lab7.FirstScenary;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException{
            byte[] bytes = {(byte)0x41, (byte)0x6c, (byte) 0x69, (byte) 0x6d, (byte)0x20,
                    (byte) 0x59, (byte) 0xF1, (byte)0xF3, (byte)0xF4,(byte)0xEE, (byte)0xE2};
        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;

        for(int i=0; i<bytes.length;++i){
            System.out.printf("Код: %02x ", bytes[i]); //Ascii code
            System.out.println("Можно инетпритировать как символ " + (char)bytes[i]);
        }
        try{
            outputStream = new FileOutputStream("byte1_Юсуфов_ПИН20.txt");
            outputStream.write(bytes);
        }
        catch (IOException e){
            System.out.println("Произошла ошибка при выводе содержимого в файл" + e.getMessage());
        }
        finally {
            if(outputStream!=null){
                outputStream.close();
            }
        }
    }
}
