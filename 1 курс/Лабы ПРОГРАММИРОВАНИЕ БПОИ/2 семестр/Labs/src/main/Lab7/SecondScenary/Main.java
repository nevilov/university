package main.Lab7.SecondScenary;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

public class Main {
    private static Charset UTF8 = Charset.forName("UTF-8");

    public static byte resetTetrade(byte b){
        int firstTetra = b>>4;
        if(firstTetra == 7){
            b = (byte) (firstTetra<<4);
        }

        return (byte) b;
    }

    public static String byteToString(byte b){
        //возвращает строку символов - двоичный эквивалент байта
        String str = "";
        for (int i = 1; i <= 8; i++){
            str = b < 0 ? str + "1" : str + "0";
            b = (byte)(b << 1);
        }
        return str;
    }

    public static void main(String[] args) throws IOException {
        System.out.println(resetTetrade((byte)113));

        byte b = 0; int d =0;
        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;
        try{
            inputStream = new FileInputStream("byte2_Юсуфов_ПИН20.txt");
            outputStream = new FileOutputStream("byte3_Юсуфов_ПИН20.txt");
            //byte[] buffer = inputStream.readAllBytes();

            while((d = inputStream.read()) != -1){
                b = (byte) d;
                System.out.print(byteToString(b) + " --> ");
                //System.out.println((char) d);
                b = resetTetrade((byte)b);
                System.out.println(byteToString(b));
                outputStream.write(b);
            }
        }
        catch (IOException e){
            System.out.println("Произошла ошибка " + e.getMessage());
        }
        finally {
            if(outputStream != null){
                outputStream.close();
            }
        }
    }
}
