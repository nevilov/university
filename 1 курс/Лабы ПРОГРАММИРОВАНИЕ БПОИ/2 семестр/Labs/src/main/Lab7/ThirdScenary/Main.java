package main.Lab7.ThirdScenary;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        char b = 0; int d = 0;
        FileReader fi = null;
        FileWriter fo = null;
        try{
            fi = new FileReader("char1_Юсуфов_ПИН20.txt");
            fo = new FileWriter("char2_Юсуфов_ПИН20.txt");
            while((d = fi.read()) != -1){
                b = (char)d;
                System.out.print(b);

                if(b == 'Ё'){
                    b = 'Е';
                }

                if(b == 'ё'){
                    b = 'е';
                }
                fo.write(b);
            }
        }
        catch (IOException e){System.out.println("\nerror:\n"+e);}
        finally{if(fi != null) fi.close(); if(fo != null) fo.close();
        //fo.write("TestExceptions"); // Тестирование исключения
        }
    }
}

