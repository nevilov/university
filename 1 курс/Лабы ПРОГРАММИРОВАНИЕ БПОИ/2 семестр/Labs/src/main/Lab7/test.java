package main.Lab7;

import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;

class Test{
    public static byte byteProcess(byte b){ //обработка байта
        //установка старшего бита в 1, если младший равен нулю
        int c = b; //почему удобно работать с int?
        if ((c & 1) == 0) c = c | 0x80;
        return (byte)c;
    }
    public static String byteToString(byte b){
        //возвращает строку символов - двоичный эквивалент байта
        String str = "";
        for (int i = 1; i <= 8; i++){
            str = b < 0 ? str + "1" : str + "0";
            b = (byte)(b << 1);
        }
        return str;
    }
    public static void main(String[ ] args) throws IOException{
        byte b = 0; int d = 0;
        FileInputStream fi = null; //ссылка напоток ввода
        FileOutputStream fo = null; //ссылка напоток вывода
        try{
            // создаем потоки fi и fo и связываем их с файлами
            fi = new FileInputStream("byte2_Юсуфов_ПИН20.txt");
            fo = new FileOutputStream("byte3_Юсуфов_ПИН20.txt");
            // пока в потоке ввода есть данные
            while((d = fi.read()) != -1){ //ввести очередной байт из потока fi
                b = (byte)d; //выделить младший байт из четырех (d – int)
                System.out.print(byteToString(b) + " --> "); // байт до изменения
                b = byteProcess(b); // обработка байта
                System.out.println(byteToString(b)); // байт после изменения
                fo.write(b); //вывести байт в поток fo
            }
        }
        catch (IOException e){System.out.println("error:\n"+e);} //ошибка ввода-   // вывода
        finally{ if(fi != null) fi.close(); if(fo != null) fo.close(); } //закрыть потоки,   //если они открыты  }
    }
}
