package main.Lab9.test;

import java.io.File;
import java.util.Random;

public class RAFileDemo{
    public static void main (String args[ ]) {
        int n = 9, m = 5; String t = "double"; //9 матриц 3 x 3, тип double
//В папке проекта предварительно должна быть создана папка Matr
        File f1 = new File("matr_" + n + "_" + m + "_" + t + ".bin"); //файл
        System.out.printf ("В файл выводятся %d матриц %dx%d c элементами типа %s\n", n, m, m, t);
//Формирование элементов матриц
        Random r = new Random(); //генератор псевдо-случайных чисел
        double [ ] arr = new double[n*m*m]; //всего чисел для вывода в файл
        for (int i = 0; i < arr.length; i++)
            arr[i] = r.nextDouble()*100; //получаем очередное число
        try{
//-------------------вывод--------------------------------------------------------------------
            IOFile.outDoubleRAFile(f1, arr, 0);
//----------------ввод всего файла и контроль-----------------------------------
//очистка arr:
            for (int i = 0; i < arr.length; i++) arr[i] = 0;
//ввод в одномерный массив всех чисел из файла
            System.out.printf (
                    "Содержимое файла %s, интерпретированное как double-значения:\n", f1.getName());
            arr = IOFile.inpDoubleArrRAFile(f1);
            MatrProc.putArr (arr, m * m); //выводим массив arr в окно терминала
//--------ввод из файла третьей и седьмой матрицы--------------------
            double[ ][ ] matr1= IOFile.inpDoubleMatrRAFile(f1, m, m * m * 8 * (3 - 1)); System.out.println ("Введена матрица номер 3:"); MatrProc.putMatr(matr1); //выводим матрицу в окно терминала
            System.out.printf ("Сумма элементов матрицы: %.2f\n",
            MatrProc.sumElMatr(matr1)); double[ ][ ] matr2 = IOFile.inpDoubleMatrRAFile(f1, m, m * m * 8 * (7 - 1)); System.out.println ("Введена матрица номер 7:"); MatrProc.putMatr(matr2); //выводим матрицу в окно терминала
            System.out.printf ("Сумма элементов матрицы: %.2f\n",
            MatrProc.sumElMatr(matr2));
//вычисление суммы матриц
            matr1 = MatrProc.sumTwoMatr(matr1, matr2); System.out.println ("Сумма матриц:"); MatrProc.putMatr(matr1);
//--------------Вывод результирующей матрицы в файл-------------------
//-------------------------на место третьей матрицы-----------------------------
            double[ ] arr1 = MatrProc.matrToArr(matr1); //преобразуем матрицу
            System.out.println ("После преобразования матрицы в массив:"); MatrProc.putArr(arr1,m * m); //выводим на терминал
            IOFile.outDoubleRAFile(f1, arr1, m * m * 8 * (3 - 1)); //выводим в файл
//----------------ввод всего файла и контроль------------------------------------
//очистка arr:

            for (int i = 0; i < arr.length; i++) arr[i] = 0;
//ввод в одномерный массив всех чисел из файла
            System.out.printf (
                    "Новое содержимое файла %s, интерпретированное как double-значения:\n", f1.getName());
            arr = IOFile.inpDoubleArrRAFile(f1); MatrProc.putArr(arr,m * m); //вывод на терминал
        } catch (Exception e) { } finally{ }
    } //main
}

