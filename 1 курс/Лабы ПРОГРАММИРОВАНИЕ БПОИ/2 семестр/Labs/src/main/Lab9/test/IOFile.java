package main.Lab9.test;

import java.io.*;
public class IOFile{ //Функции ввода-вывода
    public static boolean outDoubleRAFile(File file, double[ ] arr, int pos)
            throws IOException {
        RandomAccessFile outpFile = null; boolean f = true; //для возврата
        try {
            outpFile = new RandomAccessFile(file,"rw"); outpFile.seek (pos); // устанавливаем указатель файла
// на заданную позицию
            for (int i = 0; i < arr.length; i++)
                outpFile.writeDouble(arr[i]); // выводим очередное число (8 байт)
        }
        catch (Exception e) {f = false;}
        finally{ if(outpFile != null) outpFile.close();} //закрываем файл
        return f;
    }
    public static double[ ][ ] inpDoubleMatrRAFile(File file, int m, int pos)
            throws IOException {
        RandomAccessFile inpFile = null;
        long toRead; //счетчик байтов в файле
        double [ ][ ] matr = new double[m][m]; // создаем матрицу m x m
        try { //контролируем исключения при вводе данных
//открываем файл для чтения:
            inpFile = new RandomAccessFile(file, "r");
            inpFile.seek (pos); //устанавливаем указатель файла в
//заданную позицию
            toRead = inpFile.length() - pos + 1; //число байтов в файле, начиная
//с позиции pos
//Вводим числа из файла в матрицу:
            M1:{for (int i = 0; i < m; i++) // блок помечен меткой M1
                for (int j = 0; j < m; j++){
                    matr[ i ][ j ] = inpFile.readDouble(); //введено очередное число
                    toRead -= 8; // уменьшаем число оставшихся байт на 8
                    if (toRead <= 0) break M1; //если данные в файле закончились,
//переход на конец блока, отмеченного меткой M1
                }
            } //Конец блока, отмеченного меткой M1
        } //try
        catch (Exception e) {matr = null;} //если исключение, вернем null
        finally{ if(inpFile != null) inpFile.close();
        } // закрываем файл
        return matr; //возвращаем ссылку на матрицу или null
    }
    public static double[ ] inpDoubleArrRAFile(File file) throws IOException {
        RandomAccessFile inpFile = null;
        long toRead; // счетчик байтов в файле
        double [ ]arr = null;
        try {inpFile = new RandomAccessFile(file, "r"); toRead = inpFile.length(); //длина файла в байтах
            arr = new double[(int) toRead / 8]; // создаем double-массив
            int i = 0;
            while (toRead > 0){ //пока в файле есть числа
                arr[i] = inpFile.readDouble(); //прочли очередное число (8 байт)
                i = i + 1;
                toRead -= 8; // уменьшаем число оставшихся байтов на 8
            }
        }
        catch (Exception e)  {arr = null;} //если исключение, вернем null
        finally{ if (inpFile != null) inpFile.close();} // закрываем файл
        return arr; //возвращаем ссылку на массив или null
    }
}

