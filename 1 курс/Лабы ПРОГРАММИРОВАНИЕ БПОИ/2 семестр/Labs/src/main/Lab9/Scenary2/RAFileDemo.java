package main.Lab9.Scenary2;
import java.io.File;
import java.util.Random;

public class RAFileDemo{
    public static void main (String args[ ]) {
        int n = 5, m = 5; String t = "short";
        File f1 = new File("matr1_" + n + "_" + m + "_" + t + ".bin"); //файл
         System.out.printf (
        "В файл выводятся %d матриц %dx%d c элементами типа %s\n", n, m, m, t);
        Random r = new Random();
        short [ ] arr = new short[n*m* m];
        for (int i = 0; i < arr.length; i++)
            arr[i] = (short)(r.nextInt()*100);
        try{
//-------------------вывод--------------------------------------------------------------------
            IOFile.outShortRAFile(f1, arr, 1);

//----------------ввод всего файла и контроль-----------------------------------
            for (int i = 0; i < arr.length; i++)
                arr[i] = 0;
            System.out.printf (
                    "Содержимое файла %s, интерпретированное как short-значения:\n", f1.getName());
            arr = IOFile.inpShortArrRAFile(f1);
            MatrProcess.putArr (arr, m * m);

//--------ввод из файла второй и пятой матрицы--------------------
            short[ ][ ] matr2= IOFile.inpShortMatrRAFile(f1, m, m * m * 2 * (2 - 1));
            System.out.println ("Введена матрица номер 2:");
            MatrProcess.putMatr(matr2);

            System.out.printf ("Сумма элементов матрицы: %d \n", MatrProcess.sumElMatr(matr2));

            System.out.println ("Введена матрица номер 5:");
            short[ ][ ] matr5 = IOFile.inpShortMatrRAFile(f1, m, m * m *2 * (5 - 1));
            MatrProcess.putMatr(matr5);

            System.out.printf ("Обработка матриц по провилу ~a[i][j] & ~b[i][j] \n");
            matr2 = MatrProcess.multipleMatrixByRule(matr2, matr2);
            System.out.println ("Обработка матриц:"); MatrProcess.putMatr(matr2);


//--------------Вывод результирующей матрицы в файл-------------------
//-------------------------на место третьей матрицы----------------------------

            short[ ] arr1 = MatrProcess.matrToArr(matr2); //преобразуем матрицу
// в массив
        System.out.println ("После преобразования матрицы в массив:");
            MatrProcess.putArr(arr1,m * m); //выводим на терминал
            IOFile.outShortRAFile(f1, arr1, m * m * 2* (3 - 1));
//----------------ввод всего файла и контроль------------------------------------

            for (int i = 0; i < arr.length; i++) arr[i] = 0;
            System.out.printf (
                    "Новое содержимое файла %s, интерпретированное как short-значения:\n", f1.getName());
            arr = IOFile.inpShortArrRAFile(f1); MatrProcess.putArr(arr,m * m);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally{ }
    }
}
