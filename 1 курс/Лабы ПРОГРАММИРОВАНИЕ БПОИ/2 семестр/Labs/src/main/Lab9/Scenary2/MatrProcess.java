package main.Lab9.Scenary2;

public class MatrProcess {
    public static void putMatr (short [ ][ ]matr){
        if (matr == null) return;
        for (int i = 0; i < matr.length; i++) { for (int j = 0; j < matr[0].length; j++)
            System.out.printf ("% 8d", matr[ i ][ j ]); System.out.println();
        }
    }
    public static void putArr (short X[ ], int n){
        if (X == null) return;
        for (int i = 0; i < X.length; i++){
            if ((i != 0) && (i % n == 0)) {
                System.out.println();
            }
            System.out.printf ("% 8d", X[i]);
        }
        System.out.println();
    } //putArr
    public static short[ ] matrToArr (short matr[ ][ ]){
        short[ ] arr = new short[matr.length * matr[0].length]; int k = 0;
        for (int i = 0; i < matr.length; i++)
            for (int j = 0; j < matr[0].length; j++){ arr[ k ] = matr[ i ][ j ];
                k = k+1;
            }
        return arr;
    }
    public static short sumElMatr(short[ ][ ] matr){
        if (matr == null) return 0; short sum = 0;
        for (int i = 0; i < matr.length; i++)
            for (int j = 0; j < matr[0].length; j++) sum += matr[ i ][ j ];
        return sum;
    }

    public static short[ ][ ] sumTwoMatr(short[ ][ ] m1, short [ ][ ] m2){
        if ((m1 == null) || (m2 == null) || (m1.length != m2.length) || (m1[0].length != m2[0].length)) return null;
        short [ ][ ]m = new short[m1.length][m1[0].length];
        for (int i = 0; i < m.length; i++)
            for (int j = 0; j < m[0].length; j++)
                m[ i ][ j ] = (short)(m1[ i ][ j ] + m2[ i ][ j ]);
            return m;
    }

    public static short[][] multipleMatrixByRule(short[][] matr1, short[][] matr2){
        if ((matr1 == null) || (matr2 == null) || (matr1.length != matr2.length) || (matr1[0].length != matr2[0].length)) return null;
        short[][] result = new short[matr1.length][matr1[0].length];

        for(int i=0; i<matr1.length;++i){
            for(int j = 0; j< matr1[0].length; ++j){
                result[i][j] = (short) ((~matr1[i][j]) & (~matr2[i][j]));
            }
        }
        return result;
    }
}
