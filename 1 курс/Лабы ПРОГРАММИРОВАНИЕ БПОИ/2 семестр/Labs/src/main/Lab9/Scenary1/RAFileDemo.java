package main.Lab9.Scenary1;

import java.io.*;
public class RAFileDemo{
    static void p (String s){
        System.out.println(s);
    }
    static void fileInfo(File f){
        p("Имя файла: " + f.getName());
        p("Путь: " + f.getPath());
        p("Абсолютный путь: " + f.getAbsolutePath()); p("Надкаталог: " + f.getParent());
        p(f.exists() ? "существует" : "не существует"); p(f.canWrite() ? "для записи" : "не для записи");

        p(f.canRead() ? "для чтения" : "не для чтения"); p((f.isDirectory()? "каталог":"не каталог"));
        p(f.isFile() ? "нормальный файл" : "возможно именованный канал"); p(f.isAbsolute() ? "абсолютный" : "не абсолютный");
        p("последняя модификация файла: " + f.lastModified ()); p("размер файла: " + f.length() + " байтов"); System.out.println();
    }
    public static void main (String args[ ]) {
        File f1 = new File("test/test.tif"); //файл
        fileInfo(f1); //вывести свойства файла
        File f2 = new File("test"); //папка
        fileInfo(f2); // вывести свойства папки
        String [ ] S = f2.list(); //получить список файлов папки
        System.out.printf("Список файлов каталога %s:\n " , f2.getName()); for (int i = 0; i < S.length; i++) System.out.println(S[i]);
        try{
//-------------------ввод----------------------------------------------------
            byte [ ] arr = IOFile.inputRAFile(f1); if (arr != null) System.out.printf (
                    "Информация из файлa %s загружена успешно\n", f1.getAbsolutePath());
            else System.out.printf (
                        "Ошибка ввода из файлa %s\n", f1.getAbsolutePath());
//--------------------вывод без замены---------------------------------
            File f3 = new File("test/test1.tif"); //файл
            if (IOFile.outputRAFile(f3, arr, 0, arr.length, 0)) System.out.printf (
                    "\nКопирование в файл %s произведено успешно\n", f3.getAbsolutePath());
            else System.out.printf ("\nОшибка вывода в файл %s\n",
                    f3.getAbsolutePath());
//--------------------вывод и замена-------------------------------------
            f3 = new File("test/test2.tif"); //файл
            if (IOFile.outputRAFile(f3, arr, 0, arr.length, 0)) System.out.printf (
                    "\nКопирование в файл %s произведено успешно\n", f3.getAbsolutePath());
            else System.out.printf ("\nОшибка вывода в файл %s\n",

                    f3.getAbsolutePath());

            int n = 19000;
            byte [ ] arr1 = new byte[n];
            for (int i = 0; i < n; i++)
            {arr1[i] =(byte) 219;} // 0xDB=219(в десятичной системе)
            if (IOFile.outputRAFile(f3, arr1, 0, arr1.length, 10500))
                System.out.printf (
                        "\nВывод помехи в в файл %s произведен успешно\n", f3.getAbsolutePath());
            else System.out.printf ("\nОшибка вывода в файл %s\n",
                    f3.getAbsolutePath());
//----------------------------------------------------------------------------------
            S = f2.list();
            System.out.printf("Список файлов каталога %s:\n", f2.getName()); for (int i = 0; i < S.length;i++) System.out.println(S[i]);
        }
        catch (Exception e) {
            System.out.println("Произошла ошибка" + e.getMessage());
        }
    }
}
