package main.Lab9.Scenary1;

import java.io.*;

public class IOFile {
    public static byte[] inputRAFile(File file) throws IOException {
        RandomAccessFile inpFile = null;
        int nextByte;
        int i = 0;
        byte[] arr = null;
        try {
            inpFile = new RandomAccessFile(file, "r");
            arr = new byte[(int) inpFile.length()];
            while ((nextByte = inpFile.read()) != -1) {
                arr[i] = (byte) nextByte;
                i = i + 1;
            }

        } catch (Exception e) {
            arr = null;
        } finally {
            if (inpFile != null) inpFile.close();
        }
        return arr;
    }

    public static boolean outputRAFile(File file, byte[] arr, int startInd, int num, long position) throws IOException {
        RandomAccessFile outpFile = null;
        boolean f = true;
        int endInd = startInd + num;
        try {
            outpFile = new RandomAccessFile(file, "rw");
            outpFile.seek(position);
            for (int i = startInd; i < endInd; i++) outpFile.write((int) arr[i]);

        } catch (Exception e) {
            f = false;
        } finally {
            if (outpFile != null) outpFile.close();
        }
        return f;
    }
} 
