package main.Lab8;

public class IntellectMatr {
    private int[][] matr;

    public IntellectMatr(int[][] matr) {
        this.matr = matr;
    }

    public int[][] getMatr() {
        return matr;
    }

    /*
    * функция, возвращающая вектор, i-ый элемент которого равен произведению положительных элементов i-го столбца матрицы (без параметров);
     */
    public int[] calculateMatrix(){
        int [] vectorMatr = new int[matr[0].length];

        for(int i =0 ;i <matr[0].length; ++i){
            vectorMatr[i] = 1;
            for (int j =0 ;j < matr.length;++j){
                if(matr[j][i]>0) {
                    vectorMatr[i] *= matr[j][i];
                }
            }
        }

        return vectorMatr;
    }

    // Функция, заменяющая на ноль все элементы матрицы, модуль которых меньше параметра
    public void replacementMatrix(int a){
        for(int i=0;i<matr.length;++i){
            for(int j=0; j<matr[0].length;j++){
                if(Math.abs(matr[i][j]) < a){
                    matr[i][j] = 0;
                }
            }
        }
    }

    // Вернуть номер в которой каждый элемент больше параметра a
    public int lineWithElementsLargeParam(int a){
        boolean everyDigitLargeParam = false;
        for(int i=0; i< matr.length;++i){
            for(int j=0; j< matr[0].length; j++){
                if(matr[i][j]<a){
                    everyDigitLargeParam = false;
                    break;
                }
                everyDigitLargeParam = true;
            }

            if(everyDigitLargeParam){
                return i;
            }
        }
        return -1;
    }

    //Удалить строки матрицы сумма элементов в которых больше порогового значения а
    public void delRowWhereSumLargeParam(int a){
        int sumLine;
        for(int i=0;i<matr.length;++i){
            sumLine = 0;
            for(int j =0 ; j< matr[0].length; ++j){
                sumLine+=matr[i][j];
            }
            if(sumLine > 0){
                delRow(i);
            }
        }
    }

    public int[] NegInRow() {
        int X[] = new int[matr.length];
        for (int i = 0; i < matr.length; i++) {
            X[i] = 0;
            for (int j = 0; j < matr[0].length; j++)
                if (matr[i][j] < 0) X[i]++;

        }
        return X;
    }

    public boolean delRow(int n) {
        if (matr == null) return false;
        if (matr[0] == null) return false;
        if ((n < 0) || (n > matr.length - 1)) return false;
        if (matr.length - 1 == 0) {
            matr = null;
            return true;
        }
        int[][] X = new int[matr.length - 1][matr[0].length];
        int k = 0;
        for (int i = 0; i < matr.length; i++)
            if (i != n) {
                X[k] = matr[i];
                k++;
            }
        matr = X;
        return true;
    }

    public void putMatr() {
        if ((matr == null) || (matr[0] == null)) return;
        for (int i = 0; i < matr.length; i++) {
            for (int j = 0; j < matr[0].length; j++)
                System.out.printf("%d ", matr[i][j]);
            System.out.println();
        }
    }
}

