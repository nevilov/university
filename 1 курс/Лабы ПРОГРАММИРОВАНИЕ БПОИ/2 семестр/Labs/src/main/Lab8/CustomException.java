package main.Lab8;

public class CustomException extends Exception{
    private String message;

    public CustomException(String message){
        this.message = message;
    }

    @Override
    public String toString() {
        return "CustomException{" +
                "message='" + message + '\'' +
                '}';
    }
}
