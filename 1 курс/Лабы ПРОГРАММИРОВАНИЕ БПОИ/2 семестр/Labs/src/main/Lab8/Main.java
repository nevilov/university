package main.Lab8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main (String[ ] args) throws IOException {
        String fileName;
        String line="";
        String flag="";
        int [ ][ ] matr;
        BufferedReader br = new BufferedReader (
                new InputStreamReader(System.in) );
        do {
            System.out.println("Введите имя файла для ввода матрицы");   fileName = (br.readLine()).trim();

            matr = IO.inpMatr(fileName);
            if (matr != null){
                System.out.println ("Матрица введена успешно");

                IntellectMatr imatr = new IntellectMatr(matr);

                System.out.println("Введена матрица:");
                imatr.putMatr();

                System.out.println("Вернуть номер в которой каждый элемент больше параметра a");
                System.out.println(imatr.lineWithElementsLargeParam(7));

                System.out.println("Функция, возвращающая вектор, i-ый элемент которого равен произведению положительных элементов i-го столбца матрицы (без параметров)");
                IO.putArr(imatr.calculateMatrix());

                System.out.println("Заменить элемент матрицы нулем если его модуль меньше параметра a");
                imatr.replacementMatrix(10);
                imatr.putMatr();

                System.out.println("Удалить строки матрицы сумма элементов в которых больше порогового значения а");
                imatr.delRowWhereSumLargeParam(80);
                imatr.putMatr();


                System.out.println("Введите имя файла для вывода матрицы");   fileName = (br.readLine()).trim();
                if (IO.outMatr(fileName, imatr.getMatr()))
                    System.out.println ("Успешный вывод");
            }
            System.out.println("Продолжить?: Да - <Enter>, Нет - <n>");   flag = br.readLine().trim();
        } while(!flag.equals("n"));
    }
}
