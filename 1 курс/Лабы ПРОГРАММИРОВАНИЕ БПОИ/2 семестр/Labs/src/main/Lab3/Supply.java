package main.Lab3;

///Поставка деталей
public class Supply {
    public static final String SUPPLY_FORMAT_STRING = "Шифр поставки: %d, Количество поставок: %d";
    public static final int MIN_NUM = 6;

    private int cipher;
    private Batch[] batches;
    private int count; //Трекаем количество эл в массиве объектов

    public int getCipher() {
        return cipher;
    }

    public int getCount() {
        return count;
    }

    public void setCipher(int cipher) {
        this.cipher = cipher;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Batch getBatchById(int id) {
        for (int i = 0; i < count; ++i) {
            if (batches[i].getId() == id)
                return batches[i];
        }
        return null;
    }

    public boolean addBatch(Batch batch) {
        if (batches.length == count)
            return false;
        for (int i = 0; i < count; ++i) {
            if (batches[i].getId() == batch.getId())
                return false;
        }

        batches[count] = batch;
        ++count;

        return true;
    }

    public boolean deleteBatchById(int id) {
        int i;
        for (i = 0; i < batches.length; ++i) {
            if (batches[i].getId() == id)
                break;
        }
        if (i == count)
            return false;

        for (i = i + 1; i < count; ++i) {
            batches[i - 1] = batches[i];
        }
        batches[i - 1] = null;
        --count;
        return true;
    }

    public double getAveragedPrice() {
        if (count == 0)
            return 0;

        double summaryPrice = 0;
        for (int i = 0; i < count; ++i) {
            summaryPrice += batches[i].getPrice();
        }
        return (summaryPrice / count);
    }

    public Batch[] getAboveAveragePriceBatches() {
        if (count == 0)
            return null;

        var aboveAveragePrice = new Batch[count];
        var avPrice = getAveragedPrice();
        int j = 0;
        for (int i = 0; i < count; ++i) {
            if (batches[i].getPrice() > avPrice) {
                aboveAveragePrice[j] = batches[i];
                ++j;
            }
        }

        Batch[] batches1 = new Batch[j];
        for (int i = 0; i < j; ++i) {
            batches1[i] = aboveAveragePrice[i];
        }
        return batches1;
    }

    public Batch[] getIntervalBatches(double bottom, double top) {
        var intervalBatches = new Batch[batches.length];
        int j = 0;
        for (int i = 0; i < batches.length; ++i) {
            if (batches[i].getPrice() >= bottom || batches[i].getPrice() <= top) {
                intervalBatches[i] = batches[i];
                ++j;
            }
        }

        var withoutNullReferences = new Batch[j];
        for (int i = 0; i < j; ++i) {
            if (intervalBatches != null) {
                withoutNullReferences[i] = intervalBatches[i];
            }
        }
        return withoutNullReferences;

    }

    public void batchOutput() {
        for (int i = 0; i < count; ++i) {
            System.out.printf("Деталь: код = %d, имя = %s, цена = %f, количество = %d \n",
                    batches[i].getId(), batches[i].getName(), batches[i].getPrice(), batches[i].getCount());
        }
    }

    public Batch[] SortBatch(int type) {
        if (type == 0) {
            return batches;
        }
        Batch t;
        if (type < 0) {
            for (int i = 0; i < count; i++) {
                for (int j = 0; j < count - 1; j++) {
                    if (batches[j].compareTo(batches[j + 1]) < 0) {
                        t = batches[j];
                        batches[j] = batches[j + 1];
                        batches[j + 1] = t;
                    }
                }
            }
        } else {
            for (int i = 0; i < count; i++) {
                for (int j = 0; j < count - 1; j++) {
                    if (batches[j].compareTo(batches[j + 1]) > 0) {
                        t = batches[j];
                        batches[j] = batches[j + 1];
                        batches[j + 1] = t;
                    }
                }
            }
        }
        return batches;
    }



    public Supply(){
        cipher = 0;
        count = 0;
    }

    public Supply(int cipher, int maxCount) {
        this.cipher = cipher;
        this.count = 0;
        batches = new Batch[maxCount];
    }

    public String toString(){
        return String.format(SUPPLY_FORMAT_STRING, cipher, count);
    }


}
