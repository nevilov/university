package main.Lab3;


public class SupplyDemo {
    public static void main(String[] args) {
        Batch[] batches = new Batch[]{
                new Batch(1, "Шуруп", 10, 1),
                new Batch(2, "Гайка", 10, 20),
                new Batch(3, "Винтик", 10, 30),
                new Batch(4, "Шуруп", 10, 4),
                new Batch(5, "Шестеренка", 10, 5)
        };

        Supply supply1 = new Supply(165, 10);
        Supply supply2 = new Supply(132, 10);
        supply2.setCipher(32);

        for(int i=0; i<5;++i){
            supply1.addBatch(batches[i]);
        }

        for(int i=4; i>=0;--i){
            supply2.addBatch(batches[i]);
        }


        System.out.printf("Сформированы следующие партии: \n");
        System.out.printf("supply1 %s \nsupply2 %s \n", supply1, supply2);

        supply1.addBatch(new Batch(321, "Саморез 3мм", 100, 2));
        supply2.addBatch(new Batch(3546, "Саморез 23мм", 1200, 2));


        System.out.printf("Поставка 1---\n");
        supply1.batchOutput();

        System.out.printf("Средняя цена %f\n", supply1.getAveragedPrice());
        System.out.println("Детали, цена которых выше средней");
        putBatches(supply1.getAboveAveragePriceBatches());

        System.out.printf("---\n");
        supply1.deleteBatchById(5);
        supply1.batchOutput(); // Удалились поставки с кодом 5

        System.out.printf("Поставка 2---\n");
        System.out.println();
        supply2.batchOutput();

        System.out.printf("Средняя цена %f\n", supply2.getAveragedPrice());
        System.out.println("Детали, цена которых выше средней");
        putBatches(supply2.getAboveAveragePriceBatches());

        System.out.printf("---\n");
        supply2.deleteBatchById(3);
        supply2.batchOutput(); // Удалились поставки с кодом 3

        System.out.printf("Проверка наличия поставок\n");
        int n = 1232;
        Batch b1 = supply1.getBatchById(n);
        if(b1 == null){
            System.out.printf("Такой поставки не существует\n");
        }
        else{
            System.out.println(b1);
        }

        n = 1;
        b1 = supply1.getBatchById(n);
        if(b1 == null){
            System.out.printf("Такой поставки не существует\n");
        }
        else{
            System.out.println(b1);
        }

        n = 5;
        Batch b2 = supply1.getBatchById(n);
        if(b2 == null){
            System.out.printf("Такой поставки не существует\n");
        }
        else{
            System.out.println(b2);
        }


        System.out.printf("Поставка 1 без сортировки---\n");
        supply1.SortBatch(0);
        supply1.batchOutput();
        System.out.printf("Поставка 1 сортировка по возрастанию---\n");
        supply1.SortBatch(1);
        supply1.batchOutput();
        System.out.printf("Поставка 2 сортировка по убыванию---\n");
        supply1.SortBatch(-1);
        supply1.batchOutput();


    }

    public static void putBatches(Batch[] batches){
        for(int i=0; i< batches.length; ++i){
            System.out.printf("Деталь: код = %d, имя = %s, цена = %f, количество = %d \n",
                    batches[i].getId(), batches[i].getName(), batches[i].getPrice(), batches[i].getCount());
        }
    }
}

