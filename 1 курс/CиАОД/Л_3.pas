﻿Program Stack2;
type  stack=array[1..100] of char;

procedure PUSH(var s:stack; var top:integer; item:char);
begin
        inc(top);
        s[top]:=item;
end;

procedure POP(var s:stack; var top:integer; var item:char);
begin
        item:=s[top];
        dec(top);
end;

function EMPTY(top:integer):boolean;
begin
        if top=0 then EMPTY:= true
        else EMPTY:= false;
end;

procedure write_static_stack(s:stack;top:integer);
var i:integer;
var elem:char;
begin
     i:=top;
     writeln;
     while not EMPTY(i) do
     begin
        POP(s,i,elem);
        write(elem,' ');
     end;
     readln;
end;

Var st:stack;
Var top:integer; {указатель на вершину стека}
var i:char;
var cnt: integer;
var elements: array[1..30] of char;
begin
top:=0;
cnt:=0;
for i:='a' to 'f' do  PUSH(st,top,i);
write_static_stack(st,top);

POP(st,top,elements[1]);
POP(st,top,elements[2]);
POP(st,top,elements[3]);

PUSH(st,top,'w');
PUSH(st,top,elements[2]);
PUSH(st,top,elements[1]);
write_static_stack(st,top);



for var j := 1 to 3 do begin
  POP(st,top,elements[j]);
  if(j=3) then PUSH(st,top,'w');
end;

for var j := 2 downto 1 do begin
  PUSH(st,top,elements[j]);
end;


write_static_stack(st,top);

end.
