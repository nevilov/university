﻿program Lab2;

Type Hash = ^PHash;
PHash = record
Data: integer; {Ключевое поле}
Next:hash;
End;

//Объявление таблицы:
Const m = 11; {Размерность таблицы}
type h_table=array [0..m-1] of hash;
Const taskArray:array[0..24] of integer=(16,22,8,14,24,18,5,10,12,2,13,12,12,14,17,11,7,19,23,4,6,15,19,2,7);
var i:integer;
var table: h_table; {хеш-таблица}

// Функия вычисления адреса в хеш-таблице по заданному ключу
function hash_func(key:integer) : integer;
begin
        hash_func:=key mod m;
end;

procedure write_List(L:hash);
var Q:hash;
begin
	Q:=L;
	writeln;
	while Q<>nil do
	begin
		write(Q^.Data,' ');
		Q:=Q^.next;
	end;
	writeln;
end;

// Процедура  вывода на экран хеш-таблицы
procedure write_h_table(table:h_table);
var i:integer;
begin
        for i:=0 to m-1 do
        if   table[i]=nil then writeln('*')
        else
             write_List(table[i]);
             
end;

// Процедура инициализации хеш-таблицы
procedure preparation_h_table(var table:h_table);
var i:integer;
begin
        for i:=0 to m-1 do
             table[i]:=nil;
end;

procedure find_Item_by_Key(var L:hash; item:integer; var T:hash);
begin
T:=L;
while T <> nil do
begin
if T^.Data=item then break;
T:=T^.next;
end;
end;

//Процедура добавления элемента в начало списка представлена ниже 
procedure addItem_to_Begin(var L:hash;item:integer);
var Q:hash;
begin
	new(Q);
	Q^.Data:=item;
	Q^.Next:=L;
	L:=Q;
end;
// ToEnd
procedure addItem_to_End(var L:hash; var T:hash; item:integer);
var Q:hash;
begin
        if L=nil then
        begin
                addItem_to_Begin(L,item);
                T:=L;
        end
        else
        begin
	        T:=L;
	        while T^.next<> nil do T:=T^.next;
	        new(Q);
	        Q^.Data:=item;
	        Q^.Next:=nil;
	        T^.Next:=Q;
	        T:=Q;
        end;
end;


// Процедура добавления ключа в хеш-таблицу
procedure add_to_hash(var table:h_table; key:integer);
var Q:integer;
var T:hash;
begin
    Q:= hash_func (key);
	  find_Item_by_Key(table[Q],key,T);
    if t<>nil then writeln('Ключ:',key,' существует')
    else
     addItem_to_End(table[Q],T,key);
     end;



begin
       
        preparation_h_table(table);
        for i:=0 to 23 do begin
          add_to_hash(table, taskArray[i]);
        end;        
        
        write_h_table(table);
end.
