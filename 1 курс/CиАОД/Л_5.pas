﻿program Project5;
const n=17000;	
type mas=array[1..n] of integer;


Procedure Print_Mas(var A:mas);
var i: integer;
begin
        writeln;
        for i:=1 to n do write(A[i],' ' );
        writeln;
end;

procedure ExchangeSort( var A: mas);
var i, j, x: integer;
begin
for i:= 2 to n do
	for j :=n downto i do
		if a[j-1] > a[j] then
		begin
			x :=a[j-1];	
			a[j-1] := a[j];
			a[j] :=x;
		end;
end;

procedure SelectionSort( var A: mas);
var i, j, q,k: integer;
begin

for i := 1 to n-1 do begin
k := i;
for j:=i+1 to n do
        if A[j]<A[k] then   k:=j;
q:=A[i]; A[i]:=A[k]; A[k]:=q;	
end;
end;



procedure InsertSort( var A: mas);
var i, j, Tmp: integer;
begin

for i := 2 to n do begin 
Tmp := A[i]; 	{Сохраняем текущий элемент}
j := i-1;
while(j >= 1)and (A[j] > Tmp) do
begin A[j+1] := A[j]; j := j-1; end;	{Сдвигаем элементы большие, чем текущий}
A[j+1] := Tmp;	{Вставляем текущий элемент}
end;
end;



Procedure Init_Mas(var A1,A2,A3:mas);
var i: integer;
begin
        for i:=1 to n do begin
        A1[i]:=random(1000); A2[i]:=A1[i]; A3[i]:=A2[i];
        end;
end;



var A,B,C:mas;	
var Start, Stop: cardinal; Elapsed: double;	
begin
 Init_Mas(A,B,C);
 writeln('Количество элементов ', n);

 Start:=Milliseconds; 
 InsertSort(A);
 Stop:=Milliseconds; 
 Elapsed:=Stop-Start; 
 writeln('Время сортировки вставками = ' ,Elapsed, ' миллисекунд = ', Elapsed/1000, ' секунд');
 
 Start:=Milliseconds; 
 SelectionSort(B);
 Stop:=Milliseconds; 
 Elapsed:=Stop-Start; 
 writeln('Время сортировки выборкой = ' ,Elapsed, ' миллисекунд = ', Elapsed/1000, ' секунд');
 
 Start:=Milliseconds; 
 ExchangeSort(C);
 Elapsed:=Stop-Start; 
 writeln('Время сортировки пузырьком = ' ,Elapsed, ' миллисекунд = ', Elapsed/1000, ' секунд');
 

end.
