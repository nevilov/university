﻿Program L4_2;

	Const m = 11;
	const m_tab=24;
	Const c=1;  
	var i: integer;
	type h_table=array [0..m_tab] of integer;
 var table:h_table;  {хеш-таблица}
Const taskArray:array[0..24] of integer=(16,22,8,14,24,18,5,10,12,2,13,12,12,14,17,11,7,19,23,4,6,15,19,2,7);

// Функия вычисления адреса в хеш-таблице по заданному ключу
function hash_func(key:integer) : integer;
begin
        hash_func:=key mod m;
end;

// Процедура  вывода на экран хеш-таблицы
procedure write_h_table(table:h_table);
var i:integer;
begin
        for i:=0 to m_tab-1 do write(table[i],' ');
             end;

// Процедура инициализации хеш-таблицы
procedure preparation_h_table(var table:h_table);
var i:integer;
begin
        for i:=0 to m_tab-1 do
             table[i]:=-1;  {Инициализация хеш-таблицы производится элементом -1}
end;





// Процедура добавления ключа в хеш-таблицу
procedure add_to_table(var table:h_table; key:integer);
var h0,hi,i,dist:integer;
Label m1;
begin
    h0:=hash_func(key);
    i:=0;
    hi:=h0;
m1:
    if table[hi]=-1 then table[hi]:=key
    else
        if table[hi]=key then writeln(key,' the item is founded')
        // Коллизия
        else
        begin
                i:=i+1;
                dist:=c*i;
                hi:=(h0+dist) mod m_tab;
                goto m1;


        end;
end;



begin
        preparation_h_table(table);
        for i:=0 to 23 do begin
          add_to_table(table, taskArray[i]);
        end;        
        
        write_h_table(table);
end.
