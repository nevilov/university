﻿program Lab2;
type
PList = ^MyList;
MyList = record
	Data : char;
	Next : PList;
end;
Var L:PList; 	{указатель на первый элемент}
T:Plist; 	{указатель на текущий элемент}
k:integer;
key:char;



procedure find_Item_by_Key(L:PList; item:char; var T:PList);
begin
T:=L;
while T<> nil do
begin
if T^.Data=item then break;
T:=T^.next;
end;
end;


procedure add_to_Middle(var T:PList; item:char);
var Q:PList;
begin
  new(Q);
  Q^.data:=item;
  Q^.next:=T^.next;
  T^.next:=Q;
  T:=Q;
end;


//Процедура добавления элемента в начало списка представлена ниже 
procedure addItem_to_Begin(var L:PList;item:char);
var Q:PList;
begin
	new(Q);
	Q^.Data:=item;
	Q^.Next:=L;
	L:=Q;
end;

procedure get_Number_Current(L,T:PList;var n:integer);
var Q:PList;
begin
  n:=1;
  Q:=L;
  while Q<>T do
  begin
  Q:=Q^.Next;
  n:=n+1;
  end;
end;

//Процедура добавление элемента в конец  списка.  
procedure addItem_to_End(var L:PList; var T:PList; item:char);
var Q:PList;
begin
        if L=nil then
        begin
                addItem_to_Begin(L,item);
                T:=L;
        end
        else
        begin
	        T:=L;
	        while T^.next<> nil do T:=T^.next;
	        new(Q);
	        Q^.Data:=item;
	        Q^.Next:=nil;
	        T^.Next:=Q;
	        T:=Q;
        end;
end;
  
//Процедура вывода элементов списка на экран (консоль).
procedure write_List(L:PList);
var Q:PList;
begin
	Q:=L;
	writeln;
	while Q<>nil do
	begin
		write(Q^.Data,' ');
		Q:=Q^.next;
	end;
	writeln;
	readln;
end;

//Найти элемент по номеру
procedure find_Item_by_Number(L:PList; n:byte; var T:PList);
var k:byte;
begin
	T:=L;
	k:=0;
	while T<> nil do 
	begin
		k:=k+1;
		if k=n then break;
		T:=T^.next;
	end;
end;

//Удалить элемент по номеру
procedure delete_by_Number(var L:PList; n:integer);
var Q,T:PList;
begin
	if n=1 then
	begin
		Q:=L;
		L:=L^.next;

	end
	else
	begin
		n:=n-1;
 	find_Item_by_Number(L,n,T );
		Q:=T^.next;
		T^.next:=Q^.next;

	end;
	Dispose(Q);
end; 


//Тело программы
begin
  
  L:=nil;
  addItem_to_End(L, T, '4');
  addItem_to_End(L, T, 's');
  addItem_to_End(L, T, 'i');
  addItem_to_End(L, T, '2');
  addItem_to_End(L, T, '6');
  addItem_to_End(L, T, '8');
  addItem_to_End(L, T, 'a');
  addItem_to_End(L, T, 'z');
  addItem_to_End(L, T, 'c');
  addItem_to_End(L, T, 'E');
  
  write_List(L);
  delete_by_Number(L, 6);
  write_List(L);
  
  WriteLn('Введите ключ, элемент вставится через 2 элемента после ключа');
  read(key);
  
  find_Item_by_Key(L,key,T);

  get_Number_Current(L,T,k);
  k+=2;
  find_Item_by_Number(L, k, T);
  
  add_to_Middle(T, '$');
  write_List(L);
  
  end.