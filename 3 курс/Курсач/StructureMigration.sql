create database Bank;
use Bank;


create table Banks(
	Id int auto_increment,
    `Name` varchar(250) not null,
	Address varchar(250) not null,
    Phone varchar(30) not null,
    
    primary key(Id)
);


create table Employees(
	Id int auto_increment,
    PassSeries varchar(30) not null,
    PassNum varchar(30) not null,
    Phone varchar(30) not null,
    DateOfBirthday date not null,
    Inn varchar(50) not null,
    `Name` varchar(250) not null,
    Address varchar(250) not null,
    Snils varchar(50) not null,
    
	primary key(Id),
    constraint UQ_Employees_Passport unique key(PassSeries, PassNum),
    constraint UQ_Employees_Snils unique key(Snils),
    constraint UQ_Employees_Inn unique key(Inn)
);

create table Positions(
	Id int auto_increment,
    `Name` varchar(250) not null,
    Salary double not null default 60000,
    ApprovalDate datetime not null,
    
	primary key(Id)
);

create table BankEmployee(
	BankId int,
	EmployeeId int,
    PositionId int,
    
    primary key(BankId, EmployeeId),
    constraint FK_BankEmployee_Bank foreign key(BankId) 
    references Banks(Id)
    on delete cascade
    ,
    constraint FK_BankEmployee_Employee foreign key(EmployeeId) 
    references Employees(Id)
    on delete cascade,
    
    constraint FK_BankEmployee_Position foreign key(PositionId)
    references Positions(Id)
);

create table PercentRates(
	Percent double,
    MinTerm double not null default 365,
    
    primary key(Percent)
);

create table DepositTypes(
	Id int auto_increment,
    `Name` varchar(250) not null,
    Percent double,
    ApprovalDate datetime not null,
    
    primary key(Id),
    constraint FK_DepositTypes foreign key(Percent)
    references PercentRates(Percent)
);

create table Investors(
	Id int auto_increment,
    PassSeries varchar(30) not null,
    PassNum varchar(30) not null,
    Phone varchar(30) not null,
    DateOfBirthday date not null,
    Inn varchar(50) not null,
    `Name` varchar(250) not null,
    Address varchar(250) not null,
    Snils varchar(50) not null,
    
	primary key(Id),
	constraint UQ_Employees_Passport unique key(PassSeries, PassNum),
    constraint UQ_Employees_Snils unique key(Snils),
    constraint UQ_Employees_Inn unique key(Inn)
);

create table Accounts(
	Id int auto_increment,
    InvestorId int,
    CreatedDate datetime not null,
    BankId int,
    
	primary key(id),
    constraint FK_Account_Investor foreign key(InvestorId)
    references Investors(Id),
    constraint FK_Account_Bank foreign key(BankId)
    references Banks(Id)
);

create table Deposits(
	Id int auto_increment,
    CreatedDate datetime not null,
    EmployeeId int,
    Amount double not null,
    AccountId int,
    DepositTypeId int,
    
    primary key(Id),
    constraint FK_Deposits_Employee foreign key(EmployeeId)
    references Employees(Id),
    constraint FK_Deposits_Account foreign key(AccountId)
    references Accounts(Id),
    constraint FK_Deposits_DepositType foreign key(DepositTypeId)
    references DepositTypes(Id),
    
	constraint UQ_Employees_Alternative unique key(EmployeeId, AccountId, DepositTypeId)
);

create table Fines(
	Id int auto_increment,
    Percent double not null,
    `Description` text not null,
    LockAccount bool not null default 0,
    
    primary key(Id)
);

create table FinesDeposits(
	DepositId int,
    FinesId int,
    
    primary key(DepositId, FinesId),
    constraint FK_FinesDeposits_Deposit foreign key(DepositId)
    references Deposits(Id)
    on delete cascade,
    constraint FK_FinesDeposits_Fines foreign key(FinesId)
    references Fines(Id)
    on delete cascade
);

