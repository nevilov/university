use bank;
create view BankView as
select `Name`, Address, Phone from banks;
-- Банки
insert into BankView values 
('ОО No 1', 'г.Севастополь ул. Красная 7', '+7971251635'),
('ОО No 2', 'г. Симферополь ул. Кирова 6', '+7971251635'),
('ОО No 3', 'г.Севастополь ул. Жизни 16а', '+7971251635'),
('ОО No 4', 'г.Ялта ул. Главная 8', '+7971656833'),
('OO No 5', 'г.Керчь ул. Парашутная 5', '+7783525353'),
('OO No 6', 'г.Ялта ул. Фисенская 1', '+77835253213'),
('OO No 7', 'г.Саки ул. Вечная 10', '+7782425353'),
('OO No 8 Важный', 'г.Симферополь ул. Крылова 11', '+7782355353'),
('OO No 9', 'г.Алупка ул. Крылова 13', '+7782355353'),
('OO No 10', 'г.Феодосия ул. Крылова 15', '+7782355353');
select * from BankView;

-- Проценты
select * from percentRates;

create view PercentRatesView as
select Percent, MinTerm from PercentRates;

insert into PercentRatesView values
(15.6, 360),
(10, 180),
(30, 500),
(20, 400);

select * from PercentRatesView;

-- Тип вклада
select * from deposittypes;
create view DepositTypesView as
select `Name`, Percent, ApprovalDate from deposittypes;

insert into DepositTypesView values
('Выгодный', 20, curdate()),
('Cтабильный', 10, curdate()),
('Максимальный', 30, curdate()),
('Выгодный пенсионный', 15.6, curdate());

select * from DepositTypesView;

-- Сотрудник
select PassSeries, PassNum, Phone, DateOfBirthday, Inn, `Name`, Address, Snils, datediff(curdate(), DateOfBirthday) from employees
where datediff(curdate(), DateOfBirthday) >= 6570;
select * from employees;

create view EmployeesView as
select PassSeries, PassNum, Phone, DateOfBirthday, Inn, `Name`, Address, Snils from employees
where datediff(curdate(), DateOfBirthday) >= 6570 with check option;

insert into EmployeesView
values
('15234', '6243362','+7 (941) 456-25-24','2010-11-11', '5334653262346', 'Меркушев Лаврентий Тихонович', 'г. Рязань, Южная ул., д. 9 кв.13', '1251253');

insert into EmployeesView
values
('1524', '624362','+7 (941) 456-25-24','1993-11-11', '534653262346', 'Меркушев Лаврентий Тихонович', 'г. Рязань, Южная ул., д. 9 кв.13', '125125'),
('3261', '749567','+7 (941) 456-36-12','1992-06-25', '234614361464', 'Алексеев Лев Филатович', 'г. Назрань, Речной пер., д. 7 кв.188', '2346234'),
('1235', '543783','+7 (941) 456-12-24','1985-02-12', '467847637556', 'Шестаков Сергей Максимович', 'г. Химки, Полесская ул., д. 22 кв.88', '23462346'),
('4357', '341264','+7 (941) 978-12-24','1967-03-24', '456456546541', 'Савельев Андрей Львович', 'г. Химки, Полесская ул., д. 22 кв.88', '1235176'),
('7568', '864546','+7 (941) 345-78-24','1964-11-25', '123786765464', 'Мамокин Дмитрий Дмитриевич', 'г. Симферополь, Полесская ул., д. 21 кв.88', '1251235'),
('2456', '456456','+7 (941) 678-12-789','1976-12-01', '445645612378', 'Юлианов Денис Алексеевич', 'г. Химки, Полесская ул., д. 22 кв.88', '1463426'),
('2346', '789351','+7 (941) 456-45-24','1972-03-01', '789784264563', 'Живой Савелий Ильич', 'г. Севастополь, Полесская ул., д. 22 кв.88', '1461235'),
('3451', '789754','+7 (941) 213-45-78','2000-07-12', '456456789313', 'Простой Григорий Владиславович', 'г. Химки, Полесская ул., д. 22 кв.88', '123621'),
('5467', '456456','+7 (941) 456-12-24','2001-06-16', '456789321356', 'Спать Максим Владимирович', 'г. Химки, Полесская ул., д. 22 кв.88', '234624'),
('8564', '456421','+7 (941) 214-64-45','2001-04-17', '789756431234', 'Хочется Дмитрий Валерьевич', 'г. Химки, Полесская ул., д. 22 кв.88', '523471'),
('6273', '543783','+7 (941) 456-12-24','1921-01-14', '456789789878', 'Богатый Михаил Анатольевич', 'г. Химки, Полесская ул., д. 22 кв.88', '1265634'),
('5780', '867036','+7 (941) 325-26-23','1952-04-11', '435234523455', 'Максимов Аполлон Кириллович', 'г. Ангарск, Заречный пер., д. 15 кв.58', '1235135');

select * from EmployeesView;

-- Должность
select * from positions;
create view PositionsView as
select `Name`, Salary, ApprovalDate from positions;

insert into PositionsView values
('Менеджер', 60000, curdate()),
('Оператор', 50000, curdate()),
('Директор', 190000, curdate()),
('Бухгалтер', 80000, curdate());

select * from PositionsView;

-- БанкСотрудник
select * from bankemployee;
create view BankEmployeeView as
select BankId, EmployeeId, PositionId from bankemployee;

insert into BankEmployeeView values
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(3, 4, 4),
(4, 5, 1),
(5, 6, 1),
(3, 7, 1),
(4, 8, 1);

select * from BankEmployeeView;

-- Вкладчик
 select * from investors;
create view InvestorsView as
select PassSeries, PassNum, Phone, DateOfBirthday, Inn, `Name`, Address, Snils from investors
where datediff(curdate(), DateOfBirthday) >= 6570 with check option;

insert into InvestorsView
values
('1524', '256265','+7 (941) 456-25-24','1993-11-11', '534653262346', 'Гуляев Александр Богданович', 'г. Рязань, Южная ул., д. 9 кв.13', '125125'),
('3253', '234613','+7 (941) 456-36-12','1992-06-25', '234614361464', 'Алексеев Дмитрий Дамирович', 'г. Назрань, Речной пер., д. 7 кв.188', '23514361'),
('6325', '653656','+7 (941) 456-12-24','1985-02-12', '467847637556', 'Шестаков Сергей Максимович', 'г. Химки, Полесская ул., д. 22 кв.88', '2346146'),
('3241', '362345','+7 (941) 978-12-24','1967-03-24', '456456546541', 'Большаков Дмитрий Савельевич', 'г. Химки, Полесская ул., д. 22 кв.88', '12361243'),
('4234', '735675','+7 (941) 345-78-24','1864-11-25', '123786765464', 'Мамокин Дмитрий Дмитриевич', 'г. Симферополь, Полесская ул., д. 21 кв.88', '62346134'),
('3534', '456456','+7 (941) 678-12-789','1976-12-01', '445645612378', 'Моисеев Александр Евгеньевич', 'г. Химки, Полесская ул., д. 22 кв.88', '12351235'),
('2365', '789351','+7 (941) 456-45-24','1972-03-01', '789784264563', 'Орлова Варвара Алиевна', 'г. Севастополь, Полесская ул., д. 22 кв.88', '12341236'),
('6346', '789754','+7 (941) 213-45-78','2000-07-12', '456456789313', 'Тихомирова Таисия Марковна', 'г. Химки, Полесская ул., д. 22 кв.88', '6234634'),
('6584', '456456','+7 (941) 456-12-24','2001-06-16', '456789321356', 'Спать Максим Владимирович', 'г. Химки, Полесская ул., д. 22 кв.88', '23462346'),
('3573', '456421','+7 (941) 214-64-45','2001-04-17', '789756431234', 'Орлова Варвара Алиевна', 'г. Химки, Полесская ул., д. 22 кв.88', '275426243'),
('4362', '543783','+7 (941) 456-12-24','1921-01-14', '456789789878', 'Богатый Михаил Анатольевич', 'г. Химки, Полесская ул., д. 22 кв.88', '1614614'),
('2634', '867036','+7 (941) 325-26-23','1942-04-11', '435234523455', 'Ефимов Константин Максимович', 'г. Ангарск, Заречный пер., д. 15 кв.58', '13741345');

insert into InvestorsView
values
('15242', '2562645','+7 (941) 425-65-24','2000-11-11', '531264364134', 'Богданский Тихомир Игоревич', 'г. Рязань, Красная ул., д. 1 кв.2', '2415215');
select * from InvestorsView;

-- Счет
select * from accounts;
create view AccountsView as
select InvestorId, CreatedDate, BankId from accounts;

insert into AccountsView values
(1, curdate(), 1),
(1, curdate(), 2),
(2, curdate(), 3),
(1, curdate(), 4),
(5, curdate(), 3),
(6, curdate(), 1),
(9, curdate(), 1),
(7, curdate(), 1),
(8, curdate(), 1),
(9, curdate(), 5),
(4, curdate(), 4);

select * from AccountsView;

-- Вклад
select * from deposits;

create view DepositsView1 as
select CreatedDate, EmployeeId, Amount, AccountId, DepositTypeId from Deposits
where date(CreatedDate) = curdate() with check option;
drop view DepositsView1;

insert into DepositsView1 values
(now(), 1, 500000, 3, 1);

create view DepositsView as
select CreatedDate, EmployeeId, Amount, AccountId, DepositTypeId from Deposits;
select curdate();
select now();
insert into DepositsView values
(now(), 1, 50000, 1, 1),
(now(), 2, 80000, 2, 2),
(now(), 3, 100000, 3, 3),
(now(), 4, 300000, 4, 4),
(now(), 5, 501000, 5, 1),
(now(), 6, 548000, 1, 2),
(now(), 7, 1000000, 6, 3),
(now(), 5, 8210000, 1, 4),
(now(), 6, 15000, 7, 1),
(now(), 5, 350000, 2, 2),
(now(), 8, 250000, 5, 3),
(now(), 2, 600000, 4, 4),
(now(), 3, 5550000, 5, 1);

select * from DepositsView;

-- Штрафные санкции
select * from fines;

create view FinesView as
select Percent, Description, LockAccount from Fines;

insert into FinesView values
(10, 'Снятие средств за месяц до окончания вклада', 0),
(20, 'Снятие средств за два месяца до окончания вклада', 0),
(30, 'Снятие средств за три месяца до окончания вклада', 0),
(100, 'Снятие средств за пол-года до окончания вклада', 1);

select * from FinesView;

-- Штрафные санкции вкладalter
select * from FinesDeposits;

create view FinesDepositsView as
select DepositId, FinesId from FinesDeposits;

insert into FinesDepositsView values
(1, 1),
(2, 2);

select * from FinesDepositsView;

-- Тестирование бд

select * from banks;
update banks
set Name = 'OO No8'
where Id = 8;

delete from banks
where Id = 10;


select * from employees;
update employees
set PassSeries = '1111', PassNum = '555555'
where Id = 10;

delete from employees
where Id = 1;



select * from bankemployee;
update bankemployee
set PositionId = 3
where EmployeeId = 4;

delete from bankemployee
where EmployeeId = 2;


select * from positions;
update positions
set Name = 'Оператор_1'
where Name = 'Оператор';

delete from positions
where Name ='Бухгалтер';


insert into deposittypes (Id, Name, Percent, ApprovalDate)
values (5, 'Молодежный', 10, curdate());
select * from deposittypes;

update deposittypes
set Percent = 30
where Name = "Выгодный";

delete from deposittypes
where Id = 2;


select * from investors;
update investors
set Phone = '+7 (999) 999-99-99'
where Id = 1;

delete from investors
where Id = 10;


select * from accounts;
update accounts
set BankId = 5
where InvestorId = 5;

delete from accounts
where Id = 11;

select * from deposits;
update deposists
set Id = 7
where Amount = 890000;

delete from deposits
where Id = 1;

select * from finesdeposits;


-- Выборка
select * from deposits;
insert into deposits values(14, curdate(), 3, 10000, 3, 2);
insert into deposits values(15, '2010-10-12', 3, 100000, 3, 2);
insert into deposits values(16, '2012-10-12', 3, 100000, 3, 2);

select curdate() + 360;
select i.Name, a.Id, d.Amount, d.CreatedDate, pr.Percent, date_add(d.CreatedDate, INTERVAL pr.MinTerm DAY) as fd from investors i
inner join accounts a on a.InvestorId = i.Id
inner join deposits d on d.AccountId = a.Id
inner join deposittypes dt on dt.Id = d.DepositTypeId
inner join percentrates pr on pr.Percent = dt.Percent
where i.Id = 2 and (date_add(d.CreatedDate, INTERVAL pr.MinTerm DAY)) < curdate();

select sum(d.Amount * pr.Percent) from investors i
inner join accounts a on a.InvestorId = i.Id
inner join deposits d on d.AccountId = a.Id
inner join deposittypes dt on dt.Id = d.DepositTypeId
inner join percentrates pr on pr.Percent = dt.Percent
where i.Id = 2 and (date_add(d.CreatedDate, INTERVAL pr.MinTerm DAY)) < curdate();

select* from deposits d
inner join accounts a on d.AccountId = a.Id;

select b.Name, sum(d.Amount) as Amount from deposits d
inner join accounts a on d.AccountId = a.Id
inner join banks b on b.Id = a.Id
where d.CreatedDate < '2024-01-01' and d.CreatedDate > '2010-01-01'
group by b.Id
order by sum(d.Amount) desc;

select i.Name, a.Id from investors i
inner join accounts a on a.InvestorId = i.Id
where not exists (select * from deposits d where d.AccountId = a.Id);

select i.Name, b.Name from investors i
inner join accounts a on a.InvestorId = i.Id
inner join banks b on b.Id = a.BankId
where exists (select * from deposits d where d.AccountId = a.Id);

select e.Name from employees e
where not exists (select * from deposits where deposits.EmployeeId = e.Id);


select b.Name, count(be.BankId) from Banks b
inner join bankemployee be on be.BankId = b.Id
group by b.Name;

select * from banks where Name = 'ОО No 1';
update banks
set Id = 14
where Name = 'ОО No 1';

select * from accounts;
update accounts
set Id = 100
where Id = 1;

select f.Description, f.LockAccount, f.Percent, d.Amount, i.Name from finesdeposits fd
join fines f on f.Id = fd.FinesId
join deposits d on d.Id = fd.DepositId
join accounts a on a.Id = d.AccountId
join investors i on i.Id = a.InvestorId;

-- Директора банка 
select *from bankemployee be;
select Name, PassSeries, PassNum from employees
where Id in (
	select be.employeeId from bankemployee be
	join positions p on p.Id = be.PositionId
	where p.Name like '%Директор%');
--
update Investors
set Name = 'Борщ Андрей Дмитриевич'
where Name like '%Шестаков%';

update Investors
set Name = 'Дриус Андрей Викторович'
where Name like '%Хочется%';

update Investors
set Name = 'Снежок Павел Викторович'
where Name like '%Спать%';
--
select i.Name, count(d.Id) as `Количество вкладов` from investors i
join accounts a on a.InvestorId = i.Id
join deposits d on d.AccountId = a.Id
group by i.Id;

select * from deposits;
select * from accounts;