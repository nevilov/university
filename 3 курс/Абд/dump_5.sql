-- MySQL dump 10.13  Distrib 8.0.31, for Win64 (x86_64)
--
-- Host: localhost    Database: data_centres
-- ------------------------------------------------------
-- Server version	8.0.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accessories`
--

DROP TABLE IF EXISTS `accessories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accessories` (
  `name` varchar(500) NOT NULL,
  `machine_id` int NOT NULL,
  `price` decimal(2,0) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`name`,`machine_id`),
  KEY `fk_accessories_machine_idx` (`machine_id`),
  CONSTRAINT `fk_accessories_machine` FOREIGN KEY (`machine_id`) REFERENCES `machine` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accessories`
--

LOCK TABLES `accessories` WRITE;
/*!40000 ALTER TABLE `accessories` DISABLE KEYS */;
/*!40000 ALTER TABLE `accessories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_center`
--

DROP TABLE IF EXISTS `data_center`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_center` (
  `code` int NOT NULL,
  `insitution` varchar(100) NOT NULL,
  `city` varchar(45) NOT NULL,
  `street` varchar(45) NOT NULL,
  `house` varchar(45) NOT NULL,
  `appartament` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_center`
--

LOCK TABLES `data_center` WRITE;
/*!40000 ALTER TABLE `data_center` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_center` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_center_service`
--

DROP TABLE IF EXISTS `data_center_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_center_service` (
  `id` int NOT NULL,
  `data_center_code` int NOT NULL,
  `position_name` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `midlle_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_data_center_idx` (`data_center_code`),
  KEY `fk_position_idx` (`position_name`),
  CONSTRAINT `fk_data_center` FOREIGN KEY (`data_center_code`) REFERENCES `data_center` (`code`),
  CONSTRAINT `fk_position` FOREIGN KEY (`position_name`) REFERENCES `data_center_service_position` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_center_service`
--

LOCK TABLES `data_center_service` WRITE;
/*!40000 ALTER TABLE `data_center_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_center_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_center_service_position`
--

DROP TABLE IF EXISTS `data_center_service_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `data_center_service_position` (
  `name` varchar(50) NOT NULL,
  `salary` varchar(45) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_center_service_position`
--

LOCK TABLES `data_center_service_position` WRITE;
/*!40000 ALTER TABLE `data_center_service_position` DISABLE KEYS */;
/*!40000 ALTER TABLE `data_center_service_position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `machine`
--

DROP TABLE IF EXISTS `machine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `machine` (
  `id` int NOT NULL,
  `data_center_code` int NOT NULL,
  `bough_at` datetime NOT NULL,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_machine_data_center_idx` (`data_center_code`),
  CONSTRAINT `fk_machine_data_center` FOREIGN KEY (`data_center_code`) REFERENCES `data_center` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `machine`
--

LOCK TABLES `machine` WRITE;
/*!40000 ALTER TABLE `machine` DISABLE KEYS */;
/*!40000 ALTER TABLE `machine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `machine_service`
--

DROP TABLE IF EXISTS `machine_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `machine_service` (
  `id` int NOT NULL,
  `machine_id` int NOT NULL,
  `position_name` varchar(50) NOT NULL,
  `name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `midlle_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_machine_service_position_idx` (`position_name`),
  KEY `fk_machine_service_data_center_idx` (`machine_id`),
  CONSTRAINT `fk_machine_service_data_center` FOREIGN KEY (`machine_id`) REFERENCES `machine` (`id`),
  CONSTRAINT `fk_machine_service_position` FOREIGN KEY (`position_name`) REFERENCES `machine_service_position` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `machine_service`
--

LOCK TABLES `machine_service` WRITE;
/*!40000 ALTER TABLE `machine_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `machine_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `machine_service_position`
--

DROP TABLE IF EXISTS `machine_service_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `machine_service_position` (
  `name` varchar(50) NOT NULL,
  `salary` varchar(45) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `machine_service_position`
--

LOCK TABLES `machine_service_position` WRITE;
/*!40000 ALTER TABLE `machine_service_position` DISABLE KEYS */;
/*!40000 ALTER TABLE `machine_service_position` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `software`
--

DROP TABLE IF EXISTS `software`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `software` (
  `id` int NOT NULL,
  `data_center_code` int NOT NULL,
  `firm` varchar(45) NOT NULL,
  `software_date` datetime NOT NULL,
  `bough_at` datetime NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_software_data_center_idx` (`data_center_code`),
  CONSTRAINT `fk_software_data_center` FOREIGN KEY (`data_center_code`) REFERENCES `data_center` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `software`
--

LOCK TABLES `software` WRITE;
/*!40000 ALTER TABLE `software` DISABLE KEYS */;
/*!40000 ALTER TABLE `software` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `software_service`
--

DROP TABLE IF EXISTS `software_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `software_service` (
  `id` int NOT NULL,
  `software_id` int NOT NULL,
  `name` varchar(45) NOT NULL,
  `position_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `midlle_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_software_service_software_idx` (`software_id`),
  KEY `fk_software_service_poistion_idx` (`position_name`),
  CONSTRAINT `fk_software_service_poistion` FOREIGN KEY (`position_name`) REFERENCES `software_service_position` (`name`),
  CONSTRAINT `fk_software_service_software` FOREIGN KEY (`software_id`) REFERENCES `software` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `software_service`
--

LOCK TABLES `software_service` WRITE;
/*!40000 ALTER TABLE `software_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `software_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `software_service_position`
--

DROP TABLE IF EXISTS `software_service_position`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `software_service_position` (
  `name` varchar(50) NOT NULL,
  `salary` decimal(2,0) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `software_service_position`
--

LOCK TABLES `software_service_position` WRITE;
/*!40000 ALTER TABLE `software_service_position` DISABLE KEYS */;
/*!40000 ALTER TABLE `software_service_position` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-05-06  8:42:03
