create database EmployeeDismissal;
use EmployeeDismissal;

create table Employee(
Id int auto_increment,
LastName varchar(50) not null,
FirstName varchar(50) not null,
MiddleName varchar(50) not null,
Position varchar(50) not null,
Subdivision varchar(50) not null,
DateOfReceipt datetime not null default CURRENT_TIMESTAMP,

primary key(Id)
);

create table DismissalArticle(
Id int auto_increment,
`Name` varchar(50) not null,
Reason varchar(250) not null,
NumArticle varchar(50) not null,
DefaultCompensation double not null,

primary key(Id)
);

create table Document(
Id int auto_increment,
DismissalArticleId int not null,
EmployeeId int not null,
DocumentNum varchar(50) not null,
RegistrationDate datetime not null default CURRENT_TIMESTAMP,
DismissalDate datetime not null default CURRENT_TIMESTAMP,
Compensation double not null null,

primary key(Id),
constraint FK_DismissalArticleId_Document foreign key(DismissalArticleId) references DismissalArticle(Id),
constraint FK_EmployeeId_Document foreign key(EmployeeId) references Employee(Id)
);

INSERT INTO Employee (Id, LastName, FirstName, MiddleName, Position, Subdivision, DateOfReceipt)
VALUES (1, 'Иванов', 'Петр', 'Сергеевич', 'Директор', 'Головной', '2020-01-01 00:00:00'),
       (2, 'Сидоров', 'Алексей', 'Иванович', 'Менеджер', 'Блок А', '2021-02-15 10:30:00'),
       (3, 'Петров', 'Андрей', 'Александрович', 'Оператор', 'Блок Б', '2022-01-05 08:15:00'),
       (4, 'Борисов', 'Игорь', 'Николаевич', 'Менеджер', 'Блок А', '2021-04-10 14:00:00'),
        (5, 'Кузнецова', 'Ольга', 'Алексеевна', 'Оператор', 'Блок Б', '2022-07-20 08:00:00'),
        (6, 'Новиков', 'Андрей', 'Дмитриевич', 'Менеджер', 'Блок А', '2022-02-15 11:30:00'),
        (7, 'Козлов', 'Дмитрий', 'Александрович', 'Менеджер', 'Блок А', '2022-03-01 09:00:00'),
        (8, 'Громов', 'Сергей', 'Игоревич', 'Оператор', 'Блок Б', '2022-05-10 07:45:00'),
        (9, 'Соколова', 'Екатерина', 'Дмитриевна', 'Менеджер', 'Блок А', '2022-01-15 14:30:00'),
        (10, 'Кулагина', 'Ольга', 'Викторовна', 'Оператор', 'Блок Б', '2023-02-10 08:30:00');

-- Insert statements for DismissalArticle table
INSERT INTO DismissalArticle (Id, Name, Reason, NumArticle, DefaultCompensation)
VALUES (1, 'Сокращение численности сотрудников', 'Экономические причины', 'Статья 83', 10000),
       (2, 'Нарушение дисциплины', 'Пропуск работы без уважительной причины', 'Статья 192', 5000),
       (3, 'Соглашение сторон', 'Увольнение по соглашению сторон', 'Статья 79', 15000),
       (4, 'Неудовлетворительная работа', 'Несоответствие профессиональным требованиям', 'Статья 81', 8000),
       (5, 'Нарушение правил внутреннего трудового распорядка', 'Постоянное нарушение графика работы', 'Статья 192', 7000),
       (6, 'Причины, не связанные с работой', 'Семейные обстоятельства', 'Статья 83', 15000);

-- Insert statements for Document table
INSERT INTO Document (Id, DismissalArticleId, EmployeeId, DocumentNum, RegistrationDate, DismissalDate, Compensation)
VALUES (1, 1, 1, '001', '2021-05-01 14:30:00', '2021-06-15 12:00:00', 8000),
       (2, 2, 2, '002', '2022-03-15 10:00:00', '2022-03-20 16:45:00', 3000),
       (3, 3, 3, '003', '2023-01-10 09:15:00', '2023-02-01 14:30:00', 12000),
       (4, 2, 4, '004', '2021-08-20 16:00:00', '2021-08-30 10:00:00', 4000),
       (5, 1, 5, '005', '2022-06-05 11:45:00', '2022-07-01 09:30:00', 9000),
       (6, 4, 6, '006', '2023-01-02 10:30:00', '2023-01-15 16:00:00', 6000),
       (7, 5, 7, '007', '2022-05-15 13:45:00', '2022-05-20 09:00:00', 4000),
       (8, 6, 8, '008', '2022-12-10 11:00:00', '2022-12-31 17:30:00', 12000),
       (9, 4, 9, '009', '2021-11-02 08:00:00', '2021-11-15 13:00:00', 5000),
       (10, 5, 10, '010', '2021-09-20 16:30:00', '2021-09-25 10:15:00', 3000);
       
INSERT INTO Employee (LastName, FirstName, MiddleName, Position, Subdivision, DateOfReceipt)
VALUES ('Алексеев', 'Михаил', 'Павлович', 'Менеджер', 'Блок А', '2021-01-01 00:00:00'),
('Беляева', 'Анастасия', 'Сергеевна', 'Оператор', 'Блок Б', '2021-02-15 00:00:00'),
('Васильев', 'Александр', 'Иванович', 'Директор', 'Головной', '2021-03-01 00:00:00'),
('Горбунов', 'Илья', 'Александрович', 'Менеджер', 'Блок А', '2021-04-15 00:00:00'),
('Данилова', 'Екатерина', 'Алексеевна', 'Оператор', 'Блок Б', '2021-05-01 00:00:00'),
('Егоров', 'Артем', 'Андреевич', 'Оператор', 'Блок А', '2021-06-15 00:00:00');

-- 1
select Name, Reason from DismissalArticle;

-- 2 Получить всех сотрудников, принятых в 2023 году
select * from Employee where DateOfReceipt >= '2023-01-01';

-- 3 Получить названия статей и причины увольнений для которых тарифная ставка больше 
select `Name`, Reason from DismissalArticle
where DefaultCompensation > 5000;

-- 4 получить всех сотрудников, для которых название должности является либо «Директор», либо «Менеджер» или денежная компенсация составляет более 5 000р - Объединение
select Id from Employee
where Position in ('Директор', 'Менеджер')
union 
select EmployeeId from Document
where Compensation > 5000;

-- 5 Получить всех сотрудников, уволенных в 2022 году

select * from Employee
where Id in
(select EmployeeId from Document where Year(DismissalDate) = '2022')

-- 6  Получить всех активных сотрудников - Вычитание

select * from Employee
where not exists (select EmployeeId from Document where EmployeeId = Employee.Id);

-- 7 Получить всех сотрудников и причины их увольнения – Естественное соединение

select e.LastName, e.FirstName, e.MiddleName, da.`Name`, da.Reason from Employee e
join Document d on d.EmployeeId = e.Id
join DismissalArticle da on da.Id = d.DismissalArticleId;

select `Name`, Reason from DismissalArticle
join Document on Document.Compensation = DismissalArticle.DefaultCompensation;

-- Определить подразделения, сотрудники которых были уволены по всем возможным статьям
select * from Employee e
where exists (
	select * from Document.Compo
)
;
