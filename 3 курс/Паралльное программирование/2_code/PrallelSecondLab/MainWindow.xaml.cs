﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PrallelSecondLab
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        //5 Highest — самый высокий
        //4 AboveNormal — выше среднего
        //3 Normal — стандартный
        //2 BelowNormal — ниже среднего
        //1 owest — самый низкий
        /// </summary>

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Data data = new Data(Convert.ToDouble(xVariable.Text));
            
            ThreadPriority firstThreadPriority = (ThreadPriority)firstThreadPrioritize.Value;
            ThreadPriority secondThreadPriority = (ThreadPriority)secondThreadPrioritize.Value;

            Thread firstThread = new Thread(() => RunCalculation(1, data));
            firstThread.Priority = firstThreadPriority;

            Thread secondThread = new Thread(() => RunCalculation(2, data));
            secondThread.Priority = secondThreadPriority;

            firstThread.Start();
            secondThread.Start();
        }

        private readonly Mutex m = new Mutex();

        private void RunCalculation(int numOperation, Data data)
        {
            m.WaitOne();
            
            if (numOperation == 1)
            {
                CalculateSinX(data);
            }
            else
            {
                CalculateMn(data);
            }

            m.ReleaseMutex();
        }

        private void CalculateSinX(Data data)
        {
            Thread.Sleep(1000);

            this.Dispatcher.Invoke(() =>
            {
                firstX.Content = data.X.ToString();
            });

            Stopwatch sw = new Stopwatch();
            sw.Start();
            data.X = Math.Sin(data.X);
            sw.Stop();

            this.Dispatcher.Invoke(() =>
            {
                FirstResult.Content = data.X.ToString();
                TimeFirstFunc.Content = sw.Elapsed;
            });
        }

        private void CalculateMn(Data data)
        {
            Thread.Sleep(1000);
            this.Dispatcher.Invoke(() =>
            {
                secondX.Content = data.X.ToString();
            });

            Stopwatch sw = new Stopwatch();
            sw.Start();
            data.X = Math.Pow(data.X, 3) + 3 * Math.Pow(data.X, 2) + 4 * data.X + 5;
            sw.Stop();

            this.Dispatcher.Invoke(() =>
            {
                SecondResult.Content = data.X.ToString();
                TimeSecondFunc.Content = sw.Elapsed;
            });
        }



        private void firstThreadPrioritize_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }
    }
}
