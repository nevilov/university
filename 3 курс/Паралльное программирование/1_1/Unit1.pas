unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    WordField: TEdit;
    Calculate: TButton;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Result1: TLabel;
    SelectedUserLine: TEdit;
    Label6: TLabel;
    NumWordsInLine: TLabel;
    NumOfWordsVowelsGreater: TLabel;
    procedure CalculateClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.CalculateClick(Sender: TObject);
  const glasn ='aeijouyAEIJOUY';
  var word, tempWord: string;
  var wordsCount, wordsWhereGlasnGreater, counter, currentLine: integer;
  var selectedLine: integer;
  var temp, temp2, line: string;
  var words: string;
  var f : TextFile;
  var i, index, frequencyWord: integer;
begin
  word := WordField.Text;
  selectedLine := StrToInt(SelectedUserLine.Text);
  frequencyWord := 0;
  currentLine := 0;
  wordsCount := 0;
  wordsWhereGlasnGreater := 0;
  counter := 0;
  temp2 := '';
  tempWord := '';


   AssignFile(f, 'C:/temp/input.txt');
   Reset(f);
   while (not EOF(f)) do begin

      Readln(f, line);
      temp := line;
      words := Trim(line);

      //���������� ��������� �����
      while(temp.IndexOf(word) > -1) do begin
        frequencyWord := frequencyWord + 1;

        for i := temp.IndexOf(word)+1+Length(word) to Length(temp) do begin
         temp2:= temp2 + temp[i];
        end;

        temp := temp2;
        temp2:= '';
      end;

      //� ��������� �������
      if currentLine = selectedLine then begin
        while(words.IndexOf(' ') > -1) do begin
          index := words.IndexOf(' ');
          wordsCount := wordsCount + 1;

          //�������� �������
          for i := 1 to index do begin
            if glasn.IndexOf(words[i]) > -1 then begin
              counter:= counter + 1;
              end
            else
              counter := counter - 1;
          end;
          if(counter > 0) then
            wordsWhereGlasnGreater := wordsWhereGlasnGreater + 1;

          counter := 0;
          words := Trim(Copy(words, index+1, Length(words)));
        end;

        for i := 1 to Length(words) do begin
            if glasn.IndexOf(words[i]) > -1 then begin
              counter:= counter + 1;
              end
            else
              counter := counter - 1;
          end;
          if(counter > 0) then
            wordsWhereGlasnGreater := wordsWhereGlasnGreater + 1;

        if (wordsCount > 0) or (Length(line) > 1) then
           wordsCount := wordsCount + 1;
      end;

      currentLine := currentLine + 1;
   end;

   CloseFile(f);

   Result1.Caption := frequencyWord.ToString();
   NumWordsInLine.Caption := wordsCount.ToString();
   NumOfWordsVowelsGreater.Caption := wordsWhereGlasnGreater.ToString();
end;

end.
