object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 300
  ClientWidth = 1050
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 61
    Top = 32
    Width = 106
    Height = 19
    Caption = #1042#1074#1077#1076#1080#1090#1077' '#1089#1083#1086#1074#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 61
    Top = 160
    Width = 73
    Height = 19
    Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 178
    Top = 160
    Width = 320
    Height = 19
    Caption = #1042#1089#1090#1088#1077#1095#1072#1077#1090#1089#1103' '#1079#1072#1076#1072#1085#1085#1086#1077' '#1089#1083#1086#1074#1086' '#1074#1086' '#1074#1089#1077#1084' '#1090#1077#1082#1089#1090#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 178
    Top = 200
    Width = 191
    Height = 19
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1089#1083#1086#1074' '#1074' '#1089#1090#1088#1086#1082#1077
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 176
    Top = 241
    Width = 516
    Height = 19
    Caption = #1057#1082#1086#1083#1100#1082#1086' '#1089#1083#1086#1074' '#1074' '#1089#1090#1088#1086#1082#1077' '#1089#1086#1076#1077#1088#1078#1080#1090' '#1075#1083#1072#1089#1085#1099#1093' '#1073#1091#1082#1074' '#1073#1086#1083#1100#1096#1077' '#1095#1077#1084' '#1089#1086#1075#1083#1072#1089#1085#1099#1093
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Result1: TLabel
    Left = 536
    Top = 165
    Width = 43
    Height = 13
    Caption = 'Result1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 61
    Top = 75
    Width = 164
    Height = 19
    Caption = #1042#1074#1077#1076#1080#1090#1077' '#1085#1086#1084#1077#1088' '#1089#1090#1088#1086#1082#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object NumWordsInLine: TLabel
    Left = 423
    Top = 205
    Width = 43
    Height = 13
    Caption = 'Result1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object NumOfWordsVowelsGreater: TLabel
    Left = 714
    Top = 246
    Width = 43
    Height = 13
    Caption = 'Result1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object WordField: TEdit
    Left = 242
    Top = 34
    Width = 121
    Height = 21
    TabOrder = 0
    Text = #1057#1083#1086#1074#1086
  end
  object Calculate: TButton
    Left = 176
    Top = 112
    Width = 75
    Height = 25
    Caption = #1055#1086#1089#1095#1080#1090#1072#1090#1100
    TabOrder = 1
    OnClick = CalculateClick
  end
  object SelectedUserLine: TEdit
    Left = 242
    Top = 77
    Width = 121
    Height = 21
    TabOrder = 2
    Text = #1053#1086#1084#1077#1088' '#1089#1090#1088#1086#1082#1080
  end
end
