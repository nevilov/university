﻿using System;
using System.Threading;
using System.Windows;


namespace Lab3
{
    public class Params
    {
        public static readonly int ElementsCount = 1024;
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var generatedData = DataGenerator.Generate(Params.ElementsCount);
            var dw = new DataWorker(generatedData);

            Thread threadOne = new(x => dw.ProcessItems(0));
            threadOne.Name = "First";
            Thread threadTwo = new(x => dw.ProcessItems(1));
            threadTwo.Name = "Second";
            threadOne.Start();
            threadTwo.Start();

            Thread.Sleep(5000);
            threadOne.Join();
            threadTwo.Join();
        }
    }

    public static class DataGenerator
    {
        public static int[] Generate(int count)
        {
            int[] result = new int[count];
            Random rnd = new Random();

            for (int i = 0; i < count; i++)
            {
                result[i] = rnd.Next(-10000, 10000);
            }

            return result;
        }
    }

    //bi = ai + an / 2i 
    public class DataWorker
    {
        private readonly int[] _items;
        private readonly int[] _result;
        private Semaphore Sem = new Semaphore(1, 2);
        
        public DataWorker(int[] items)
        {
            _items = items;
            _result = new int[items.Length];
        }

        //0-1 1-0
        public void ProcessItems(int firstFlow)
        {
            if (firstFlow is 0)
            {
                ProcessItems(0, Params.ElementsCount / 2); // ai первые 500
            }
            else
            {
                ProcessItems(Params.ElementsCount / 2, Params.ElementsCount, 1); //an/2i вторые 500
            }
            Thread.Sleep(1000);
            Sem.WaitOne();

            if (firstFlow is 0)
            {
                ProcessItems(Params.ElementsCount / 2, Params.ElementsCount); //an/2i первые 500
            }
            else
            {
                ProcessItems(0, Params.ElementsCount / 2, 1); // ai вторые 500
            }

            Sem.Release();
        }

        private void ProcessItems(int fromIndex, int toIndex, int opNum = default)
        {
            Console.WriteLine($"{Thread.CurrentThread.Name} читает");

            for(int i = fromIndex; i < toIndex; ++i)
            {
                _result[i] += opNum == default ? _items[i] : _items[Params.ElementsCount / (2)];
            }
        }
    }
}