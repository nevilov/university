﻿using System;
using System.Linq;
using System.Threading;
//bj = max(ai, ai+1)
namespace lab4
{
    public class Program
    {
        const int COUNT = 1000;

        static void Main(string[] args)
        {
            var values = GetRandomElements(COUNT);
            Store s = new(values);

            Thread t1 = new Thread(() => { new Worker(s, new ThreadMeta(1)).Run(); });
            Thread t2 = new Thread(() => { new Worker(s, new ThreadMeta(2)).Run(); });
            Thread t3 = new Thread(() => { new Worker(s, new ThreadMeta(3)).Run(); });
            Thread t4 = new Thread(() => { new Worker(s, new ThreadMeta(4)).Run(); });

            t1.Start();
            t2.Start();
            t3.Start();
            t4.Start();

            SemData.s5.WaitOne();
            Console.WriteLine("Результат готов. Выведем первые 10 элементов сгенерированных данных и 10 из результата");
            Console.WriteLine(string.Join(", ", s.GetValues().ToList().Take(10)));
            Console.WriteLine(string.Join(", ", s.GetResult().ToList().Take(10)));
        }

        public static double[] GetRandomElements(int count)
        {
            var rnd = new Random();
            var elements = new double[count];
            for (int i = 0; i < count; i++)
            {
                elements[i] = rnd.Next(-100, 100);
            }

            return elements;
        }
    }

    public class SemData
    {
        public static Semaphore s1 = new Semaphore(1, 1);
        public static Semaphore s2 = new Semaphore(0, 1);
        public static Semaphore s3 = new Semaphore(0, 1);
        public static Semaphore s4 = new Semaphore(0, 1);
        public static Semaphore s5 = new Semaphore(0, 1);
    }

    public class Store
    {
        private double[] _result;
        private double[] _initValues;

        public Store(double[] initialValues)
        {
            _initValues = initialValues;
            _result = new double[initialValues.Length];
        }

        public double[] GetValues()
        {
            return _initValues;
        }

        public double[] GetResult()
        {
            return _result;
        }

        public void SetResult(double[] result)
        {
            _result = result;
        }
    }

    public class ThreadMeta
    {
        public ThreadMeta(int indf)
        {
            Indifier = indf;
        }

        public int Indifier { get; set; }
        public string Description { get; set; }
    }
    
    public class Worker
    {
        private readonly Store _store;
        private readonly ThreadMeta _meta;
        public Worker(Store store, ThreadMeta meta)
        {
            _store = store;
            _meta = meta;
        }

        public void Run()
        {
            Wait();

            Console.WriteLine($" Поток {_meta.Indifier} начал свое выполнение после блокировки");

            var result = _store.GetResult();
            var startIndex = _meta.Indifier is 1 ? 0 : (_meta.Indifier-1) * 250;
            var reachIndex = _meta.Indifier * 250;

            int resultActualIndex = startIndex;

            for (int i = startIndex; i < reachIndex-1; i++)
            {
                result[resultActualIndex] = Math.Max(_store.GetValues()[i], _store.GetValues()[i + 1]);
                resultActualIndex++;
            }
            _store.SetResult(result);

            Release();
        }

        public void Wait()
        {
            switch (_meta.Indifier)
            {
                case 1:
                    SemData.s1.WaitOne();
                    break;
                case 2:
                    SemData.s2.WaitOne();
                    break;
                case 3:
                    SemData.s3.WaitOne();
                    break;
                case 4:
                    SemData.s4.WaitOne();
                    break;

                default:
                    throw new Exception("Невалидный индификатор");
            };
        }

        public void Release()
        {
            switch (_meta.Indifier)
            {
                case 1:
                    SemData.s2.Release();
                    break;     
                case 2:        
                    SemData.s3.Release();
                    break;     
                case 3:        
                    SemData.s4.Release();
                    break;
                case 4:
                    SemData.s5.Release();
                    break;

                default:
                    throw new Exception("Невалидный индификатор");
            };
        }
    }
}
