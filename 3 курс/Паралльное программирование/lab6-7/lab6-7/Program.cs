﻿using System;
using System.Linq;
using System.Threading;

namespace lab6_7
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var store = new Store(5, 5);
            Worker worker = new Worker(store);
            worker.RunGeneration();
        }

    }

    public static class SemData
    {
        public static Semaphore sem = new Semaphore(0, 5);
    }
    public class Store
    {
        private double[][] _initValues;
        public double[] minimums;
        public double[] maximums;
        public Store(int datchCount, int count)
        {
            minimums = new double[datchCount];
            maximums = new double[datchCount];
            _initValues = new double[datchCount][];
            for (int i = 0; i < datchCount; i++)
            {
                _initValues[i] = new double[count];
            }
        }

        public double[][] GetValues()
        {
            return _initValues;
        }

        public void SetValues(double[][] initValues)
        {
            _initValues = initValues;
        }
    }


    public class Worker
    {
        private readonly Store _store;

        public Worker(Store s)
        {
            _store = s;
        }

        public void RunGeneration()
        {
            var rnd = new Random();
            var elements = _store.GetValues();

            for (int i = 0; i < elements.Length; i++)
            {
                for (int j = 0; j < elements[0].Length; j++)
                {
                    elements[i][j] = rnd.Next(-100, 100);
                }
            }

            _store.SetValues(elements);
            Console.WriteLine("Выведем значения");
            Thread[] threads = new Thread[elements.Length];

            for (int f = 0; f < elements.Length; f++)
            {
                Console.WriteLine(string.Join(", ", _store.GetValues()[f].ToList().Take(10)));
                int local = f; // EDJE CASE. zamikalka
                threads[f] = new Thread(() => { FindMin(local, elements[local]); FindMax(local, elements[local]); SemData.sem.Release(); });
            }


            Console.WriteLine("Запускаем потоки...");
            Thread.Sleep(1000);
            foreach(var thread in threads)
            {
                thread.Start();
            }

            Result(elements.Length);
        }

        public void FindMin(int row, double[] buff)
        {
            var min = buff[0];
            for (int i = 0; i < buff.Length; ++i)
            {
                if(buff[i] < min)
                {
                    min = buff[i];
                }
            }

            _store.minimums[row] = min;
        }

        public void FindMax(int row, double[] buff)
        {
            var max = buff[0];
            for (int i = 0; i < buff.Length; ++i)
            {
                if (buff[i] > max)
                {
                    max = buff[i];
                }
            }

            _store.maximums[row] = max;
        }

        public void Result(int row)
        {
            for (int i = 0; i < row; i++)
            {
                SemData.sem.WaitOne();
            }

            double result = 0;
            Console.WriteLine($"Минимумы: {string.Join(", ", _store.minimums)}");
            Console.WriteLine($"Максимумы: {string.Join(", ", _store.maximums)}");

            for (int i = 0; i < row; i++)
            {
                result = _store.minimums[i] + _store.minimums[i];
            }

            Console.WriteLine("Результат равен ", result);
        }
    }
}
