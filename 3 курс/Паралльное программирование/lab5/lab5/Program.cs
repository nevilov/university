﻿using System;
using System.Linq;
using System.Threading;

namespace lab5
{
    internal class Program
    {
        const int COUNT = 10;
        static void Main(string[] args)
        {
            Store s = new Store(COUNT);
            Worker worker = new Worker(s);

            Thread tGeneration = new Thread(() => worker.RunGeneration())
            {
                Name = "1"
            };

            Thread tMin = new Thread(() => worker.FindMinFirst())
            {
                Name = "2"
            };

            Thread tMax = new Thread(() => worker.FindMinSecond())
            {
                Name = "3"
            };

            tMax.Start();
            tMin.Start();
            tGeneration.Start();
        }
    }



    public class Store
    {
        private double[] _initValues;

        public Store(int count)
        {
            _initValues = new double[count];
        }

        public double[] GetValues()
        {
            return _initValues;
        }

        public void SetValues(double[] initValues)
        {
            _initValues = initValues;
        }
    }

    public class Worker
    {
        private readonly Store _store;
        private readonly Semaphore _sem;

        public Worker(Store s)
        {
            _store = s;
            _sem = new Semaphore(0, 1);
        }

        public void RunGeneration()
        {
            var rnd = new Random();
            var elements = _store.GetValues();

            for (int i = 0; i < elements.Length; i++)
            {
                elements[i] = rnd.Next(-100, 100);
            }

            _store.SetValues(elements);
            Console.WriteLine("Выведем значения");
            Console.WriteLine(string.Join(", ", _store.GetValues().ToList().Take(10)));

            Thread.Sleep(1000);
            // Так как два потока должны начать выполнение
            _sem.Release();
            _sem.Release();
        }

        public void FindMinFirst()
        {
            _sem.WaitOne();
            Console.WriteLine($"Поток {Thread.CurrentThread.Name} начал свое выполнение после блокировки find min = min(BUF[2i])");
            var values = _store.GetValues();
            var min = values[1];
            for (int i = 1; i < 5; i++)
            {
                if(values[2*i] < min)
                {
                    min = values[2*i];
                }
            }

            Console.WriteLine($"Минимальное значение найдено = {min}. find min = min(BUF[2i])");
        }
        public void FindMinSecond()
        {
            _sem.WaitOne();
            Console.WriteLine($"Поток {Thread.CurrentThread.Name} начал свое выполнение после блокировки find min = min(BUF[2i+1])");
            var values = _store.GetValues();
            var min = values[0];
            for (int i = 0; i < 4; i++)
            {
                if (values[2 * i + 1] < min)
                {
                    min = values[2 * i + 1];
                }
            }

            Console.WriteLine($"Минимальное значение найдено = {min}. find min = min(BUF[2i+1]");
        }
    }
}
