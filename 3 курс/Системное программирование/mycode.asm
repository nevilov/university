EXAMPLE        SEGMENT
ASSUME  CS:EXAMPLE,DS:EXAMPLE
ORG     100H
X       EQU     5
N       EQU     5

START:
XOR      AX,AX

MOV      CX, N
LEA      SI, B
l:
mov      bx, [SI]
cmp      [SI], X 
je       reverse
         
INC      SI
INC      SI
LOOP     l      

reverse:
        mov ax, B[N*2 - 4]
        mov bx, B[0]
        mov B, ax
        mov B[N*2-4],bx
        

INT      20H
B              DW       5,2,4,3,7


EXAMPLE        ENDS
END      START                 


cmp      bl, c
jge      skip
