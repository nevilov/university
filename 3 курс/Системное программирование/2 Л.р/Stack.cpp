#include <iostream>
#include <string>

using namespace std;

class Node {
public:
    string _element;
    Node* _nextElement;

    Node(string element, Node nextElement) {
        _element = element;
        _nextElement = &nextElement;
    }

    Node(string element) {
        _element = element,
        _nextElement = NULL;
    }

    Node() {
        _element = "",
            _nextElement = NULL;
    }
};

class Stack {
public:
    Node* _element;

    Stack() {
        _element = NULL;
    }

    void push(string data) {

        if (_element == NULL) {
            _element = new Node(data);
            return;
        }

        Node* newElement = new Node(data);
        newElement->_nextElement = _element;
        _element = newElement;
    }

    void print() {
        cout << "\n ����� \n" << endl;
        if (_element == NULL) {
            cout << "���� ������" << endl;
            return;
        }

        Node* current = _element;

        while (current != NULL) {
            cout << "�������: ";
            cout << current->_element << endl;
            current = current->_nextElement;
        }
        cout << "\t -- \t" << endl;
    }

    string pop() {
        Node* firstElement = _element;
        _element = firstElement->_nextElement;

        cout << "�������� ������� �� �����: " << firstElement->_element << endl;
        return firstElement->_element;
    }
};

int main() {
    setlocale(LC_ALL, "ru");

    Stack stack = Stack();
    stack.push("������ ������� �����");
    stack.push("������ ������� �����");
    stack.push("bla-bla");
    stack.print();
    stack.pop();
    stack.pop();
    stack.push("bla-bla 1");
    stack.push("bla-bla 2");
    stack.push("bla-bla 3");
    stack.print();
}