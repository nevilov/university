#include <iostream>
using namespace std;

int main() {
	setlocale(LC_ALL, "ru");

	//Task 1
	cout << "������ ���� int = "  << sizeof(int) << endl;
	cout << "������ ���� short = "  << sizeof(short) << endl;
	cout << "������ ���� long = "  << sizeof(long) << endl;
	cout << "������ ���� float = "  << sizeof(float) << endl;
	cout << "������ ���� double = "  << sizeof(double) << endl;

	//Task 2
	signed char a = '�';
	unsigned char b = '�';
	cout << (int)a << endl;
	cout << (int)b << endl;

	//Task 3
	const int u = 25l;
	unsigned int U = -25;
	cout << U << endl;
	printf("����������������� ������������� ��������� � ������ �������� %d \n", u);
	printf("����������������� ������������� ��������� � ������ �������� %u \n", U);

	printf("����������������� ������������� ��������� � ������ �������� %X \n", u);
	printf("����������������� ������������� ��������� � ������� ������� %X \n", U);
	printf("������������ ������������� ��������� � ������ �������� %o \n", u);
	printf("������������ ������������� ��������� � ������ �������� %o \n", U);


	const double dLo = 215L;
	const double dLt = 241L;

	const string s1 = "LA";
	const string s2 = "LIGA";
	cout << s1 + s2 << endl;

	//Task 4
	char digitZ = '0';
	char digitN = '9';

	for (int i = (int)digitZ; i <= (int)digitN; ++i) {
		cout << (int)i << "   " << (char)i << endl;
	}

	char lettera = 'a';
	char letterz = 'z';

	for (int i = (int)lettera; i <= (int)letterz; ++i) {
		cout << (int)i << "   " << (char)i << endl;
	}

	char letterA = 'A';
	char letterZ = 'Z';

	for (int i = (int)letterA; i <= (int)letterZ; ++i) {
		cout << (int)i << "   " << (char)i << endl;
	}

	cout << (int)' ' << endl;

	//TASK 5
	//const string strA = 5;
	//const int intB;

	//TASK 6

	int sVar;
	//cout << "�� ���������" << sVar << endl;
	//������	C4700	������������ �������������������� ��������� ���������� "sVar"	Project1	C : \Users\SevGU\source\repos\Project1\Project1\main.cpp
	string tVar;
	cout << "�� ���������" << tVar << endl;

	//TASK 7
	//const int constantVariable = 5;
	//const string constantStringVariable = "asdf";
	//constantVariable = 1532;
	//constantStringVariable += "23153";
	
	//TASK 8
	int task8Digit = (-1251) % 8;
	cout << "������� �� ������� = " << task8Digit << endl;

	//TASK 9
	bool result1 = false && true == false;
	bool result2 = true || false != true;
	cout << "result1 " << result1 << endl;
	cout << "result2 " << result2 << endl;

	auto res = sizeof 5 + 15;
	cout << res << endl;
}