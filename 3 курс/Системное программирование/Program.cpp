#include <iostream>
using namespace std;
class Node {
public:
    int _data;
    Node* _left;
    Node* _right;
    Node* _parent;

    Node(int data) {
        _data = data;
    }

    Node() {

    }

    Node(Node* parent, int data) {
        _parent = parent;
        _data = data;
    }
};

class Tree {
public:
    Node* _nodes;
    int _count;

    Tree() {
        _nodes = NULL;
        _count = 0;
    }

    void Delete(int data) {
        cout << "�������� �������� " << data << endl;

        bool isElementFound = false;
        Node* head = _nodes;
        if (head == NULL) {
            cout << "������ ������!" << endl;
            return;
        }

        Node* elementToDelete = new Node();
        while (!isElementFound) {
            if (head->_data == data) {
                cout << "������� " << data << " ������, ������������ ��������." << endl;
                elementToDelete = head;
                isElementFound = true;
            }
            else if (head->_left == NULL && head->_right == NULL) {
                cout << "������� �� ������ " << endl;
                break;
            }

            if (head->_left != NULL && head->_left->_data >= data) {
                head = head->_left;
            }
            else if (head->_right != NULL && head->_right->_data <= data) {
                head = head->_right;
            }
        }

        if (!isElementFound) {
            cout << "������� �� ������ " << endl;
            return;
        }

        Node* leastElement = elementToDelete;
        if (leastElement->_right != NULL) {
            leastElement = leastElement->_right;
            while (true) {
                //����� ���������� � ������� ������
                if (leastElement->_right != NULL && leastElement->_left != NULL) {
                    leastElement = leastElement->_left;
                }
                else if (leastElement->_left != NULL) {
                    leastElement = leastElement->_left;
                }
                // ������ ����, ������ ���
                else if (leastElement->_right != NULL) {
                    leastElement = leastElement->_right;
                }
                else {
                    break;
                }
            }
        }
        else {
            leastElement = elementToDelete->_left;
        }

        //�������� �������� ����� ����������� ��������
        if (leastElement->_parent->_left == leastElement) {
            leastElement->_parent->_left = NULL;
        }
        else {
            leastElement->_parent->_right = NULL;
        }

        leastElement->_parent = elementToDelete->_parent;
        leastElement->_left = elementToDelete->_left;
        leastElement->_right = elementToDelete->_right;

        //������ ������ ��� �������
        if (elementToDelete->_parent->_right == elementToDelete) {
            elementToDelete->_parent->_right = leastElement;
        }
        else {
            elementToDelete->_parent->_left = leastElement;
        }

        //������ �������� �� ������������
        if (elementToDelete->_left != NULL) {
            elementToDelete->_left->_parent = leastElement;
        }
        if (elementToDelete->_right != NULL) {
            elementToDelete->_right ->_parent = leastElement;

        }

    }

    //���������� ���������� ���������, �������� ������� ������ �
    int Multiplicity(int x) {
        cout << "=======\n\n";
        cout << "���������� ����� ������ �������� �������" << endl;
        cout << "���������� ���������� ���������, �������� ������� ������ �" << endl;

        Node* last = _nodes;
        Node* visitedNodes[10];
        int freeIndex = 0;
        int result = 0;

        while (true) {
            if (last->_left == NULL && last->_right == NULL) {
                visitedNodes[freeIndex] = last;
                freeIndex++;
                cout << "��������� ������� " << last->_data <<" �� ��������� " << "x = " << x << endl;
                if (last->_data % x == 0) {
                    ++result;
                }
                last = last->_parent;
            }

            if (last->_left != NULL && !IsVisited(visitedNodes, last->_left)) {
                last = last->_left;
            }
            else if (last->_right != NULL && !IsVisited(visitedNodes, last->_right)) {
                last = last->_right;
            }
            else {
                cout << "��������� ������� " << last->_data << " �� ��������� " << "x = " << x << endl;
                if (last->_data % x == 0) {
                    ++result;
                }
                visitedNodes[freeIndex] = last;
                freeIndex++;
                last = last->_parent;
            }

            if (last == NULL) {
                break;
            }
        }

        cout << "���������� ���������, ������� ������� " << x << " ����� " << result;
        cout << "\n\n=======";
        return result;
    }

    void Push(int data) {
        bool isInserted = false;

        if (_nodes == NULL) {
            _nodes = new Node(data);
            return;
        }

        Node* lastNode = _nodes;
        while (!isInserted) {
            if (data == lastNode->_data) {
                cout << "��������!. " << data << " ������� ���������� ��������" << endl;
            }

            if (data > lastNode->_data) {
                if (lastNode->_right == NULL) {
                    lastNode->_right = new Node(lastNode, data);
                    isInserted = true;
                    _count += 1;
                }
                else {
                    lastNode = lastNode->_right;
                }
            }
            else {
                if (lastNode->_left == NULL) {
                    lastNode->_left = new Node(lastNode, data);
                    isInserted = true;
                    _count += 1;
                }
                else {
                    lastNode = lastNode->_left;
                }
            }
        }
    }

    bool IsVisited(Node* visitedNodes[], Node* element) {
        for (int i = 0; i < _count; ++i) {
            if (visitedNodes[i] == element) {
                return true;
            }
        }

        return false;
    }

    void Display() {
        cout << "����� ��������� \n";
        Node* last = _nodes;
        Node* visitedNodes[10];
        int freeIndex = 0;

        while (true) {
            if (last->_left == NULL && last->_right == NULL) {
                visitedNodes[freeIndex] = last;
                freeIndex++;
                cout << "������ " << last->_data << endl;
                last = last->_parent;
            }

            if (last->_left != NULL && !IsVisited(visitedNodes, last->_left)) {
                last = last->_left;
            }
            else if (last->_right != NULL && !IsVisited(visitedNodes, last->_right)) {
                last = last->_right;
            }
            else {
                cout << "������ " << last->_data << endl;
                visitedNodes[freeIndex] = last;
                freeIndex++;
                last = last->_parent;
            }

            if (last == NULL) {
                return;
            }
        }
    }
};

int main()
{
    setlocale(LC_ALL, "ru");
    Tree* tree = new Tree();
    tree->Push(20);
    tree->Push(30);
    tree->Push(40);
    tree->Push(50);
    tree->Push(10);
    tree->Push(15);
    tree->Push(39);
    tree->Push(21);

    tree->Display();
    tree->Delete(10);
    tree->Display();
    tree->Delete(30);
    tree->Display();

    tree->Multiplicity(5);
    tree->Multiplicity(10);

    return 0;
}


