org 100h

mov si, length
mov di, 0

for1:
    mov al, b.X[si]
    and al, 0Fh
    mov bl, al
    
    dec si
    
    mov al, b.X[si]
    and al, 0Fh
    mov bh, al
    
    dec si
    
    mov al,b.con
    mul bh
    or al, bl
    
    mov res1[di], ax
    inc di
    
    cmp si, -1
    je end1
     
    loop for1 
end1:

mov si, length1
mov di, 0
for2:
    mov al, b.Y[si]
    and al, 0Fh
    mov bl, al
    
    dec si
    
    mov al, b.Y[si]
    and al, 0Fh
    mov bh, al
    
    dec si
    
    mov al,b.con
    mul bh
    or al, bl
    
    mov res2[di], ax
    inc di
    
    cmp si, -1
    je end2
     
    loop for2 
end2:

mov ax, res2
sub res1, ax

X dw "8924"
length dw $-X-1
Y dw "6142"
length1 dw $-Y-1

res1 dw ?
res2 dw ?

con dw 16

ret

