org 100h

mov si, 0
mov bp, 0

for:
    mov dl, data[si]
    
    subFor:
        mov al, table1[bp]
        cmp dl, al
        je found
        inc bp
        loop subfor
               
    found:
        mov al, table2[bp]
        mov codeResult[si], al
        mov bp, 0              
    
    cmp si, dataLength
    je endParse 
    
    inc si
    
    loop for    
endParse:


mov dx, 0
mov si, 1000000000000000b
lea bx, codeResult
forcontrolsum:
    mov al, [bx]
    test al, al
    jp parity
    or dx, si
    
    parity:
        shr si, 1
        inc bx
    loop forcontrolsum
    

table1 db "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
table2 db "3LX6QKBJ8EWCUZ7SF1MGN4DO25AP9HRIVY0T"

data db "AB"
dataLength dw $-data-1

codeResult db ""

result dw ?

con dw 16

ret
