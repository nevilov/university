#include <iostream>
#include <string>
using namespace std;
int** GetTable();

class Leksema {
public:
	int _code;
	string _value;

	Leksema(int code, string value) {
		_code = code;
		_value = value;
	}

	Leksema() {
		_code = 0;
		_value = "";
	}
};

class LeksemaTable {
public:
	string _word;
	Leksema* _leksems;
	int _size;


	LeksemaTable(string word, Leksema* leksems) {
		_word = word;
		_leksems = leksems;
		_size = 0;
	}

	LeksemaTable() {
		_word = "";
		_size = 0;
		_leksems = new Leksema[500];
	}

	void AddLeksema(string value) {
		bool isLexemaExists = false;
		int potentialCode = 0;

		for (int i = 0; i < _size; ++i) {
			if (_leksems[i]._value == value) {
				isLexemaExists = true;
			}

			potentialCode = _leksems[i]._code;
		}

		if (!isLexemaExists) {
			++potentialCode;
			_leksems[_size] = Leksema(potentialCode, value);
			_size++;
		}


		_word += to_string(potentialCode);
		_word += "\n";
	}

	void PrintLexems() {
		cout << "\n===" << endl;
		cout << "����� ������" << endl;
		cout << "Code |\t" << "Value\t" << endl;
		for (int i = 0; i < _size; ++i) {
			cout << _leksems[i]._code << "\t" << _leksems[i]._value << endl;
		}

		cout << "������������������\n" << _word;
	}

};

//������� �������� ����� �� 0 ����� ����� 1 - 101 10001
//������� �������� ����� �� 1 ����� ���� 0 - 010 01110
//��� 1 ������ - 11
void main() {
	setlocale(LC_ALL, "ru");
	string sequence = "1011111001010";

	int** table = GetTable();
	int nextState = 0;
	string leksema = "";
	LeksemaTable leksemaTable = LeksemaTable();

	for (int i = 0; i < sequence.length(); ++i) {
		int value = int(sequence[i]) - 48;

		nextState = table[value][nextState];
		leksema += sequence[i];

		if (nextState == -1) {
			break;
		}

		if (nextState == 0) {
			cout << "������� ����������: " << leksema << endl;
			leksemaTable.AddLeksema(leksema);
			leksema = "";
		}
	}

	if (leksema != "") {
		cout << "������� �� ���������� :" << leksema << endl;
	}

	leksemaTable.PrintLexems();
}

int** GetTable() {
	int** table = new int* [2];
	table[0] = new int[6];
	table[1] = new int[6];


	// S0 ��� F 
	table[0][0] = 3;
	table[1][0] = 1;

	// S1
	table[0][1] = 2;
	table[1][1] = 0;

	// S2
	table[0][2] = 1;
	table[1][2] = 0;

	// S3
	table[0][3] = -1;
	table[1][3] = 4;//

	// S4
	table[0][4] = 0;
	table[1][4] = 3;

	return table;
}