#include <iostream>
#include <stack>

using namespace std;

const int TABLE_LENGTH = 12;

class ManagementTable {
public:
	string _row;
	char* _headers;
	int* _values;

	ManagementTable(string row, char* cols, int values[])
	{
		_row = row;
		_headers = cols;
		_values = values;
	}

	int FindValue(char header) {
		int position = -1;
		for (int i = 0; i < 8; ++i) {
			if (_headers[i] == header) {
				position = i;
			}
		}

		if (position == -1) {
			return 0;
		}

		return _values[position];
	}
};

class Rule {
public:
	string _ruleName;
	string* _values;
	int _valueLength;

	Rule(string ruleName, string* values, int length) {
		_ruleName = ruleName;
		_values = values;
		_valueLength = length;
	}
};

ManagementTable* FindValues(string row, ManagementTable tables[]) {
	for (int i = 0; i < TABLE_LENGTH; ++i) {
		if (tables[i]._row == row) {
			return &tables[i];
		}
	}

	return NULL;
}


void main() {
	setlocale(LC_ALL, "ru");

	string input = "ibtoeof";
	//������� ����� ���o� ������o
	char headers[8] = { 'i', 'b', 't', 'e', '$', ';', 'o', 'f' };
	string firstRule = "<qop>";
	int firstRuleValues[8] ={ 1, 0, 0, 0, 0, 0, 0,0 };

	string secondRule = "<sop>";
	int secondRuleValues[8] = { 2, 0, 0, 0, 3, 3, 2,0 };

	string thirdRule = "<h>";
	int thirdRuleValues[8] = { 0, 0, 0, 0, 4, 5, 0,0 };

	string fourthRule = "<op>";
	int fourthRuleValues[8] = { 7, 0, 0, 0, 3, 3, 6,0 };

	string fifthRule = "i";
	int fifthRuleValues[8] = { -1, 0, 0, 0, 0, 0, 0, 0};

	string sixRule = "b";
	int sixthRuleValues[8] = { 0, -1, 0, 0, 0, 0, 0, 0};

	string sevenRule = "t";
	int sevenRuleValues[8] = { 0, 0, -1, 0, 0, 0, 0, 0};

	string eightRule = "e";
	int eigthRuleValues[8] = { 0, 0, 0, -1, 0, 0, 0,0};

	string nineRule = "$";
	int nineRuleValues[8] = { 0, 0, 0, 0, -1, 0, 0, 0};

	string tenRule = ";";
	int tenRuleValues[8] = { 0, 0, 0, 0, 0, -1, 0,0 };

	string elevenRule = "o";
	int elevenRuleValues[] = { 0, 0, 0, 0, 0, 0, -1, 0};

	string twelveRule = "f";
	int twelveRuleValues[] = { 0, 0, 0, 0, 0, 0, 0, -1 };

	ManagementTable tables[TABLE_LENGTH] = {
		ManagementTable(firstRule, headers, firstRuleValues),
		ManagementTable(secondRule, headers, secondRuleValues),
		ManagementTable(thirdRule, headers, thirdRuleValues),
		ManagementTable(fourthRule, headers, fourthRuleValues),
		ManagementTable(fifthRule, headers, fifthRuleValues),
		ManagementTable(sixRule, headers, sixthRuleValues),
		ManagementTable(sevenRule, headers, sevenRuleValues),
		ManagementTable(eightRule, headers, eigthRuleValues),
		ManagementTable(nineRule, headers, nineRuleValues),
		ManagementTable(tenRule, headers, tenRuleValues),
		ManagementTable(elevenRule, headers, elevenRuleValues),
		ManagementTable(twelveRule, headers, twelveRuleValues),
	};

	for (int i = 0; i < 8; ++i) {
		cout << tables[i]._row << endl;
	}

	Rule initialRules[8] = {
		Rule("<qop>", new string[10] {"i", "b", "t", "<sop>", "e", "<sop>", "f"}, 7),
		Rule("<qop>", new string[10] {"i", "b", "t", "<sop>", "e", "<sop>", "f"}, 7),
		Rule("<sop>", new string[10] { "<op>" }, 1),
		Rule("<sop>", new string[10] { "<h>" }, 1),
		Rule("<h>", new string[10] { "$" }, 2),
		Rule("<h>", new string[10] { ";", "<sop>" }, 2),
		Rule("<op>", new string[10] { "o" }, 1),
		Rule("<op>", new string[10]{ "<qop>"}, 1)
	};
	
	int currentPosition = 0;
	stack<string> stack;
	stack.push("<qop>");

	while (true) {
		if (stack.empty()) {
			cout << "��o���� ���o�� ����������� ����������" << endl;
			return;
		}

		char inputEl = input[currentPosition];

		//��������� o������o� ������o
 		string ruleString = stack.top();
		stack.pop();

		//���o��� �������
		ManagementTable* ruleTable = FindValues(ruleString, tables);
		int ruleNumber = ruleTable->FindValue(inputEl);

		if (ruleNumber == 0) {
			cout << "��o���� ���o�� �� ����������� ����������" << endl;;
			return;
		}
		else if (ruleNumber == -1) {
			cout << "���o������� �����" << endl;
			currentPosition++;
		}
		else {
			//���������� ������� � ����
			Rule rule = initialRules[ruleNumber];
			for (int i = rule._valueLength-1; i >= 0; --i) {
				stack.push(rule._values[i]);
			};
		}
	}
}

