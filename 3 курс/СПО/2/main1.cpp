#include <iostream>
#include <string>
using namespace std;

class Node {
public:
	string _fileName;
	string _type;
	string _size;
	string _createdDate;
	string _lastModifiedDate;
	string _attributes;

	Node* _left;
	Node* _right;

	Node() {

	}

	Node(
		string fileName,
		string type,
		string size,
		string createdDate,
		string lastModifiedDate,
		string attributes,
		Node* left,
		Node* right) {
		_fileName = fileName;
		_type = type;
		_size = size;
		_createdDate = createdDate;
		_lastModifiedDate = lastModifiedDate;
		_attributes = attributes;

		_left = left;
		_right = right;
	}

	Node* GetParent(Node* node) {
		Node* root = this;

		while (true) {
			if (root->_right->_fileName == node->_fileName || root->_left->_fileName == node->_fileName) {
				return root;
			}

			if (root->_fileName <= node->_fileName && root->_right != nullptr) {
				root = root->_right;
			}
			else if (root->_fileName >= node->_fileName && root->_left != NULL) {
				root = root->_left;
			}
		}

		return NULL;
	}

	Node* FindElement(string fileName) {
		Node* root = this;

		while (true) {
			if (root->_fileName == fileName) {
				return root;
			}

			if (root->_fileName <= fileName && root->_right != nullptr) {
				root = root->_right;
			}
			else if (root->_fileName >= fileName && root->_left != NULL) {
				root = root->_left;
			}
		}

		return NULL;
	}

	Node* GetLastElement(string fileName) {
		Node* root = this;
		while (true) {
			if (root->_fileName <= fileName && root->_right != nullptr) {
				root = root->_right;
			}
			else if (root->_fileName >= fileName && root->_left != NULL) {
				root = root->_left;
			}
			else {
				break;
			}
		}

		return root;
	}

	Node* GetLeastElementFromRight() {
		Node* topRoot = this;

		while (topRoot->_right != NULL || topRoot->_left != NULL) {
			if (topRoot->_right != NULL && topRoot->_fileName < topRoot->_right->_fileName) {
				topRoot = topRoot->_right;
			}
			else if (topRoot->_left != NULL) {
				topRoot = topRoot->_left;
			}
		}

		return topRoot;
	}
};

class Catalog {
public:
	Node* _root;

private:
	Node* NodeCreation() {
		string fileName;
		string type;
		string size;
		string createdDate;
		string lastModifiedDate;
		string attributes;
		cout << "������� ��� �����" << endl;
		cin >> fileName;

		cout << "������� ��� �����" << endl;
		//cin >> type;

		cout << "������� ������ �����" << endl;
		//cin >> size;

		cout << "������� ���� ��������" << endl;
		//cin >> createdDate;

		cout << "������� ���� ��������� ����������� �����" << endl;
		//cin >> lastModifiedDate;

		cout << "������� �������� �����" << endl;
		//cin >> attributes;

		Node* file = new Node(fileName, type, size, createdDate, lastModifiedDate, attributes, NULL, NULL);
		return file;
	}

public:
	Catalog() {
		_root = NULL;
	}

	void Display(Node* root, int level) {

		for (int i = 0; i < level; ++i) {
			cout << "\t";
		}

		cout << root->_fileName << endl;
		if (root->_left != NULL) {
			Display(root->_left, level / 2);
		}
		if (root->_right != NULL) {
			Display(root->_right, level / 2);
		}

		cout << endl;
	}

	void Sort() {

	}

	Node* GetList(Node* root, bool isLeftReviewd, bool isRightReview) {
		Node list[1000];
		
		Node* subRoot = root;
		isLeftReviewd = false;
		isRightReview = false;
		Node* subList = new Node();

		if (root->_left != NULL && !isLeftReviewd) {
			isLeftReviewd = true;
			subList = GetList(root->_left, isLeftReviewd, isRightReview);
		}

		int arrSize = sizeof(subList)/sizeof(Node);
		int iterator = 0;

		for(int i=0; i<)

		if (root->_right != NULL && !isRightReview) {
			isRightReview = true;
			subList = GetList(root->_right, isLeftReviewd, isRightReview);
		}

		arrSize = sizeof(subList);
		listSize = sizeof(list);
		for (int i = 0; i < arrSize; ++i) {
			list[listSize + i] = subList[i];
		}

		return list;

	}

	void Display(Node* root, bool isLeftReviewd, bool isRightReview) {
		Node* subRoot = root;
		isLeftReviewd = false;
		isRightReview = false;


		if (root->_left != NULL && !isLeftReviewd) {
			isLeftReviewd = true;
			Display(root->_left, isLeftReviewd, isRightReview);
		}

		if (root->_right != NULL && !isRightReview) {
			isRightReview = true;
			Display(root->_right, isLeftReviewd, isRightReview);
		}

		if ((subRoot->_left == nullptr && subRoot->_right == nullptr) || (isRightReview || isLeftReviewd)) {
			cout << "������� " << subRoot->_fileName << endl;
		}
	}


	int Remove(string fileName) {
		Node* elementToDelete = _root->FindElement(fileName);
		if (elementToDelete->_fileName != fileName) {
			cout << "���� �� ������" << endl;
				return 0;
		}

		Node* parent = _root->GetParent(elementToDelete);

		Node* elementToReplace = elementToDelete->GetLeastElementFromRight();

		if (elementToDelete->_right != NULL && elementToDelete->_left != NULL) {
			Node* parentOfElementToReplace = _root->GetParent(elementToReplace);

			// ������� ������
			if (parentOfElementToReplace->_right->_fileName == elementToDelete->_fileName) {
				parentOfElementToReplace->_right = NULL;
			}
			else {
				parentOfElementToReplace->_left = NULL;
			}

			elementToReplace->_left = elementToDelete->_left;
			elementToReplace->_right = elementToDelete->_right;
		}

		if (parent->_right->_fileName == elementToDelete->_fileName) {
			parent->_right = elementToReplace;
		}
		else {
			parent->_left = elementToReplace;
		}

		return 1;
	}

	int Create() {
		Node* file = NodeCreation();

		if
			(_root == NULL) {
			_root = file;
			return 0;
		}

		Node* lastElement = _root->GetLastElement(file->_fileName);

		if (lastElement->_fileName == file->_fileName) {
			cout << "������� ������� ��������!" << endl;
			return 0;
		}

		if (lastElement->_fileName < file->_fileName) {
			lastElement->_right = file;
		}
		else {
			lastElement->_left = file;
		}

		return 1;
	}
};

int main() {
	setlocale(LC_ALL, "Russian");

	Catalog catalog = Catalog();
	catalog.Create();
	catalog.Create();
	catalog.Create();
	catalog.Create();
	catalog.Create();
	//catalog.Remove("s");

	//catalog.Display(catalog._root, 8);

	catalog.Display(catalog._root, false, false);
	catalog.GetList(catalog._root, false, false);

	cout << "Hello, world!" << endl;
	return 0;
}