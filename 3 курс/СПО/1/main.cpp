#include <iostream>

using namespace std;

class Node {
public:
	int _memoryRef;
	int _allocateAmount;
	Node* _next;
	Node* _prev;

	Node(int memoryRef, int allocateAmount, Node* next, Node* prev) {
		_memoryRef = memoryRef;
		_allocateAmount = allocateAmount;
		_next = next;
		_prev = prev;
	}

	Node(int memoryRef, int allocateAmount) {
		_memoryRef = memoryRef;
		_allocateAmount = allocateAmount;
		_next = NULL;
		_prev = NULL;
	}

	Node(Node* fromCopy) 
		: Node(fromCopy->_memoryRef, fromCopy->_allocateAmount, fromCopy->_next, fromCopy->_prev){
		
	}

	Node* GetLastElement(){
		Node* node = this;
		return GetLastChild(node);
	}

private:
	Node* GetLastChild(Node* node) {
		if (node->_next == NULL) {
			return node;
		}
		
		return GetLastChild(node->_next);
	}
};

class MemoryManager {
	const int LIMIT = 1000;

	Node* freeMemoryList;
	Node* engagedMemoryList;

	void DisplayList(Node* node) {
		Node* topPointer = node;
		while (topPointer != NULL) {
			cout << "[A: "
				<< topPointer->_memoryRef
				<< " | �: " << topPointer->_allocateAmount << "]"
				<< "\t --->\t";
			topPointer = topPointer->_next;
		}
	}

public:
	MemoryManager() {
		freeMemoryList = new Node(0, LIMIT);
		engagedMemoryList = NULL;
	}

	void Display() {
		cout << "<-------------�����------------->" << endl;
		cout << "\n<-------------��������� ������------------->\n" << endl;
		DisplayList(freeMemoryList);
		cout << "\n<-------------������� ������------------->\n" << endl;
		DisplayList(engagedMemoryList);
		cout << "\n-----------------------------------------------\n" << endl;
	}

	void Release(int memoryRef) {
		Node* foundNode = FindNodeByMemoryRef(memoryRef, engagedMemoryList);
		if (foundNode == NULL) {
			cout << "������! ����� ���� �� ������, ���� �� ��������� �� ������ �����" << endl;
			return;
		}

		RemoveNode(foundNode, engagedMemoryList);

		Node* lastFreeElement = freeMemoryList->GetLastElement();
		lastFreeElement->_next = new Node(memoryRef, foundNode->_allocateAmount, NULL, lastFreeElement);
	}

	void Sort() {
		Node* nodes[100];
		Node* topNode = engagedMemoryList;
		int counter = 0;
		while (topNode != NULL) {
			nodes[counter] = topNode;
			topNode = topNode->_next;
			++counter;
		}

		for (int i = 0; i < counter; i++)
		{
			for (int j = 0; j < counter; ++j) {
				if (nodes[i]->_memoryRef < nodes[j]->_memoryRef) {
					Node* temp = nodes[i];
					nodes[i] = nodes[j];
					nodes[j] = temp;
				}
			}
		}
		
		nodes[0]->_prev = NULL;
		nodes[0]->_next = nodes[1];

		nodes[counter - 1]->_next = nodes[counter - 2];
		nodes[counter - 1]->_next = NULL;

		for (int i = 1; i < counter-1; i++)
		{
			nodes[i]->_next = nodes[i+1];
			nodes[i]->_prev = nodes[i-1];
		}

		engagedMemoryList = nodes[0];
	}

	int Allocate(int allocateAmount) {
		Node* topFreeEl = freeMemoryList;
		int position = 0;
		bool isFreePlaceFound = false;

		while (topFreeEl != NULL) {
			if (topFreeEl->_allocateAmount >= allocateAmount) {
				position = topFreeEl->_memoryRef;
				topFreeEl->_memoryRef += allocateAmount;
				topFreeEl->_allocateAmount -= allocateAmount;
				isFreePlaceFound = true;

				//��������� ������ ����� ������
				if (topFreeEl->_allocateAmount == 0) {
					RemoveNode(topFreeEl, freeMemoryList);
				}
				break;
			}

			topFreeEl = topFreeEl->_next;
		}

		if (!isFreePlaceFound) {
			cout << "��������� ����� ��� ��������� ������ ���!" << endl;
			return 0;
		}


		if (engagedMemoryList == NULL)
		{
			engagedMemoryList = new Node(position, allocateAmount, NULL, engagedMemoryList);
			return position;
		}

		Node* lastElement = engagedMemoryList->GetLastElement();
		lastElement->_next = new Node(position, allocateAmount, NULL, lastElement);

		return position;
	}

	Node* FindNodeByMemoryRef(int ref, Node* node) {
		if (node == NULL) {
			return NULL;
		}

		if (node->_memoryRef == ref) {
			return node;
		}

		FindNodeByMemoryRef(ref, node->_next);
	}

	void RemoveNode(Node* node, Node*& list) {
		if (node->_prev != NULL) {
			Node* prevEngagedElement = node->_prev;
			Node* nextEngagedElement = node->_next;
			prevEngagedElement->_next = nextEngagedElement;
		}
		else if (node->_next != NULL) {
			//��� ������ �������
			list = list->_next;
		}
		else {
			//������������ �������
			list = NULL;
		}
	}

};

int main()
{
	setlocale(LC_ALL, "ru");
	MemoryManager memoryManager = MemoryManager();
	memoryManager.Allocate(250);
	memoryManager.Allocate(250);
	memoryManager.Allocate(250);
	memoryManager.Release(250);

	memoryManager.Allocate(50);
	memoryManager.Allocate(220);

	memoryManager.Display();

	memoryManager.Sort();
	memoryManager.Display();

	memoryManager.Allocate(200);
	memoryManager.Display();

	return 0;
}

