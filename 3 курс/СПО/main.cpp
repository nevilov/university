#include <iostream>
using namespace std;

class ManagementTable {
public:
	int length = 8;

	string _row;
	string* _cols;
	int* _values;

	ManagementTable(string row, string* cols, int* values)
	{
		_row = row;
		_cols = cols;
		_values = values;
	}
};

class StackElement {
public:
	string* _heap;
	string _input;
	int _nextRule;

	StackElement(string* heap, string input, int nextRule) {
		_heap = heap;
		_input = input;
		_nextRule = nextRule;
	}
};

ManagementTable* PopulateTable() {
	string headers[] = { "i", "b", "t", "e", "$", ";", "o", "f" };
	string firstRule = "<qop>";
	int firstRuleValues[] = { 1, 0, 0, 0, 0, 0, 0,0 };

	string secondRule = "<cop>";
	int secondRuleValues[] = { 2, 0, 0, 0, 3, 3, 2,0 };

	string thirdRule = "<h>";
	int thirdRuleValues[] = { 0, 0, 0, 0, 4, 5, 0,0 };

	string fourthRule = "<op>";
	int fourthRuleValues[] = { 7, 0, 0, 0, 3, 3, 6,0 };

	ManagementTable tables[] = {
		ManagementTable(firstRule, headers, firstRuleValues),
		ManagementTable(secondRule, headers, secondRuleValues),
		ManagementTable(thirdRule, headers, thirdRuleValues),
		ManagementTable(fourthRule, headers, fourthRuleValues),
	};

	return tables;
}


void main() {
	string input = "ibtoeof";
	ManagementTable* tables = PopulateTable();

	string rules[7][7] = {
		{ "i", "b", "t", "<sop>", "e", "sop", "f"},
		{ "op" },
		{ "<h>" },
		{ "$" },
		{ ";", "<sop>" },
		{ "o" },
		{ "<qop>"}
	}


}

