#include <iostream>
using namespace std;

class Analyzer {
public:
	string sequence = "ibt;oe;of";
	//string sequence = "ibtoeof";
	//string sequence = "ibtoeof";
	int _position = 0;
	bool hasError = false;

	void run() {
		cout << "���������� ������ " << sequence << endl;
		qop();
		if (!hasError) {
			cout << "������� ������ ����������� ����������!" << endl;
		}
	}

	void showError() {
		cout << "===" << endl;
		cout << "������� ������ �� ����������� ����������" << endl;
		cout << "�������� " << sequence[_position] << " ������� " << _position << endl;
		cout << "===" << endl;
		hasError = true;
	}

	void qop() {
		if (sequence[_position] != 'i'
			|| sequence[_position+1] != 'b'
			|| sequence[_position+2] != 't') 
		{
			showError();
			return;
		}
		_position += 3;

		sop();
		if (sequence[_position] != 'e') {
			showError();
			return;
		}
		_position += 1;

		sop();

		if (sequence[_position] != 'f' || _position != sequence.length() - 1) {
			showError();
			return;
		}
	}

	void sop() {
		if (sequence[_position] == '$' || sequence[_position] == ';') {
			sopt();
			return;
		}

		op();
	}

	void sopt() {
		if (sequence[_position] == '$') {
			_position++;
			return;
		}

		if (sequence[_position] == ';') {
			_position++;
			sop();
			return;
		}

		showError();
	}

	void op() {
		if (sequence[_position] == 'o') {
			_position++;
			return;
		}

		qop();
	}
};


void main()
{
	setlocale(LC_ALL, "ru");
	Analyzer a = Analyzer();
	a.run();
}