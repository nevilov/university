drop database taxi;
create database taxi;

use taxi;
create table Clients(
	Id int not null auto_increment,
    `Name` varchar(50) not null,
    Email varchar(50) not null,
    `Password` varchar(100) not null,
	PhoneNumber varchar(50) not null,
    
    primary key(Id)
);

use taxi;
create table Drivers(
	Id int not null auto_increment,
    `Name` varchar(50) not null,
    Email varchar(50) not null,
    `Password` varchar(100) not null,
    PhoneNumber varchar(50) not null,
    
    primary key(Id)
);

use taxi;
create table Cars(
	Id int not null auto_increment,
    `Name` varchar(50) not null,
    `Number` varchar(50) not null,
    `OwnerId` int not null,
    
	primary key(Id),
    constraint fk_cars_driverId foreign key (`OwnerId`) references Drivers(Id)
);

use taxi;
create table Trips(
	Id int not null auto_increment,
    ClientId int not null,
    DriverId int not null,
    Price double not null,
    CreatedDate datetime not null,
    Treshold varchar(100) not null,
    Destination varchar(100) not null,
	`Description` varchar(1000) null,
    
    primary key(Id),
    constraint fk_trip_clientId foreign key(ClientId) references Clients(Id),
    constraint fk_trip_driverId foreign key(ClientId) references Drivers(Id)
);


insert into Clients(`Name`, Email, `Password`, PhoneNumber)
values ('Vasya', 'vasf@mial.ru', '12345678', '+12472356172');

insert into Drivers(`Name`, Email, `Password`, PhoneNumber)
values ('Vasya', 'vasf@mial.ru', '12345678', '+12472356172');

insert into Cars(`Name`, `Number`, OwnerId)
values ('Chevy', '125721-5218', 1);
