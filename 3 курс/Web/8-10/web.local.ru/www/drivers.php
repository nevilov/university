<?php session_start() ?>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="./styles.css" />
        <title>Лучшее такси в Крыму.</title>
    </head>

    <body>
        <div class="header">
            <?php
                require_once "shared/header.php";
            ?>
        </div>

        <div class="content">
        <?php
                require_once "./view/drivers/driversTable.php";
        ?>
        </div>
    </body>
</html>

<style>
.content{
    margin: 24px 0;
}
</style>