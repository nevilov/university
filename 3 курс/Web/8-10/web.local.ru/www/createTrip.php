<?php
session_start();
include_once __DIR__ . ' ./core/services/authService.php';
include_once __DIR__ . ' ./core/services/driverService.php';
include_once __DIR__ . ' ./core/services/tripService.php';

if (!empty($_POST)){
    $service = new TripService();


    echo ($_POST['price']);
    $trip = new Trip(null, $_SESSION['id'], $_POST['driver'], $_POST['price'], '12/12/12', $_POST['treshold'], $_POST['destination'], $_POST['description']);
    $service->CreateTrip($trip);

}

$driverService = new DriverService();
$drivers = $driverService->GetDrivers();
$price = rand(10, 10000);
?>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="./styles.css" />
        <title>Лучшее такси в Крыму.</title>
    </head>

    <body>
    <div class="header">
        <?php 
            require_once "shared/header.php";
        ?>
    </div>

        <div class="content">
            <div class="page-description">
                <h2>Создать заказ</h2>
                <p>Оставьте заказ такси!<p>
            </div>

            <form class="create-trip" method="post" name="trip" action="">

                <div class="class">
                    <select name="classType">
                        <option value="Econom">Эконом</option>
                        <option value="Business">Бизнес</option>
                    </select>
                </div>

                <div class="price">
                    <?echo '<input type="text" id="price" name="price" placeholder="Цена '.$price.'" value="'.$price.'" readonly required/>'?>
                </div>

                <div class="location">
                    <input type="text" placeholder="Место отправления" name="treshold" required>
                    <input type="text" placeholder="Место назначения" name="destination" required>
                </div>

                <div class="date">
                    <input type="date" placeholder="Дата" name="createdDate" required>
                </div>

                <div class="description">
                    <input type="text" placeholder="Дополнительное описание" name="description" required>
                </div>

                <div class="driver">
                    <p>Выберите водителя</p>
                    <select placeholder="Выберите водителя" name="driver" required>
                    <?                        
                        if (is_array($drivers) || is_object($drivers)) {
                            foreach ($drivers as $driver) {
                                echo '<option value="'.$driver->getId().'">'.$driver->getName().'</option>';
                            }
                        }                        
                    ?>
                    </select>
                </div>

                <button type="submit" name="button-login">
                    Создать заявку на трансфер
                </button>
            </form>
        </div> 


    </body>

    <style>
.page-description{
    text-align: center;
    margin: 32px 0;
}

p{
    margin: 12px;
    text-align: center;
}

.link{
    text-align: center;
    font-weight: bold;
}

.create-trip{
    display:flex;
    flex-direction: column;
    width: 100%;
    align-items: center;
    gap: 24px;
}

.create-trip input, .create-trip button, .create-trip select{
    padding: 12px;
    width: 300px;
    border-radius: 8px;
    border: 0.5px solid;
}

    </style>
</html>