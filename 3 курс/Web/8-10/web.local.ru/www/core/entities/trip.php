<?php
class Trip{
    private $id;
    private $client;
    private $driver;
    private $price;
    private $createdDate;
    private $treshold;
    private $destination;
    private $description;

    function getId(){
        return $this->id;
    }

    function getClient(){
        return $this->client;
    }

    function getDriver(){
        return $this->driver;
    }

    function getTreshold(){
        return $this->treshold;
    }

    function getDestination(){
        return $this->destination;
    }

    function getDescription(){
        return $this->description;
    }

    function getPrice(){
        return $this->price;
    }

    function getDate(){
        return $this->createdDate;
    }

    function __construct($id, $client, $driver, $price, $createdDate, $treshold, $destination, $description){
        $this->id = $id;
        $this->client = $client;
        $this->driver = $driver;
        $this->price = $price;
        $this->createdDate = $createdDate;
        $this->treshold = $treshold;
        $this->destination = $destination;
        $this->description = $description;
    }
}
?>