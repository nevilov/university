<?php

    class UserFactory{
        static function CreateUserWithoutId($name, $email, $password, $phoneNumber){
            return new User(null, $name, $email, $password, $phoneNumber);
        }
    }

    class User {
    
        protected $id;
        protected $name;
        protected $email;
        protected $password;
        protected $phoneNumber;

        function __construct($id, $name, $email, $password, $phoneNumber) {
            $this->id = $id;
            $this->name = $name;
            $this->email = $email;
            $this->password = $password;
            $this->phoneNumber = $phoneNumber;
        }

        function getName() {
            return $this->name;
        }

        function getId(){
            return $this->id;
        }

        function getEmail(){
            return $this->email;
        }

        function getPhoneNumber(){
            return $this->phoneNumber;
        }

        function getPassword(){
            return $this->password;
        }
    }
?>