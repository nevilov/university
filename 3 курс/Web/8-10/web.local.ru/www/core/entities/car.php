<?php
include_once './driver.php';

class Car{
    private int $id;
    private string $name;
    private string $number;
    private Driver $owner;
    
    public function __construct(int $id, string $name, string $number, Driver $owner){
        $this->id = $id;
        $this->name = $name;
        $this->number = $number;
        $this->owner = $owner;
    }
}
?>