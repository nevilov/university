<?php
require_once __DIR__ . './../infrastructure/repository.php';
require_once __DIR__ . './../entities/trip.php';
class TripService{
    private $repository;

    public function __construct(){
        $this->repository = new Repository();
    }

    public function GetTrips(){
        $trips = $this->repository->GetTrips();
        return $trips;
    }

    public function CreateTrip($trip){
        $this->repository->CreateTrip($trip);
    }
}
?>