<?php
require_once __DIR__ . './../infrastructure/repository.php';
require_once __DIR__ . './../entities/driver.php';
class DriverService{
    private $repository;

    public function __construct(){
        $this->repository = new Repository();
    }

    public function GetDrivers(){
        $drivers = $this->repository->GetDrivers();
        return $drivers;
    }
}
?>