<?php
require_once __DIR__ . './../infrastructure/repository.php';
require_once __DIR__ . './../entities/user.php';

class AuthService{
    private $repository;

    public function __construct(){
        $this->repository = new Repository();
    }

    function Login($email, $password){
        if(!$this->CheckUserCredentials($email, $password)){
            return;
        }

        $user = $this->repository->GetUserByEmail($email);

        if($user == null){
            return;
        }

        $_SESSION["email"] = $email;
        $_SESSION["pass"] = $password;
        $_SESSION["id"] = $user->getId();
        $_SESSION["IsAuthorized"] = true;
        $_SESSION['Role'] = 'adminsad';

        echo("<script>location.href = '/index.php'</script>");
    }

    function Register(User $user, $role){
        if($role == 'Driver'){
            $this->repository->CreateDriver($user);
            return;
        }

        $this->repository->CreateClient($user);
        echo("<script>location.href = '/login.php';</script>");
    }

    function Logout(){
        $_SESSION["clientId"] = null;
        $_SESSION["email"] = null;
        $_SESSION["pass"] = null;
        $_SESSION["IsAuthorized"] = null;
        $_SESSION['Role'] = null;
    }

    function CheckUserCredentials($email, $password){
        $userQuery = $this->repository->RunQuery("select * from Drivers where email='$email'");
        if($userQuery->num_rows == 0){

            $userQuery = $this->repository->RunQuery("select * from Clients where email='$email'");
            if($userQuery->num_rows==0){
                echo "Пользователь не найден!";
                return false;
            }
        }

        $row = mysqli_fetch_assoc($userQuery);
        if($row["Password"] != $password){
            echo "Неверный логин или пароль!";
            return false;
        }

        echo 'Успешно авторизирован';
        return true;
    }


}


?>