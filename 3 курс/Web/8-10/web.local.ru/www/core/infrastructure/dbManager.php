<?php
class DbManager{
    public $conn;

    function __construct(){
        $this->conn = $this->OpenConnection();
    }

    private function OpenConnection(){
        $dbhost = "localhost";
        $dbuser = "root";
        $dbpass = "12345678";
        $db = "taxi";

        $conn = new mysqli($dbhost, $dbuser, $dbpass, $db) 
        or die("Connect failed: %s\n". $conn -> error);

        return $conn;
    }
    
    function CloseConnection(){
        $this->conn->close();
    }

}

?>