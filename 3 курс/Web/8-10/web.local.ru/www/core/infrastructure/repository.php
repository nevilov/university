<?php
require_once __DIR__ . './dbManager.php';
include_once __DIR__ . './../entities/driver.php';
include_once __DIR__ . './../entities/user.php';
include_once __DIR__ . './../entities/trip.php';

class Repository
{
    public $dbManager;

    function __construct()
    {
        $this->dbManager = new DbManager();
    }

    function GetTrips(){
        $trips = array();
        $tripsQuery = $this->dbManager->conn->query("
        select t.*, ct.`Name` as ClientName, ct.Email as ClientEmail, ct.PhoneNumber as ClientPhone,d.PhoneNumber as DriverPhone, d.`Name` as DriverName, d.Email as DriverEmail from trips t
inner join clients ct on ct.Id = t.ClientId
inner join drivers d on d.Id = t.DriverId;
        ");
        if($tripsQuery->num_rows>0){
            while($row = mysqli_fetch_assoc($tripsQuery)){
                $client = new User(null, $row["ClientName"], $row["ClientEmail"], null, $row["ClientPhone"]);
                $driver = new User(null, $row["DriverName"], $row["DriverEmail"], null, $row["DriverPhone"]);

                $trip = new Trip($row["Id"], $client, $driver, $row["Price"], $row['CreatedDate'], $row['Treshold'], $row['Destination'], $row['Description']);
                array_push($trips, $trip);
            }
        }

        return $trips;
    }

    function GetUserByEmail($email)
    {
        $req = "select * from Clients where Email like '%$email%'";
        $userQuery = $this->dbManager->conn->query($req);
        if ($userQuery->num_rows > 0) {
            while ($row = mysqli_fetch_assoc($userQuery)) {
                $user = new User($row["Id"], $row["Name"], $row["Email"], $row["Password"], $row["PhoneNumber"]);
                return $user;
            }
        }

        return null;
    }

    function GetDrivers()
    {
        $drivers = array();
        $driversQuery = $this->dbManager->conn->query("select * from Drivers");
        if($driversQuery->num_rows>0){
            while($row =  mysqli_fetch_assoc($driversQuery)) {
                $driver = new Driver($row["Id"], $row["Name"], $row["Email"], $row["Password"], $row["PhoneNumber"]);
                array_push($drivers, $driver);
              }
        }

        return $drivers;
    }

    function RunQuery($query){
        return $this->dbManager->conn->query($query);
    }

    function CreateDriver(User $user){
        $name = $user->getName();
        $phone = $user->getPhoneNumber();
        $password = $user->getPassword();
        $email = $user->getEmail();

        $sqlRequest =
            "insert into Drivers(name, email, PhoneNumber, Password) values
            ('$name', '$email', '$phone', '$password')
            ";

        $this->dbManager->conn->query($sqlRequest);
    }

    function CreateClient(User $user){
        $name = $user->getName();
        $phone = $user->getPhoneNumber();
        $password = $user->getPassword();
        $email = $user->getEmail();

        $sqlRequest =
            "insert into Clients(name, email, PhoneNumber, Password) values
            ('$name', '$email', '$phone', '$password')
            ";

        $this->dbManager->conn->query($sqlRequest);
    }

    function CreateTrip(Trip $trip){
        $clientId = $trip->getClient();
        $driverId = $trip->getDriver();
        $destination = $trip->getDestination();
        $treshold = $trip->getTreshold();
        $description = $trip->getDescription();
        $price = $trip->getPrice();
        $date = $trip->getDate();
        echo ('sa');
        echo ($price);
        $sqlRequest =
        "insert into Trips(clientId, driverId, destination, treshold, description, price, createdDate) values
        ('$clientId', '$driverId', '$destination', '$treshold', '$description', '$price', '$date')
        ";
        echo ($sqlRequest);

        $result = $this->dbManager->conn->query($sqlRequest);
        echo $result;
    }
}

/*
        $sqlRequest =
            "insert into Drivers(name, email, PhoneNumber, Password) values
            (".$name.", ".$email.", ".$phone.", ".$password.")
            ";

        var_dump($sqlRequest);

        $name = $user->getName();
        $phone = $user->getPhoneNumber();
        $password = $user->getPassword();
        $email = $user->getEmail();

        $sqlRequest->bind_param('s', $name);
        $sqlRequest->bind_param('s', $email);
        $sqlRequest->bind_param('s', $phone);
        $sqlRequest->bind_param('s', $password);

        $sqlRequest->execute();*/
?>


