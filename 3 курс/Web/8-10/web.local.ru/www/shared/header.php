<?php
session_start();
include_once __DIR__ . ' ./../core/services/authService.php';

if (isset($_POST['logout'])){
    $service = new AuthService();
    $service->Logout();
}

?>

<header>
    <div class="banner">
        <img src="./assets/taxi.png" class="logo" />
    </div>

    <menu>
        <div class="items">
            <a href="./">Главная</a>
            <a href="./trips.php">Заказы</a>
            <a href="./car-types.php">Виды транспорта</a>
            <a href="./drivers.php">Водители</a>
            <a href="#">Контакты</a>
        </div>

        <div class="login">
            <?php
            $userName = $_SESSION['email'];
            if (empty($userName)) {
                echo '<a href="./login.php">Войти</a>';
            } else {
                echo "Добро пожаловать '$userName'";
            }
            ?>

            <?
                if($_SESSION['email'] != null){
                    echo '<form method="post">';
                    echo '<button name="logout" type="submit" class="logout">Выйти</button>';
                    echo '</form>';
                }
            ?>
        </div>
        
    </menu>
</header>