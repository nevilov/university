<?php
session_start();
if (isset($_POST['createOrderButton'])) {
    "
    ";
}

?>

<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="./styles.css" />
    <title>Лучшее такси в Крыму.</title>
</head>

<body>
    <div class="header">
        <?php
        require_once "shared/header.php";
        ?>
    </div>

    <div class="content">
        <div class="create-trip-button">
            <a href="./createTrip.php" class="basic-button">Создать заказ</a>
        </div>
        <?php
        require_once "./view/tripsTable.php";
        ?>
    </div>
</body>

</html>

<style>
    .content {
        margin: 24px 0;
    }
</style>