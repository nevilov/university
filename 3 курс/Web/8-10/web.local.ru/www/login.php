<?php
session_start();
include_once __DIR__ . ' ./core/services/authService.php';

if (!empty($_POST)){
    $service = new AuthService();
    $service->Login($_POST['email'], $_POST['pass']);
}

?>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="./styles.css" />
        <title>Лучшее такси в Крыму.</title>
    </head>

    <body>
    <div class="header">
        <?php 
            require_once "shared/header.php";
        ?>
    </div>

        <div class="content">
            <div class="page-description">
                <h2>Авторизация</h2>
                <p>Войдите, чтобы пользоваться сервисом!<p>
            </div>

            <form class="login-form" method="post" name="login" action="">
                <div class="email">
                    <input type="text" id="email" name="email" placeholder="Введите email"/>
                </div>

                <div class="password">
                    <input type="password" id="pass" name="pass" placeholder="Введите пароль"/>
                </div>

                <button type="submit" name="button-login">
                    Войти
                </button>
            </form>
            <p>Нет аккаунта? <a href="./register.php" class="link">Зарегистрироваться</a></p>
        </div> 


    </body>

    <style>
.page-description{
    text-align: center;
    margin: 32px 0;
}

p{
    margin: 12px;
    text-align: center;
}

.link{
    text-align: center;
    font-weight: bold;
}

.login-form{
    display:flex;
    flex-direction: column;
    width: 100%;
    align-items: center;
    gap: 24px;
}

.login-form input, .login-form button{
    padding: 12px;
    width: 300px;
    border-radius: 8px;
    border: 0.5px solid;
}

    </style>
</html>