<?php session_start(); ?>
<!DOCTYPE html>
<html>
    
    <head>
        <link rel="stylesheet" href="./styles.css" />
        <title>Лучшее такси в Крыму.</title>
    </head>
    <body>
    
    <div class="header">
        <?php
            require_once "shared/header.php";
        ?>
    </div>

        <div class="content-wrapper">
            
            <div class="content">
                <span class="title">Лучшее такси в Крыму!</span>
                <div class="main-content">
                    <img class="taxi-image" src="./assets/taxi_image.png">

                    <div class="pricelist">
                        <div class="option">
                            <span class="option-title">Оплачиваемый пробег</span>
                            
                            <div class="option-content">
                                <span>Стоимость 1км по городу</span>
                                <span>23 руб</span>
                            </div>
                            <div class="option-content">
                                <span>Стоимость 1км по городу</span>
                                <span>23 руб</span>
                            </div>
                        </div>

                        <div class="option">
                            <span class="option-title">Оплачиваемый пробег</span>
                            
                            <div class="option-content">
                                <span>Стоимость 1км по городу</span>
                                <span>23 руб</span>
                            </div>
                            <div class="option-content">
                                <span>Стоимость 1км по городу</span>
                                <span>23 руб</span>
                            </div>
                        </div>

                        <div class="option">
                            <span class="option-title">Оплачиваемый пробег</span>
                            
                            <div class="option-content">
                                <span>Стоимость 1км по городу</span>
                                <span>23 руб</span>
                            </div>
                            <div class="option-content">
                                <span>Стоимость 1км по городу</span>
                                <span>23 руб</span>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <?php 
                require_once "shared/sidebar.php"
            ?>
        </div>

    </body>
</html>