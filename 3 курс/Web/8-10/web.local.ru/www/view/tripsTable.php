<?php
include_once __DIR__ . ' ./../core/services/tripService.php';

?>

<div class="drivers-table">
    <div class="item-container">
        <div class="item">Идентификатор зазка</div>
        <div class="item">Имя клиента</div>
        <div class="item">Имя водителя</div>
        <div class="item">Номер телефона заказчика</div>
        <div class="item">Место отправления/назначения</div>

    </div>

    <?php
            $tripService = new TripService();
            $trips = $tripService->GetTrips();

            if (is_array($trips) || is_object($trips)) {
                foreach ($trips as $trip) {

                    echo '<div class="item-container">';

                    echo '<div class="item">' . $trip->getId() . '</div>';
                    echo '<div class="item">' . $trip->getClient()->getName() . '</div>';
                    echo '<div class="item">' . $trip->getDriver()->getName() . '</div>';
                    echo '<div class="item">' . $trip->getDriver()->getPhoneNumber() . '</div>';
                    echo '<div class="item">' . $trip->getTreshold() . '/ ' .$trip->getDestination() .'</div>'; 
                    echo '</div>';
                }
            }
    ?>


    <style>
        .drivers-table {
            margin: 14px;
            border: 1px solid;
            border-radius: 8px;
            padding: 12px;
        }

        .item-container {
            display: flex;
            border-left: solid 1px #0000;
            justify-content: space-around;

        }

        .item-container:nth-child(1) {
            border-bottom: 0.5px solid #000;
            margin: 12px 0;
            font-weight: bold;
        }

        .item {
            width: calc(100% / 4);
            text-align: center;
            padding: 0.5em 0.5em;
            border-right: solid 1px #0000;
            border-bottom: solid 1px #0000;
        }
    </style>

    </html>