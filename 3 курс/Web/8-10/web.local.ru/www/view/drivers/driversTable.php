<?php
include_once __DIR__ . ' ./../../core/services/driverService.php';

?>

<div class="drivers-table">
    <div class="item-container">
        <div class="item">Идентификатор</div>
        <div class="item">Имя</div>
        <div class="item">Эмаил</div>
        <div class="item">Номер телефона</div>
    </div>

    <?php
            $driverService = new DriverService();
            $drivers = $driverService->GetDrivers();

            if (is_array($drivers) || is_object($drivers)) {
                foreach ($drivers as $driver) {

                    echo '<div class="item-container">';

                    echo '<div class="item">' . $driver->getId() . '</div>';
                    echo '<div class="item">' . $driver->getName() . '</div>';
                    echo '<div class="item">' . $driver->getEmail() . '</div>';
                    echo '<div class="item">' . $driver->getPhoneNumber() . '</div>';

                    echo '</div>';
                }
            }
    ?>


    <style>
        .drivers-table {
            margin: 14px;
            border: 1px solid;
            border-radius: 8px;
            padding: 12px;
        }

        .item-container {
            display: flex;
            border-left: solid 1px #0000;
            justify-content: space-around;

        }

        .item-container:nth-child(1) {
            border-bottom: 0.5px solid #000;
            margin: 12px 0;
            font-weight: bold;
        }

        .item {
            width: calc(100% / 4);
            text-align: center;
            padding: 0.5em 0.5em;
            border-right: solid 1px #0000;
            border-bottom: solid 1px #0000;
        }
    </style>

    </html>