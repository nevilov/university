<?php session_start() ?>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="./styles.css" />
        <title>Виды автомобилей!</title>
    </head>

    <style>
        .taxi-class-type{
            display: flex;
            gap: 12px;
            align-items: center;
            margin: 24px 0;
        }
        .car-image img{
            width: 300px;
        }        
    </style>
    <body>
        <div class="header">
            <?php
                require_once "shared/header.php";
            ?>
        </div>
        <div class="content-wrapper">
            <div class="content">
                <div class="title">
                    <h2>Виды автомобилей</h2>
                </div>

                <div class="main-content">
                    <div class="data">
                        <div class="taxi-class-type">
                            <div class="car-image">
                                <img src="./assets/taxi_image.png"/>
                            </div>
                            <div class="type-description">
                                <h3>Бюджет - Schoda Octavia 2015</h3>
                                <p>Удобное и качественное авто. Проверенная годами категория!</p>
                            </div>
                        </div>

                        <div class="taxi-class-type">
                            <div class="car-image">
                                <img src="./assets/cars/middle.png"/>
                            </div>
                            <div class="type-description">
                                <h3>Средний класс - Mercedes Benz</h3>
                                <p>Более удобный класс. Качетсвтенное авто.</p>
                            </div>
                        </div>

                        <div class="taxi-class-type">
                            <div class="car-image">
                                <img src="./assets/cars/business.png"/>
                            </div>
                            <div class="type-description">
                                <h3>Бизнес - Audi A4</h3>
                                <p>Раскошное и качественное авто, максимум удобств. Доступны дополнительные услуги!</p>
                            </div>
                        </div>
                    </div>           
                </div>
            </div>
            <?php 
                require_once "shared/sidebar.php"
            ?>
        </div>
    </body>
</html>