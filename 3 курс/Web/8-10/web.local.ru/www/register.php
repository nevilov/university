<?php
session_start();

include_once __DIR__ . ' ./core/services/authService.php';
require_once __DIR__ . './core/entities/user.php';
echo $_POST['role'];
if (!empty($_POST)) {

    $service = new AuthService();
    $user = UserFactory::CreateUserWithoutId(
        $_POST['name'],
        $_POST['email'],
        $_POST['pass'],
        $_POST['phone']
    );
    $service->Register($user, $_POST['role']);
}

?>

<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="./styles.css" />
    <title>Лучшее такси в Крыму.</title>
</head>

<body>
    <div class="header">
        <?php
        require_once "shared/header.php";
        ?>
    </div>

    <div class="content">
        <div class="page-description">
            <h2>Регистрация</h2>
            <p>Зарегистрируйтесь, чтобы пользоваться сервисом!
            <p>
        </div>

        <form class="login-form" method="post" name="login" action="">
            <div class="name">
                <input type="text" id="name" name="name" placeholder="Введите Имя" />
            </div>

            <div class="phone">
                <input type="text" id="phone" name="phone" placeholder="Введите номер телефона" />
            </div>

            <div class="email">
                <input type="email" id="email" name="email" placeholder="Введите email" />
            </div>

            <div class="password">
                <input type="password" id="pass" name="pass" placeholder="Введите пароль" />
            </div>

            <div class="role">
                <div>Роль</div>
                <div class="role1">

                    <input type="radio" name="role" value="Driver">
                    <label for="role">Водитель</label>
                </div>

                <div class="role2">

                    <input type="radio" name="role" value="Client">
                    <label for="role">Клиент</label>
                </div>
            </div>

            <button type="submit" name="button-login">
                Войти
            </button>
        </form>
    </div>


</body>

<style>
    .page-description {
        text-align: center;
        margin: 32px 0;
    }

    .login-form {
        display: flex;
        flex-direction: column;
        width: 100%;
        align-items: center;
        gap: 24px;
    }

    .login-form input,
    .login-form button {
        padding: 12px;
        width: 300px;
        border-radius: 8px;
        border: 0.5px solid;
    }
</style>

</html>