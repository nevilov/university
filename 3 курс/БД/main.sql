create database deansoffice;
use deansoffice;
create table Faculty(
 Id int not null auto_increment,
 `Name` nvarchar(50) not null,
 primary key(Id)
);
create table Speciality(
 Id int not null auto_increment,
 `Name` nvarchar(50) not null,
 FacultyId int,
 primary key(Id),
 foreign key(FacultyId) references Faculty(Id)
);

create table Students(
 RecordBook int primary key auto_increment,
 FirstName nvarchar(50) not null,
 SecondName nvarchar(50) not null,
 MiddleName nvarchar(50) not null,
 `Grant` int not null default 0,
 SpecialityId int not null,
 GroupNum int not null,
 foreign key(SpecialityId) references Speciality(Id)
);
create table Teacher(
 Id int not null auto_increment,
 `Name` nvarchar(50) not null,
 `Rank` nvarchar(50),
 AcademicDegree bit not null default 0,
 Position nvarchar(50) not null,
 Salary double not null,
 SpecialityId int not null,
 primary key(Id),
 constraint foreign key(SpecialityId) references Speciality(Id)
);

create table Course(
 `Name` nvarchar(50),
 Hourse double not null,
 TabNumber int not null,
 `Description` nvarchar(500),
 primary key(`Name`),
 foreign key(TabNumber) references Teacher(Id)
);

create table StudentCourse(
 RecordBook int not null auto_increment,
 CourseName nvarchar(50),
 primary key(RecordBook, CourseName),
 foreign key(RecordBook) references Students(RecordBook),
 foreign key(CourseName) references Course(`Name`)
);

