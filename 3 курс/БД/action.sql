use deansoffice;

select * from faculty;
select * from speciality;

insert into faculty(Id, Name) values
(1, 'ИТИКС'), 
(2, 'Гуманитарии'),
(3, 'ИС');

insert into speciality(Id, `Name`, FacultyId) values 
(1, 'Программная инженерия', 1),
(2, 'Информационные технологии и компьютерные системы', 1),
(3, 'Педагоги', 2),
(4, 'Информационные системы', 3);

insert into Students(RecordBook, FirstName, SecondName, MiddleName, `Grant`,
SpecialityId, GroupNum)
values
(1, 'Григорий', 'Григорьев', 'Григорьевич', 3500, 2, 1),
(2, 'Михаил', 'Гиря', 'Леонтьевич', 3500, 2, 1),
(3, 'Александр', 'Сухарев', 'Григорьевич', 3500, 1, 1),
(4, 'Ашраф', 'Хакимьевич', 'Высоцкий', 3500, 1, 1),
(5, 'Дарья', 'Андреевна', 'Чудская', 3500, 3, 1),
(6, 'Андрей', 'Ильич', 'Чукотский', 3500, 4, 1);

insert into teacher(Id, Name, `Rank`, AcademicDegree, Position, Salary, SpecialityId) values
(1, 'Савельева Оксана Андреевна', 'Профессор', 1, '', 100000, 3),
(2, 'Ершов Лев Иванович', '2 степень', 0, 'Доцент', 90000, 1),
(3, 'Смирнова Ева Егоровна', '3 степень', 0, 'Доцент', 90000, 1),
(4, 'Головина Валерия Ярославовна', '2 степень', 1, 'Доцент', 90000, 2),
(5, 'Полякова Анастасия Павловна', '3 степень', 0, 'Доцент', 90000, 2);

select * from course;
insert into course(Name, Hourse, TabNumber, Description) values
('Русский язык и экономика', 180, 1, ''),
('Программирование', 180, 2, ''),
('Базы данных', 180, 3, ''),
('Моделирование', 180, 4, ''),
('Дизайн', 180, 5, '');

select * from students;
insert into studentcourse(RecordBook, CourseName) values
(1, 'Дизайн'),
(2, 'Дизайн'),
(3, 'Программирование'),
(4, 'Программирование'),
(3, 'Дизайн'),
(4, 'Дизайн'),
(5, 'Русский язык и экономика'),
(6, 'Дизайн'),
(6, 'Программирование');


select s.RecordBook, s.FirstName, s.SecondName, sp.Name from students as s
join speciality sp on sp.Id = s.SpecialityId;

select s.RecordBook, s.FirstName, s.SecondName, c.Name as CourseName, tc.Name as Teacher
from students as s
join studentcourse stco on stco.RecordBook = s.RecordBook
join course c on stco.CourseName = c.Name
join teacher tc on tc.Id = c.TabNumber;

UPDATE `deansoffice`.`students` SET `Grant` = '2500' WHERE (`RecordBook` = '4');
UPDATE `deansoffice`.`students` SET `Grant` = '1500' WHERE (`RecordBook` = '3');
UPDATE `deansoffice`.`students` SET `Grant` = '3300' WHERE (`RecordBook` = '2');
UPDATE `deansoffice`.`students` SET `Grant` = '1500' WHERE (`RecordBook` = '6');


select s.RecordBook, s.FirstName, s.SecondName, s.Grant
from students as s
where s.Grant > (select avg(st.Grant) from Students as st);

select sp.Name, avg(`Grant`) as avgStip from Students as s
join speciality sp on sp.Id = s.SpecialityId
Group by sp.Name
having avgStip > 2000
order by avgStip;

select t1.Name, t1.Salary from teacher as t1
where t1.Salary < (select Max(Salary) from teacher);

-- Препод без курсов
insert into teacher(Id, Name, `Rank`, AcademicDegree, Position, Salary, SpecialityId) values
(100, 'Осипов Илья Маркович', 'Профессор', 1, '', 100000, 3);
select * from teacher
where Name = all (select c.Name from course as c where c.Id = teacher.Id);



-- saf

create view StudentsView as
select FirstName, SecondName, MiddleName, `Grant`, SpecialityId, GroupNum
from students;

create view SpecialityView as
select `Name`, FacultyId from speciality;

select * from SpecialityView;
select * from StudentsView;
use deansoffice;
insert into StudentsView values
('Василий','Васильевич','Нижнегорский', 3500, 1, 1)

insert into StudentsView values
('Василий','Васильевич','Нижнегорский', 3500, 1, 1,1);

drop view StudentsView;
drop view SpecialityView;

create view StudentsFullView as
select s.RecordBook, s.FirstName, s.SecondName, c.Name as CourseName, tc.Name as Teacher
from students as s
join studentcourse stco on stco.RecordBook = s.RecordBook
join course c on stco.CourseName = c.Name
join teacher tc on tc.Id = c.TabNumber;

select * from StudentsFullView;

CREATE INDEX FIO ON Students(FirstName, SecondName, MiddleName);

select sp.Name, avg(`Grant`) as avgStip from Students as s
join speciality sp on sp.Id = s.SpecialityId
Group by sp.Name
having avgStip > 2000
order by avgStip;