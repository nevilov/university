﻿using System.Text;
using Lab1;

Console.InputEncoding = Encoding.Unicode;
Console.OutputEncoding = Encoding.Unicode;

var firstAlphabet = Alphabet.FirstWildcardAlphabet();
var secondAlphabet = Alphabet.SecondWildcardAlphabet();
var defaultAlphabet = Alphabet.DefaultAlphabet();

if (firstAlphabet is null || secondAlphabet is null || defaultAlphabet is null)
{
    throw new Exception("Алфавиты непроинициализированы");
}

while (true)
{
    Console.WriteLine("Введите последовательность, которую необходимо зашифровать");
    var word = Console.ReadLine()!;
    
    var converted = Crypt(word);

    Console.WriteLine($"Шифруем {word} = {converted}");
    
    Console.WriteLine("Расшифровка");

    var decrypted = Decrypt(converted);
    Console.WriteLine($"Расшифровано = {decrypted}");
}


string Crypt(string word)
{
    var result = new StringBuilder();
    
    for (var i = 0; i < word.Length; ++i)
    {
        var symbolIndex = defaultAlphabet.IndexOf(word[i].ToString());
        result.Append(i % 2 == 0 ? firstAlphabet[symbolIndex] : secondAlphabet[symbolIndex]);
    }

    return result.ToString();
}

string Decrypt(string crypt)
{
    var result = new StringBuilder();
    
    for (var i = 0; i < crypt.Length; ++i)
    {
        var symbolIndex = i % 2 == 0 
            ? firstAlphabet.IndexOf(crypt[i].ToString())
            : secondAlphabet.IndexOf(crypt[i].ToString());
        
        result.Append(defaultAlphabet[symbolIndex]);
    }

    return result.ToString();
}
