﻿using System.Text;

class Program
{
    public static void Main()
    {
        Console.InputEncoding = Encoding.Unicode;
        Console.OutputEncoding = Encoding.Unicode;
        
        var encrypt = new Encrypt();
        Console.WriteLine("Введите текст для шифрования");
        var word = Console.ReadLine()!;
        var encryptionResult = encrypt.Process(word, true);
        Console.WriteLine($"Зашифрованая последовательность = {encryptionResult}");
        var decryptedResult = encrypt.Process(encryptionResult, false);
        Console.WriteLine($"Расшифрованная последовательность = {decryptedResult}");
    }
}

class Encrypt
{
    private string _key = "sjdhsbdhjfuhgdhfbcxjhgsf";
    private int _roundAmount = 24;
    private int _blockLength = 2;
    
    int ObrFunction(int word, int round)
    {
        var keyChar = _key[(round % _key.Length)];
        return word ^ keyChar;
    }

    public string Process(string word, bool encrypt)
    {
        var nearWordLength = word.Length + word.Length % _blockLength;
        word = word.PadRight(nearWordLength);

        var encryptionResult = string.Empty;
        for (var i = 0; i < word.Length; i+= _blockLength)
        {
            var block = string.Join("",word
                .Skip(i)
                .Take(_blockLength));
            encryptionResult += encrypt ? EncryptBlock(block) : DecryptBlock(block);
        }

        return encryptionResult;
    }

    string EncryptBlock(string block)
    {
        for (int round = 0; round < _roundAmount; ++round)
        {
            var leftPart = block.Substring(0, block.Length / 2);
            var rightPart = block.Substring(block.Length / 2);
            var foundValue = (char)(rightPart[0] ^ ObrFunction(leftPart[0], round));
            block = rightPart + foundValue + leftPart.Substring(1);
        }

        return block;
    }
    
    string DecryptBlock(string block)
    {
        for (int round = _roundAmount - 1; round >= 0; --round)
        {
            var leftPart = block.Substring(0, block.Length / 2);
            var rightPart = block.Substring(block.Length / 2);
            var foundValue = (char)(leftPart[0] ^ ObrFunction(rightPart[0], round));
            block = foundValue + rightPart.Substring(1) + leftPart;
        }

        return block;
    }
}