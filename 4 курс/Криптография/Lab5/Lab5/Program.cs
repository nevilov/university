﻿var p = 353;
var q = 11; // Простой делитель числа 352
var a = GenerateA();
var x = 5;
var y = IntPow(a, x, p);

Console.WriteLine("Введите последовательность");
var seq = Console.ReadLine()!;
var hash = GetHash(seq);
hash = hash % q is 0 ? 1 : hash;
var (s, r1) = GetSubscriptionHash();

Console.WriteLine($"Полученная контрольная сумма = {hash}");
Console.WriteLine($"Полученная ЭЦП = {s}, {r1}");
var checkResult = CheckSubscription(seq);
Console.WriteLine($"Проверка ЭЦП. Результат = {checkResult}");
Console.WriteLine($"Добавление к контенту цифру 1 в конец и проверка ЭЦП = {CheckSubscription(seq+1)}");

bool CheckSubscription(string word)
{
    if (r1 < 0 || r1 > q || s < 0 || s > q)
    {
        return false;
    }

    var hashCheck = GetHash(word);
    hashCheck = hashCheck % q is 0 ? 1 : hash;

    var v = IntPow(hashCheck, q - 2, q);
    var z1 = (int)(s * v) % q;
    var z2 = (int)((q - r1) * v) % q;
    var u = (IntPow(a, z1, p) * IntPow(y, z2, p)) % p;
    u %= q;

    return r1 == u;
}

(long, long) GetSubscriptionHash()
{
    while (true)
    {
        var rnd = new Random();
        var k = rnd.Next(0, q);
        var r = IntPow(a, k, p);
        var r1 = r % q;

        if (r1 == 0)
        {
            continue;
        }

        var s = (x * r1 + k * hash) % q;
        if (s != 0)
        {
            return (s, r1);
        }
    }
}
    
//Количество 1 в битовом представлении символов исходного текста
int GetHash(string word)
{
    var count = 0;
    foreach (var letter in word)
    {
        var value = (int)letter;
        while (value>1)
        {
            if (value % 2 == 1)
            {
                count += 1;
            }

            value /= 2;
        }
        count += 1;
    }

    return count;
}

long GenerateA()
{
    for (int i = 2; i < p; ++i)
    {
        var result = IntPow(i, q, p);
        if (result == 1)
        {
            return i;
        }
    }

    return 0;
}

long IntPow(long x, int power, int mod)
{
    if (power == 0)
        return 1;
    if (power % 2 == 0)
    {
        long halfPower = IntPow(x, power / 2, mod);
        return (halfPower * halfPower) % mod;
    }
    else
    {
        return (x * IntPow(x, power - 1, mod)) % mod;
    }
}