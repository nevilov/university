﻿#pragma warning disable CA1416
using System.Drawing;
using System.Drawing.Imaging;
using Lab6;

var initialFilePath = "C:\\Users\\nevil\\Desktop\\University\\4 курс\\Криптография\\Lab6\\Lab6\\image.bmp";
using var bitMapImg = new Bitmap(initialFilePath);

var processedImage = Steganography.Process("DUE SOme  TEXT", bitMapImg);
processedImage.Save("processed_image.bmp", ImageFormat.Bmp);

var extractedText = Steganography.GetText(processedImage);
Console.WriteLine($"Полученная последовательность из изображения {extractedText}");
