﻿#pragma warning disable CA1416
using System.Drawing;

namespace Lab6;

public static class Steganography
{
    public static string GetText(Bitmap img)
    {
        var textResult = string.Empty;
        var pixelIndex = 0;
        var charValue = 0;

        for (var i = 0; i < img.Height; i++)
        {
            for (var j = 0; j < img.Width; j++)
            {
                var pixel = img.GetPixel(j, i);
                
                for (var n = 0; n < 3; ++n)
                {
                    if (pixelIndex % 8 is 0 && pixelIndex is not 0)
                    {
                        var reversed = ReverseBits(charValue);
                        if (charValue is 0)
                        {
                            return textResult;
                        }

                        charValue = 0;
                        textResult += (char)reversed;
                    }

                    charValue = (pixelIndex % 3) switch
                    {
                        0 => charValue * 2 + pixel.R % 2,
                        1 => charValue * 2 + pixel.G % 2,
                        2 => charValue * 2 + pixel.B % 2,
                        _ => charValue
                    };

                    pixelIndex++;
                }
            }
        }

        return textResult;
    }
    
    public static int ReverseBits(int n)
    {
        int result = 0;

        for (int i = 0; i < 8; i++)
        {
            result = result * 2 + n % 2;

            n /= 2;
        }

        return result;
    }

    public static Bitmap Process(string text, Bitmap bmp)
    {
        var textIndex = 0;
        var pixelIndex = 0;
        var charValue = 0;
        
        for (var i = 0; i < bmp.Height; i++)
        {
            for (var j = 0; j < bmp.Width; j++)
            {
                var pixel = bmp.GetPixel(j, i);
                
                // now, clear the least significant bit (LSB) from each pixel element
                var R = pixel.R - pixel.R % 2;
                var G = pixel.G - pixel.G % 2;
                var B = pixel.B - pixel.B % 2;

                if (text.Length < textIndex)
                {
                    //Заполнить нулями
                    bmp.SetPixel(j, i, Color.FromArgb(R, G, B));
                    continue;
                }

                for (var n = 0; n < 3; ++n)
                {
                    if (pixelIndex % 8 == 0)
                    {
                        charValue = textIndex >= text.Length ? 0 : (int)text[textIndex];
                        textIndex++;
                    }

                    switch (pixelIndex % 3)
                    {
                        case 0:
                            R += charValue % 2;
                            break;
                        case 1:
                            G += charValue % 2;
                            break;
                        case 2:
                            B += charValue % 2;
                            break;
                    }
                    
                    charValue /= 2;
                
                    pixelIndex++;
                }
                
                bmp.SetPixel(j, i, Color.FromArgb(R, G, B));
            }
        }

        return bmp;
    }
}