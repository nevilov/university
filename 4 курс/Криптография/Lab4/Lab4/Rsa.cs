﻿namespace Lab4;

public class Rsa
{
    public static string Encrypt(string word, int e, int n)
    {
        var encryptedResult = new List<long>();
        foreach (var block in word)
        {
            var encryptedBlock = IntPow(block, e, n);

            encryptedResult.Add(encryptedBlock);
        }

        return string.Join("", encryptedResult.Select(x => (char)x));
    }

    public static string Decrypt(string word, int d, int n)
    {
        var encryptedResult = new List<long>();
        foreach (var block in word)
        {
            var decryptedBlock = IntPow(block, d, n);
            encryptedResult.Add(decryptedBlock); 
        }

        return string.Join("", encryptedResult.Select(x => (char)x));
    }
    
    public static long IntPow(long x, int power, int mod)
    {
        if (power == 0)
            return 1;
        if (power % 2 == 0)
        {
            long halfPower = IntPow(x, power / 2, mod);
            return (halfPower * halfPower) % mod;
        }
        else
        {
            return (x * IntPow(x, power - 1, mod)) % mod;
        }
    }
}