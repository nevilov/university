﻿using System.Text;
using Lab4;
Console.InputEncoding = Encoding.Unicode;
Console.OutputEncoding = Encoding.Unicode;

var p = 193;
var q = 353;

var n = p * q;
var res = (p - 1) * (q - 1);
var e = 5;

if (FindNok(res, e) != 1)
{
    throw new Exception($"Число {e} не является взаимно простым");
}

var d = FindD();

Console.WriteLine("Введите последовательность для шифрования");
var word = Console.ReadLine()!;

var encryptedResult = Rsa.Encrypt(word, e, n);
Console.WriteLine($"Зашифрованая последовательность = {encryptedResult}");

var decrypted = Rsa.Decrypt(encryptedResult, d, n);
Console.WriteLine($"Расшифрованая последовательность = {decrypted}");

int FindNok(int a, int b)
{
    while (a != b)
    {
        if (a < 0 || b < 0)
        {
            return 0;
        }
        
        if (a > b)
            a -= b;
        else
            b -= a;
    }

    return a;
}


int FindD()
{
    var temp = 1;
    var iteration = 1;
    while (temp % e != 0)
    {
        temp = (res * iteration) + 1;
        
        if (iteration == 10)
        {
            throw new Exception("Невозомжно подобрать число d");
        }
        iteration++;
    }

    return temp / e;
}

