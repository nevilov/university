﻿using System.Text;

class Crypto
{
    private readonly double _a;
    private readonly double _c; 
    private readonly double _m;
    private readonly double _tFirst;

    private double tState = 0;

    public Crypto(double a, double c, double t, double b)
    {
        _a = a;
        _c = c;
        _m = Math.Pow(2, b);
        _tFirst = t;
        tState = t;
    }

    private double Next()
    {
        tState = (_a * tState + _c) % _m;
        return tState;
    }

    public string Process(string text)
    {
        tState = _tFirst;
        var encrypted = string.Empty;
        foreach (var letter in text)
        {
            encrypted += (char)((char)Next() ^ letter);
        }

        return encrypted;
    }
}

class Program
{
    static void Main()
    {
        Console.InputEncoding = Encoding.Unicode;
        Console.OutputEncoding = Encoding.Unicode;
        
        var lcg = new Crypto(5, 3, 700, 8);
        Console.WriteLine("Введите текст для шифрования");
        var text = Console.ReadLine()!;
        var encryptedText = lcg.Process(text);

        Console.WriteLine($"Зашифрованный текст: {encryptedText}");

        var decrypted = lcg.Process(encryptedText);
        Console.WriteLine($"Расшифрованный текст: {decrypted}");
    }
}