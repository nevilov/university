terraform {
  required_providers {
    twc = {
      source = "tf.timeweb.cloud/timeweb-cloud/timeweb-cloud"
    }
  }
  required_version = ">= 1.4.4"
}
    
provider "twc" {
  token = "eyJhbGciOiJSUzUxMiIsInR5cCI6IkpXVCIsImtpZCI6IjFrYnhacFJNQGJSI0tSbE1xS1lqIn0.eyJ1c2VyIjoiY2g5ODQxOCIsInR5cGUiOiJhcGlfa2V5IiwicG9ydGFsX3Rva2VuIjoiNjM5MjU0YzYtOWU2OC00ZDkwLTlkMjItMmIwMjQzZDk1NDMxIiwiYXBpX2tleV9pZCI6IjAyMjM0M2NhLWYwOGItNDMyMC1hZjA0LTg1N2NjYTlhYzg1OCIsImlhdCI6MTcwODk0NjgwNX0.VBQdd02T5NuaAa1AjA9ucT7yIBWVERiodr0zKv-F5NTPlXgYfy0RHlzqCTpFpG8qZkBvOe1vHdk_xBxY4mG4YUlBlp9rLd2XwLYm7Gw8ZRJzbhcR_SW1LZXjY5iLmUFCbXL-eobXL8fj0ix6ItwAC8nXDjpTEFDCp_JksrbIwb236s-67M7ovCgz-uLq3VP6MIffxIfQepUBTKeBn5CxQ6PsL-FgFH9uqWm5JpWjDaJIZCqeGOrtzK5uiX2a4vgSL9c_4BlxF0CFyKtV2C7HADKWSuHFZ9uOLhrJjsiWEQbKsvHi2t0HOEAlugzFJEkSBddwEdkEs7qWBFrRj-6hlUsswryIxnpt7YSPl4ETFSZIF9eXpNwjKU4iWgJXI1k3EGpJDty3OtsJTBSSNsiaJEhRJlTnRJkGtxNtXVUrXBSJmQ8Tt2sK3b2OGHYvgnAk3ySYzMmGGmw2EfffUEnEc15k0EM9RH9UpixTCMvGo-hHe3d5XzqXmCZgb3GyE4zJ"
}

data "twc_configurator" "configurator" {
  location = "ru-1"
  disk_type = "nvme"
}

data "twc_os" "os" {
  name = "ubuntu"
  version = "20.04"
}

resource "twc_server" "my-timeweb-server" {
  name = "Server created by terraform"
  os_id = data.twc_os.os.id

  configuration {
    configurator_id = data.twc_configurator.configurator.id
    disk = 15360
    cpu = 1
    ram = 1024
  }
}