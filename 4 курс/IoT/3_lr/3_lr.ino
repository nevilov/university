#define MEASURE_PERIOD 500 // время периода измерения
#define R2 0.2 // сопротивление резистора R1
#define R1 1. // сопротивление резистора R2э

#define R3 1 // сопротивление резистора R1
#define R4 1. // сопротивление резистора R2

uint32_t timer = 0;
float u1;
float u2;


void setup() {
 Serial.begin(9600); // инициализируем порт, скорость 9600
}
void loop() {
 // период 500 мс
 if (millis() - timer >= MEASURE_PERIOD) { // таймер на millis()
 timer = millis(); // сброс
 u1= ((float)analogRead(A0)) * 5. / 1024. /R2 * (R1 + R2);
 u2 = ((float)analogRead(A7)) * 5. / 1024. /R3 * (R3 + R4);


  // передача данных через последовательный порт
  Serial.print("U1 = "); Serial.println(u1, 2);
  Serial.print("U2 = "); Serial.println(u2, 2);
 }
}