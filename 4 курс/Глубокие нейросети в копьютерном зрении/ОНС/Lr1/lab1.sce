//1 Задание

function y=f1(x)
    y=sqrt((3*x+1)^3)/x+2 + (sin((2*x )))^2
endfunction

xgrid;
xtitle('Реализованная функция по варианту', 'x', 'y')
x=linspace(0,1,15);
plot(x,f1,"r") // График функции
legend('f(x)',4,%t) //Label, 4


//2 Задание

A = [
    4, 2, 1;
    3, -2, 0;
    0, -1, 2;
    ];

B = [
    2, 0, 1;
    5, -7, -2;
    1, 0, -1;
    ];

D = 3*(A^2 + B^2) - 2*A*B
disp(D)

//3 Задание

function z = f2(x, y)
    z = 4*(cos((x+y+%pi/3)/(x*y+2)))^2
endfunction

x2 = linspace(0,1,3);
y2 = linspace(0,1,3);
z2 = feval(x2, y2, f2);

figure(1)
subplot(1,3,1)
surf(x2,y2,z2)

subplot(1,3,2)
plot3d(x2,y2,z2)

subplot(1,3,3)
plot3d1(x2,y2,z2)

//4 задание
//Матрица 22x1200
figure(2)
xtitle('Матрица 22x1200, расп Пуассона с мат ожиданием 10', 'x', 'y')
M=grand(22,1200,"poi",10); 
histplot(12,M);
disp(M);
middleByCol = mean(M) //- среднее
disp('Среднее по столбцам', middleByCol)

stOtk = stdev(M); //- cтандартное отклонение
disp("Стандартное отклонение", stOtk)
           
dispers = variance(M); //- дисперсия
disp('Дисперсия', dispers);

med = median(M); //- медиана
disp('Медиана', med)
