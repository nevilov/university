F = 0.04; //минимальная частота входного сигнала
R = 50; //число входов слоя
S1 = 32; // число нейронов слоя 1
S2 = 1; // число нейронов слоя 2

//формирование матрицы p входных данных, размером R x length(t)
t=0:0.1:2/F; // формируем массив отсчетов времени от 0 до 2/F c шагом 0.1
fi=(2*%pi*F*12)*t;

p=[sin(fi)]; // формируем массив отсчетов функции sin для 1-го входа

for i=2:R // формируем м    ассивы отсчетов функции sin для i-го входа, где i=1..R
 p=[p; sin(fi*i)]; // и добавляем эти массисы в виде очередной строки в матрицу p
end

//выполняем инициализацию весов и смещений
scale=1/sqrt(R);
w=grand(S1,R,'unf',-scale,scale)
b=zeros(S1,1);

//выполняем моделирование слоя c polsin нелинейностью
n=w*p+repmat(b,1,size(p,2)) // repmat копирует вектор b в виде столбцов
// и строит из него матрицу размера size(w*p)= (S x length(t))
a=ann_relu_activ(n);

//вычисление производных polsin нелинейности
d_a=ann_d_relu_activ(a);

disp('Слой 1 - polsin');

mean_a=mean(a)
stdev_a=stdev(a)
mean_d_a=mean(d_a)
stdev_d_a=stdev(d_a)

disp('Среднее', mean_a);
disp('Дисперсия', stdev_a);
disp('Среднее производной', mean_a);
disp('Дисперсия производной', stdev_d_a);

//построение графиков активностей, производных и гистограмм
figure(1);
subplot(2,2,1)
plot(t,a(1,:),t,a(2,:), t,a(3,:));
title('Активность нейронов слоя')
subplot(2,2,2)
plot(t,d_a(1,:),t,d_a(2,:), t,d_a(3,:));
title('Производные функции активации слоя')
subplot(2,2,3)
histplot(20,a);
title('Гистограмма активности нейронов слоя')
subplot(2,2,4)
histplot(20,d_a);
title('Гистограмма производных функции активации')

scale=1/sqrt(S1); // Относительно числа входнов
w2=grand(S2,S1,'unf',-scale,scale)
b2=zeros(S2,1);

n2=w2*a+repmat(b2,1,size(a,2))
a2=ann_relu_activ(n2);

//вычисление производных polsin нелинейности
d_a2=ann_d_relu_activ(a2);


disp('Слой 2 - polsin');

mean_a=mean(a2)
stdev_a=stdev(a2)
mean_d_a=mean(d_a2)
stdev_d_a=stdev(d_a2)

disp('Среднее', mean_a);
disp('Дисперсия', stdev_a);
disp('Среднее производной', mean_a);
disp('Дисперсия производной', stdev_d_a);

figure(2);
subplot(2,2,1)
plot(t,a2(1,:));
title('Активность нейрона внешнего слоя')

subplot(2,2,2)
plot(t,d_a2(1,:));
title('Производные функции активации слоя')

subplot(2,2,3)
histplot(20,a2);
title('Гистограмма активности нейронов слоя')

subplot(2,2,4)
histplot(20,d_a2);
title('Гистограмма производных функции активации')



