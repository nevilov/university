-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "11/30/2023 19:34:08"

-- 
-- Device: Altera EP4CE22F17C6 Package FBGA256
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	leds_and_buttons IS
    PORT (
	osc : IN std_logic;
	keys : IN std_logic_vector(0 TO 1);
	leds : BUFFER std_logic_vector(3 DOWNTO 0)
	);
END leds_and_buttons;

-- Design Ports Information
-- leds[0]	=>  Location: PIN_A12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- leds[1]	=>  Location: PIN_C11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- leds[2]	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- leds[3]	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- keys[0]	=>  Location: PIN_E10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- keys[1]	=>  Location: PIN_B12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- osc	=>  Location: PIN_E1,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF leds_and_buttons IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_osc : std_logic;
SIGNAL ww_keys : std_logic_vector(0 TO 1);
SIGNAL ww_leds : std_logic_vector(3 DOWNTO 0);
SIGNAL \fd|s~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \osc~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \leds[0]~output_o\ : std_logic;
SIGNAL \leds[1]~output_o\ : std_logic;
SIGNAL \leds[2]~output_o\ : std_logic;
SIGNAL \leds[3]~output_o\ : std_logic;
SIGNAL \keys[1]~input_o\ : std_logic;
SIGNAL \osc~input_o\ : std_logic;
SIGNAL \osc~inputclkctrl_outclk\ : std_logic;
SIGNAL \fd|Add0~0_combout\ : std_logic;
SIGNAL \fd|rg~1_combout\ : std_logic;
SIGNAL \fd|Add0~1\ : std_logic;
SIGNAL \fd|Add0~2_combout\ : std_logic;
SIGNAL \fd|Add0~3\ : std_logic;
SIGNAL \fd|Add0~4_combout\ : std_logic;
SIGNAL \fd|Add0~5\ : std_logic;
SIGNAL \fd|Add0~6_combout\ : std_logic;
SIGNAL \fd|Add0~7\ : std_logic;
SIGNAL \fd|Add0~8_combout\ : std_logic;
SIGNAL \fd|Add0~9\ : std_logic;
SIGNAL \fd|Add0~10_combout\ : std_logic;
SIGNAL \fd|Add0~11\ : std_logic;
SIGNAL \fd|Add0~12_combout\ : std_logic;
SIGNAL \fd|Add0~13\ : std_logic;
SIGNAL \fd|Add0~14_combout\ : std_logic;
SIGNAL \fd|rg~0_combout\ : std_logic;
SIGNAL \fd|Add0~15\ : std_logic;
SIGNAL \fd|Add0~16_combout\ : std_logic;
SIGNAL \fd|Add0~17\ : std_logic;
SIGNAL \fd|Add0~18_combout\ : std_logic;
SIGNAL \fd|Add0~19\ : std_logic;
SIGNAL \fd|Add0~20_combout\ : std_logic;
SIGNAL \fd|Add0~21\ : std_logic;
SIGNAL \fd|Add0~22_combout\ : std_logic;
SIGNAL \fd|Add0~23\ : std_logic;
SIGNAL \fd|Add0~24_combout\ : std_logic;
SIGNAL \fd|rg~2_combout\ : std_logic;
SIGNAL \fd|Add0~25\ : std_logic;
SIGNAL \fd|Add0~26_combout\ : std_logic;
SIGNAL \fd|rg~3_combout\ : std_logic;
SIGNAL \fd|Add0~27\ : std_logic;
SIGNAL \fd|Add0~28_combout\ : std_logic;
SIGNAL \fd|rg~4_combout\ : std_logic;
SIGNAL \fd|Add0~29\ : std_logic;
SIGNAL \fd|Add0~30_combout\ : std_logic;
SIGNAL \fd|rg~5_combout\ : std_logic;
SIGNAL \fd|Add0~31\ : std_logic;
SIGNAL \fd|Add0~32_combout\ : std_logic;
SIGNAL \fd|Add0~33\ : std_logic;
SIGNAL \fd|Add0~34_combout\ : std_logic;
SIGNAL \fd|rg~6_combout\ : std_logic;
SIGNAL \fd|Add0~35\ : std_logic;
SIGNAL \fd|Add0~36_combout\ : std_logic;
SIGNAL \fd|Add0~37\ : std_logic;
SIGNAL \fd|Add0~38_combout\ : std_logic;
SIGNAL \fd|rg~7_combout\ : std_logic;
SIGNAL \fd|Add0~39\ : std_logic;
SIGNAL \fd|Add0~40_combout\ : std_logic;
SIGNAL \fd|rg~8_combout\ : std_logic;
SIGNAL \fd|Add0~41\ : std_logic;
SIGNAL \fd|Add0~42_combout\ : std_logic;
SIGNAL \fd|rg~9_combout\ : std_logic;
SIGNAL \fd|Add0~43\ : std_logic;
SIGNAL \fd|Add0~44_combout\ : std_logic;
SIGNAL \fd|rg~10_combout\ : std_logic;
SIGNAL \fd|Add0~45\ : std_logic;
SIGNAL \fd|Add0~46_combout\ : std_logic;
SIGNAL \fd|rg~11_combout\ : std_logic;
SIGNAL \fd|Add0~47\ : std_logic;
SIGNAL \fd|Add0~48_combout\ : std_logic;
SIGNAL \fd|Add0~49\ : std_logic;
SIGNAL \fd|Add0~50_combout\ : std_logic;
SIGNAL \fd|rg~12_combout\ : std_logic;
SIGNAL \fd|Equal0~7_combout\ : std_logic;
SIGNAL \fd|Equal0~5_combout\ : std_logic;
SIGNAL \fd|Equal0~1_combout\ : std_logic;
SIGNAL \fd|Equal0~3_combout\ : std_logic;
SIGNAL \fd|Equal0~2_combout\ : std_logic;
SIGNAL \fd|Equal0~0_combout\ : std_logic;
SIGNAL \fd|Equal0~4_combout\ : std_logic;
SIGNAL \fd|Equal0~6_combout\ : std_logic;
SIGNAL \fd|Equal0~8_combout\ : std_logic;
SIGNAL \fd|s~feeder_combout\ : std_logic;
SIGNAL \fd|s~q\ : std_logic;
SIGNAL \fd|s~clkctrl_outclk\ : std_logic;
SIGNAL \keys[0]~input_o\ : std_logic;
SIGNAL \r0|i~1_combout\ : std_logic;
SIGNAL \r0|i~0_combout\ : std_logic;
SIGNAL \r0|l~3_combout\ : std_logic;
SIGNAL \r0|l~0_combout\ : std_logic;
SIGNAL \r1|i~0_combout\ : std_logic;
SIGNAL \r1|i~1_combout\ : std_logic;
SIGNAL \r1|l~0_combout\ : std_logic;
SIGNAL \mul|ld[0]~0_combout\ : std_logic;
SIGNAL \r1|l~1_combout\ : std_logic;
SIGNAL \r0|l~1_combout\ : std_logic;
SIGNAL \mul|ld[1]~1_combout\ : std_logic;
SIGNAL \r0|l~2_combout\ : std_logic;
SIGNAL \r1|l~2_combout\ : std_logic;
SIGNAL \mul|ld[2]~2_combout\ : std_logic;
SIGNAL \r1|l~3_combout\ : std_logic;
SIGNAL \mul|ld[3]~3_combout\ : std_logic;
SIGNAL \fd|rg\ : std_logic_vector(0 TO 25);
SIGNAL \r0|l\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \r0|i\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \r1|l\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \r1|i\ : std_logic_vector(31 DOWNTO 0);

BEGIN

ww_osc <= osc;
ww_keys <= keys;
leds <= ww_leds;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\fd|s~clkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \fd|s~q\);

\osc~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \osc~input_o\);

-- Location: IOOBUF_X43_Y34_N16
\leds[0]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mul|ld[0]~0_combout\,
	devoe => ww_devoe,
	o => \leds[0]~output_o\);

-- Location: IOOBUF_X38_Y34_N2
\leds[1]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mul|ld[1]~1_combout\,
	devoe => ww_devoe,
	o => \leds[1]~output_o\);

-- Location: IOOBUF_X40_Y34_N2
\leds[2]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mul|ld[2]~2_combout\,
	devoe => ww_devoe,
	o => \leds[2]~output_o\);

-- Location: IOOBUF_X40_Y34_N9
\leds[3]~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \mul|ld[3]~3_combout\,
	devoe => ww_devoe,
	o => \leds[3]~output_o\);

-- Location: IOIBUF_X43_Y34_N22
\keys[1]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_keys(1),
	o => \keys[1]~input_o\);

-- Location: IOIBUF_X0_Y16_N8
\osc~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_osc,
	o => \osc~input_o\);

-- Location: CLKCTRL_G2
\osc~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \osc~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \osc~inputclkctrl_outclk\);

-- Location: LCCOMB_X29_Y10_N6
\fd|Add0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~0_combout\ = \fd|rg\(25) $ (VCC)
-- \fd|Add0~1\ = CARRY(\fd|rg\(25))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \fd|rg\(25),
	datad => VCC,
	combout => \fd|Add0~0_combout\,
	cout => \fd|Add0~1\);

-- Location: LCCOMB_X29_Y10_N2
\fd|rg~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|rg~1_combout\ = (!\fd|Equal0~8_combout\ & \fd|Add0~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \fd|Equal0~8_combout\,
	datad => \fd|Add0~0_combout\,
	combout => \fd|rg~1_combout\);

-- Location: FF_X29_Y10_N3
\fd|rg[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|rg~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(25));

-- Location: LCCOMB_X29_Y10_N8
\fd|Add0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~2_combout\ = (\fd|rg\(24) & (!\fd|Add0~1\)) # (!\fd|rg\(24) & ((\fd|Add0~1\) # (GND)))
-- \fd|Add0~3\ = CARRY((!\fd|Add0~1\) # (!\fd|rg\(24)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fd|rg\(24),
	datad => VCC,
	cin => \fd|Add0~1\,
	combout => \fd|Add0~2_combout\,
	cout => \fd|Add0~3\);

-- Location: FF_X29_Y10_N9
\fd|rg[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|Add0~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(24));

-- Location: LCCOMB_X29_Y10_N10
\fd|Add0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~4_combout\ = (\fd|rg\(23) & (\fd|Add0~3\ $ (GND))) # (!\fd|rg\(23) & (!\fd|Add0~3\ & VCC))
-- \fd|Add0~5\ = CARRY((\fd|rg\(23) & !\fd|Add0~3\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(23),
	datad => VCC,
	cin => \fd|Add0~3\,
	combout => \fd|Add0~4_combout\,
	cout => \fd|Add0~5\);

-- Location: FF_X29_Y10_N11
\fd|rg[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|Add0~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(23));

-- Location: LCCOMB_X29_Y10_N12
\fd|Add0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~6_combout\ = (\fd|rg\(22) & (!\fd|Add0~5\)) # (!\fd|rg\(22) & ((\fd|Add0~5\) # (GND)))
-- \fd|Add0~7\ = CARRY((!\fd|Add0~5\) # (!\fd|rg\(22)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(22),
	datad => VCC,
	cin => \fd|Add0~5\,
	combout => \fd|Add0~6_combout\,
	cout => \fd|Add0~7\);

-- Location: FF_X29_Y10_N13
\fd|rg[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|Add0~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(22));

-- Location: LCCOMB_X29_Y10_N14
\fd|Add0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~8_combout\ = (\fd|rg\(21) & (\fd|Add0~7\ $ (GND))) # (!\fd|rg\(21) & (!\fd|Add0~7\ & VCC))
-- \fd|Add0~9\ = CARRY((\fd|rg\(21) & !\fd|Add0~7\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fd|rg\(21),
	datad => VCC,
	cin => \fd|Add0~7\,
	combout => \fd|Add0~8_combout\,
	cout => \fd|Add0~9\);

-- Location: FF_X29_Y10_N15
\fd|rg[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|Add0~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(21));

-- Location: LCCOMB_X29_Y10_N16
\fd|Add0~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~10_combout\ = (\fd|rg\(20) & (!\fd|Add0~9\)) # (!\fd|rg\(20) & ((\fd|Add0~9\) # (GND)))
-- \fd|Add0~11\ = CARRY((!\fd|Add0~9\) # (!\fd|rg\(20)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fd|rg\(20),
	datad => VCC,
	cin => \fd|Add0~9\,
	combout => \fd|Add0~10_combout\,
	cout => \fd|Add0~11\);

-- Location: FF_X29_Y10_N17
\fd|rg[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|Add0~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(20));

-- Location: LCCOMB_X29_Y10_N18
\fd|Add0~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~12_combout\ = (\fd|rg\(19) & (\fd|Add0~11\ $ (GND))) # (!\fd|rg\(19) & (!\fd|Add0~11\ & VCC))
-- \fd|Add0~13\ = CARRY((\fd|rg\(19) & !\fd|Add0~11\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fd|rg\(19),
	datad => VCC,
	cin => \fd|Add0~11\,
	combout => \fd|Add0~12_combout\,
	cout => \fd|Add0~13\);

-- Location: FF_X29_Y10_N19
\fd|rg[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|Add0~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(19));

-- Location: LCCOMB_X29_Y10_N20
\fd|Add0~14\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~14_combout\ = (\fd|rg\(18) & (!\fd|Add0~13\)) # (!\fd|rg\(18) & ((\fd|Add0~13\) # (GND)))
-- \fd|Add0~15\ = CARRY((!\fd|Add0~13\) # (!\fd|rg\(18)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fd|rg\(18),
	datad => VCC,
	cin => \fd|Add0~13\,
	combout => \fd|Add0~14_combout\,
	cout => \fd|Add0~15\);

-- Location: LCCOMB_X29_Y10_N0
\fd|rg~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|rg~0_combout\ = (!\fd|Equal0~8_combout\ & \fd|Add0~14_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \fd|Equal0~8_combout\,
	datad => \fd|Add0~14_combout\,
	combout => \fd|rg~0_combout\);

-- Location: FF_X29_Y10_N1
\fd|rg[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|rg~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(18));

-- Location: LCCOMB_X29_Y10_N22
\fd|Add0~16\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~16_combout\ = (\fd|rg\(17) & (\fd|Add0~15\ $ (GND))) # (!\fd|rg\(17) & (!\fd|Add0~15\ & VCC))
-- \fd|Add0~17\ = CARRY((\fd|rg\(17) & !\fd|Add0~15\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(17),
	datad => VCC,
	cin => \fd|Add0~15\,
	combout => \fd|Add0~16_combout\,
	cout => \fd|Add0~17\);

-- Location: FF_X29_Y10_N23
\fd|rg[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|Add0~16_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(17));

-- Location: LCCOMB_X29_Y10_N24
\fd|Add0~18\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~18_combout\ = (\fd|rg\(16) & (!\fd|Add0~17\)) # (!\fd|rg\(16) & ((\fd|Add0~17\) # (GND)))
-- \fd|Add0~19\ = CARRY((!\fd|Add0~17\) # (!\fd|rg\(16)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fd|rg\(16),
	datad => VCC,
	cin => \fd|Add0~17\,
	combout => \fd|Add0~18_combout\,
	cout => \fd|Add0~19\);

-- Location: FF_X29_Y10_N25
\fd|rg[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|Add0~18_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(16));

-- Location: LCCOMB_X29_Y10_N26
\fd|Add0~20\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~20_combout\ = (\fd|rg\(15) & (\fd|Add0~19\ $ (GND))) # (!\fd|rg\(15) & (!\fd|Add0~19\ & VCC))
-- \fd|Add0~21\ = CARRY((\fd|rg\(15) & !\fd|Add0~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(15),
	datad => VCC,
	cin => \fd|Add0~19\,
	combout => \fd|Add0~20_combout\,
	cout => \fd|Add0~21\);

-- Location: FF_X29_Y10_N27
\fd|rg[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|Add0~20_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(15));

-- Location: LCCOMB_X29_Y10_N28
\fd|Add0~22\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~22_combout\ = (\fd|rg\(14) & (!\fd|Add0~21\)) # (!\fd|rg\(14) & ((\fd|Add0~21\) # (GND)))
-- \fd|Add0~23\ = CARRY((!\fd|Add0~21\) # (!\fd|rg\(14)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fd|rg\(14),
	datad => VCC,
	cin => \fd|Add0~21\,
	combout => \fd|Add0~22_combout\,
	cout => \fd|Add0~23\);

-- Location: FF_X29_Y10_N29
\fd|rg[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|Add0~22_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(14));

-- Location: LCCOMB_X29_Y10_N30
\fd|Add0~24\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~24_combout\ = (\fd|rg\(13) & (\fd|Add0~23\ $ (GND))) # (!\fd|rg\(13) & (!\fd|Add0~23\ & VCC))
-- \fd|Add0~25\ = CARRY((\fd|rg\(13) & !\fd|Add0~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(13),
	datad => VCC,
	cin => \fd|Add0~23\,
	combout => \fd|Add0~24_combout\,
	cout => \fd|Add0~25\);

-- Location: LCCOMB_X28_Y9_N10
\fd|rg~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|rg~2_combout\ = (!\fd|Equal0~8_combout\ & \fd|Add0~24_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \fd|Equal0~8_combout\,
	datac => \fd|Add0~24_combout\,
	combout => \fd|rg~2_combout\);

-- Location: FF_X28_Y9_N11
\fd|rg[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|rg~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(13));

-- Location: LCCOMB_X29_Y9_N0
\fd|Add0~26\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~26_combout\ = (\fd|rg\(12) & (!\fd|Add0~25\)) # (!\fd|rg\(12) & ((\fd|Add0~25\) # (GND)))
-- \fd|Add0~27\ = CARRY((!\fd|Add0~25\) # (!\fd|rg\(12)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(12),
	datad => VCC,
	cin => \fd|Add0~25\,
	combout => \fd|Add0~26_combout\,
	cout => \fd|Add0~27\);

-- Location: LCCOMB_X28_Y9_N16
\fd|rg~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|rg~3_combout\ = (\fd|Add0~26_combout\ & !\fd|Equal0~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \fd|Add0~26_combout\,
	datad => \fd|Equal0~8_combout\,
	combout => \fd|rg~3_combout\);

-- Location: FF_X28_Y9_N17
\fd|rg[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|rg~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(12));

-- Location: LCCOMB_X29_Y9_N2
\fd|Add0~28\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~28_combout\ = (\fd|rg\(11) & (\fd|Add0~27\ $ (GND))) # (!\fd|rg\(11) & (!\fd|Add0~27\ & VCC))
-- \fd|Add0~29\ = CARRY((\fd|rg\(11) & !\fd|Add0~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fd|rg\(11),
	datad => VCC,
	cin => \fd|Add0~27\,
	combout => \fd|Add0~28_combout\,
	cout => \fd|Add0~29\);

-- Location: LCCOMB_X28_Y9_N28
\fd|rg~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|rg~4_combout\ = (!\fd|Equal0~8_combout\ & \fd|Add0~28_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \fd|Equal0~8_combout\,
	datad => \fd|Add0~28_combout\,
	combout => \fd|rg~4_combout\);

-- Location: FF_X28_Y9_N29
\fd|rg[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|rg~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(11));

-- Location: LCCOMB_X29_Y9_N4
\fd|Add0~30\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~30_combout\ = (\fd|rg\(10) & (!\fd|Add0~29\)) # (!\fd|rg\(10) & ((\fd|Add0~29\) # (GND)))
-- \fd|Add0~31\ = CARRY((!\fd|Add0~29\) # (!\fd|rg\(10)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(10),
	datad => VCC,
	cin => \fd|Add0~29\,
	combout => \fd|Add0~30_combout\,
	cout => \fd|Add0~31\);

-- Location: LCCOMB_X29_Y9_N30
\fd|rg~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|rg~5_combout\ = (!\fd|Equal0~8_combout\ & \fd|Add0~30_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fd|Equal0~8_combout\,
	datac => \fd|Add0~30_combout\,
	combout => \fd|rg~5_combout\);

-- Location: FF_X29_Y9_N31
\fd|rg[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|rg~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(10));

-- Location: LCCOMB_X29_Y9_N6
\fd|Add0~32\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~32_combout\ = (\fd|rg\(9) & (\fd|Add0~31\ $ (GND))) # (!\fd|rg\(9) & (!\fd|Add0~31\ & VCC))
-- \fd|Add0~33\ = CARRY((\fd|rg\(9) & !\fd|Add0~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(9),
	datad => VCC,
	cin => \fd|Add0~31\,
	combout => \fd|Add0~32_combout\,
	cout => \fd|Add0~33\);

-- Location: FF_X29_Y9_N7
\fd|rg[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|Add0~32_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(9));

-- Location: LCCOMB_X29_Y9_N8
\fd|Add0~34\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~34_combout\ = (\fd|rg\(8) & (!\fd|Add0~33\)) # (!\fd|rg\(8) & ((\fd|Add0~33\) # (GND)))
-- \fd|Add0~35\ = CARRY((!\fd|Add0~33\) # (!\fd|rg\(8)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fd|rg\(8),
	datad => VCC,
	cin => \fd|Add0~33\,
	combout => \fd|Add0~34_combout\,
	cout => \fd|Add0~35\);

-- Location: LCCOMB_X28_Y9_N4
\fd|rg~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|rg~6_combout\ = (\fd|Add0~34_combout\ & !\fd|Equal0~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \fd|Add0~34_combout\,
	datad => \fd|Equal0~8_combout\,
	combout => \fd|rg~6_combout\);

-- Location: FF_X28_Y9_N5
\fd|rg[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|rg~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(8));

-- Location: LCCOMB_X29_Y9_N10
\fd|Add0~36\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~36_combout\ = (\fd|rg\(7) & (\fd|Add0~35\ $ (GND))) # (!\fd|rg\(7) & (!\fd|Add0~35\ & VCC))
-- \fd|Add0~37\ = CARRY((\fd|rg\(7) & !\fd|Add0~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(7),
	datad => VCC,
	cin => \fd|Add0~35\,
	combout => \fd|Add0~36_combout\,
	cout => \fd|Add0~37\);

-- Location: FF_X29_Y9_N11
\fd|rg[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|Add0~36_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(7));

-- Location: LCCOMB_X29_Y9_N12
\fd|Add0~38\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~38_combout\ = (\fd|rg\(6) & (!\fd|Add0~37\)) # (!\fd|rg\(6) & ((\fd|Add0~37\) # (GND)))
-- \fd|Add0~39\ = CARRY((!\fd|Add0~37\) # (!\fd|rg\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(6),
	datad => VCC,
	cin => \fd|Add0~37\,
	combout => \fd|Add0~38_combout\,
	cout => \fd|Add0~39\);

-- Location: LCCOMB_X28_Y9_N26
\fd|rg~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|rg~7_combout\ = (\fd|Add0~38_combout\ & !\fd|Equal0~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \fd|Add0~38_combout\,
	datad => \fd|Equal0~8_combout\,
	combout => \fd|rg~7_combout\);

-- Location: FF_X28_Y9_N27
\fd|rg[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|rg~7_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(6));

-- Location: LCCOMB_X29_Y9_N14
\fd|Add0~40\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~40_combout\ = (\fd|rg\(5) & (\fd|Add0~39\ $ (GND))) # (!\fd|rg\(5) & (!\fd|Add0~39\ & VCC))
-- \fd|Add0~41\ = CARRY((\fd|rg\(5) & !\fd|Add0~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \fd|rg\(5),
	datad => VCC,
	cin => \fd|Add0~39\,
	combout => \fd|Add0~40_combout\,
	cout => \fd|Add0~41\);

-- Location: LCCOMB_X29_Y9_N28
\fd|rg~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|rg~8_combout\ = (!\fd|Equal0~8_combout\ & \fd|Add0~40_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fd|Equal0~8_combout\,
	datac => \fd|Add0~40_combout\,
	combout => \fd|rg~8_combout\);

-- Location: FF_X29_Y9_N29
\fd|rg[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|rg~8_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(5));

-- Location: LCCOMB_X29_Y9_N16
\fd|Add0~42\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~42_combout\ = (\fd|rg\(4) & (!\fd|Add0~41\)) # (!\fd|rg\(4) & ((\fd|Add0~41\) # (GND)))
-- \fd|Add0~43\ = CARRY((!\fd|Add0~41\) # (!\fd|rg\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(4),
	datad => VCC,
	cin => \fd|Add0~41\,
	combout => \fd|Add0~42_combout\,
	cout => \fd|Add0~43\);

-- Location: LCCOMB_X28_Y9_N8
\fd|rg~9\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|rg~9_combout\ = (!\fd|Equal0~8_combout\ & \fd|Add0~42_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \fd|Equal0~8_combout\,
	datad => \fd|Add0~42_combout\,
	combout => \fd|rg~9_combout\);

-- Location: FF_X28_Y9_N9
\fd|rg[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|rg~9_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(4));

-- Location: LCCOMB_X29_Y9_N18
\fd|Add0~44\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~44_combout\ = (\fd|rg\(3) & (\fd|Add0~43\ $ (GND))) # (!\fd|rg\(3) & (!\fd|Add0~43\ & VCC))
-- \fd|Add0~45\ = CARRY((\fd|rg\(3) & !\fd|Add0~43\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(3),
	datad => VCC,
	cin => \fd|Add0~43\,
	combout => \fd|Add0~44_combout\,
	cout => \fd|Add0~45\);

-- Location: LCCOMB_X29_Y9_N26
\fd|rg~10\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|rg~10_combout\ = (!\fd|Equal0~8_combout\ & \fd|Add0~44_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \fd|Equal0~8_combout\,
	datad => \fd|Add0~44_combout\,
	combout => \fd|rg~10_combout\);

-- Location: FF_X29_Y9_N27
\fd|rg[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|rg~10_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(3));

-- Location: LCCOMB_X29_Y9_N20
\fd|Add0~46\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~46_combout\ = (\fd|rg\(2) & (!\fd|Add0~45\)) # (!\fd|rg\(2) & ((\fd|Add0~45\) # (GND)))
-- \fd|Add0~47\ = CARRY((!\fd|Add0~45\) # (!\fd|rg\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(2),
	datad => VCC,
	cin => \fd|Add0~45\,
	combout => \fd|Add0~46_combout\,
	cout => \fd|Add0~47\);

-- Location: LCCOMB_X28_Y9_N12
\fd|rg~11\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|rg~11_combout\ = (\fd|Add0~46_combout\ & !\fd|Equal0~8_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \fd|Add0~46_combout\,
	datad => \fd|Equal0~8_combout\,
	combout => \fd|rg~11_combout\);

-- Location: FF_X28_Y9_N13
\fd|rg[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|rg~11_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(2));

-- Location: LCCOMB_X29_Y9_N22
\fd|Add0~48\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~48_combout\ = (\fd|rg\(1) & (\fd|Add0~47\ $ (GND))) # (!\fd|rg\(1) & (!\fd|Add0~47\ & VCC))
-- \fd|Add0~49\ = CARRY((\fd|rg\(1) & !\fd|Add0~47\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(1),
	datad => VCC,
	cin => \fd|Add0~47\,
	combout => \fd|Add0~48_combout\,
	cout => \fd|Add0~49\);

-- Location: FF_X29_Y9_N23
\fd|rg[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|Add0~48_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(1));

-- Location: LCCOMB_X29_Y9_N24
\fd|Add0~50\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Add0~50_combout\ = \fd|Add0~49\ $ (\fd|rg\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \fd|rg\(0),
	cin => \fd|Add0~49\,
	combout => \fd|Add0~50_combout\);

-- Location: LCCOMB_X28_Y9_N14
\fd|rg~12\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|rg~12_combout\ = (!\fd|Equal0~8_combout\ & \fd|Add0~50_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \fd|Equal0~8_combout\,
	datad => \fd|Add0~50_combout\,
	combout => \fd|rg~12_combout\);

-- Location: FF_X28_Y9_N15
\fd|rg[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|rg~12_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|rg\(0));

-- Location: LCCOMB_X28_Y9_N6
\fd|Equal0~7\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Equal0~7_combout\ = (\fd|rg\(0) & !\fd|rg\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \fd|rg\(0),
	datad => \fd|rg\(1),
	combout => \fd|Equal0~7_combout\);

-- Location: LCCOMB_X28_Y9_N24
\fd|Equal0~5\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Equal0~5_combout\ = (\fd|rg\(6) & (\fd|rg\(8) & (!\fd|rg\(7) & !\fd|rg\(9))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(6),
	datab => \fd|rg\(8),
	datac => \fd|rg\(7),
	datad => \fd|rg\(9),
	combout => \fd|Equal0~5_combout\);

-- Location: LCCOMB_X28_Y10_N30
\fd|Equal0~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Equal0~1_combout\ = (!\fd|rg\(22) & (!\fd|rg\(20) & (!\fd|rg\(21) & !\fd|rg\(19))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(22),
	datab => \fd|rg\(20),
	datac => \fd|rg\(21),
	datad => \fd|rg\(19),
	combout => \fd|Equal0~1_combout\);

-- Location: LCCOMB_X28_Y9_N18
\fd|Equal0~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Equal0~3_combout\ = (\fd|rg\(13) & (\fd|rg\(12) & (\fd|rg\(10) & \fd|rg\(11))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(13),
	datab => \fd|rg\(12),
	datac => \fd|rg\(10),
	datad => \fd|rg\(11),
	combout => \fd|Equal0~3_combout\);

-- Location: LCCOMB_X29_Y10_N4
\fd|Equal0~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Equal0~2_combout\ = (!\fd|rg\(23) & (!\fd|rg\(25) & (!\fd|rg\(24) & !\fd|rg\(14))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(23),
	datab => \fd|rg\(25),
	datac => \fd|rg\(24),
	datad => \fd|rg\(14),
	combout => \fd|Equal0~2_combout\);

-- Location: LCCOMB_X28_Y10_N12
\fd|Equal0~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Equal0~0_combout\ = (!\fd|rg\(16) & (!\fd|rg\(17) & (\fd|rg\(18) & !\fd|rg\(15))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(16),
	datab => \fd|rg\(17),
	datac => \fd|rg\(18),
	datad => \fd|rg\(15),
	combout => \fd|Equal0~0_combout\);

-- Location: LCCOMB_X28_Y9_N22
\fd|Equal0~4\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Equal0~4_combout\ = (\fd|Equal0~1_combout\ & (\fd|Equal0~3_combout\ & (\fd|Equal0~2_combout\ & \fd|Equal0~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fd|Equal0~1_combout\,
	datab => \fd|Equal0~3_combout\,
	datac => \fd|Equal0~2_combout\,
	datad => \fd|Equal0~0_combout\,
	combout => \fd|Equal0~4_combout\);

-- Location: LCCOMB_X28_Y9_N20
\fd|Equal0~6\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Equal0~6_combout\ = (\fd|rg\(3) & (\fd|rg\(5) & (\fd|rg\(4) & \fd|rg\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fd|rg\(3),
	datab => \fd|rg\(5),
	datac => \fd|rg\(4),
	datad => \fd|rg\(2),
	combout => \fd|Equal0~6_combout\);

-- Location: LCCOMB_X28_Y9_N0
\fd|Equal0~8\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|Equal0~8_combout\ = (\fd|Equal0~7_combout\ & (\fd|Equal0~5_combout\ & (\fd|Equal0~4_combout\ & \fd|Equal0~6_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \fd|Equal0~7_combout\,
	datab => \fd|Equal0~5_combout\,
	datac => \fd|Equal0~4_combout\,
	datad => \fd|Equal0~6_combout\,
	combout => \fd|Equal0~8_combout\);

-- Location: LCCOMB_X28_Y9_N30
\fd|s~feeder\ : cycloneive_lcell_comb
-- Equation(s):
-- \fd|s~feeder_combout\ = \fd|Equal0~8_combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \fd|Equal0~8_combout\,
	combout => \fd|s~feeder_combout\);

-- Location: FF_X28_Y9_N31
\fd|s\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \osc~inputclkctrl_outclk\,
	d => \fd|s~feeder_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \fd|s~q\);

-- Location: CLKCTRL_G19
\fd|s~clkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \fd|s~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \fd|s~clkctrl_outclk\);

-- Location: IOIBUF_X45_Y34_N15
\keys[0]~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_keys(0),
	o => \keys[0]~input_o\);

-- Location: LCCOMB_X43_Y33_N6
\r0|i~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \r0|i~1_combout\ = (!\keys[0]~input_o\ & (!\r0|i\(0) & !\r0|i\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \keys[0]~input_o\,
	datac => \r0|i\(0),
	datad => \r0|i\(2),
	combout => \r0|i~1_combout\);

-- Location: FF_X43_Y33_N7
\r0|i[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \fd|s~clkctrl_outclk\,
	d => \r0|i~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \r0|i\(0));

-- Location: LCCOMB_X43_Y33_N28
\r0|i~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \r0|i~0_combout\ = (!\r0|i\(2) & (!\keys[0]~input_o\ & (\r0|i\(0) $ (\r0|i\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \r0|i\(2),
	datab => \r0|i\(0),
	datac => \r0|i\(1),
	datad => \keys[0]~input_o\,
	combout => \r0|i~0_combout\);

-- Location: FF_X43_Y33_N29
\r0|i[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \fd|s~clkctrl_outclk\,
	d => \r0|i~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \r0|i\(1));

-- Location: LCCOMB_X43_Y33_N4
\r0|l~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \r0|l~3_combout\ = (\r0|i\(0) & (!\keys[0]~input_o\ & (!\r0|i\(2) & \r0|i\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \r0|i\(0),
	datab => \keys[0]~input_o\,
	datac => \r0|i\(2),
	datad => \r0|i\(1),
	combout => \r0|l~3_combout\);

-- Location: FF_X43_Y33_N5
\r0|i[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \fd|s~clkctrl_outclk\,
	d => \r0|l~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \r0|i\(2));

-- Location: LCCOMB_X43_Y33_N16
\r0|l~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \r0|l~0_combout\ = (!\keys[0]~input_o\ & !\r0|i\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \keys[0]~input_o\,
	datad => \r0|i\(2),
	combout => \r0|l~0_combout\);

-- Location: FF_X43_Y33_N17
\r0|l[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \fd|s~clkctrl_outclk\,
	d => \r0|l~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \r0|l\(0));

-- Location: FF_X43_Y33_N27
\r1|i[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \fd|s~clkctrl_outclk\,
	d => \r1|l~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \r1|i\(2));

-- Location: LCCOMB_X43_Y33_N10
\r1|i~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \r1|i~0_combout\ = (!\keys[1]~input_o\ & (!\r1|i\(0) & !\r1|i\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \keys[1]~input_o\,
	datac => \r1|i\(0),
	datad => \r1|i\(2),
	combout => \r1|i~0_combout\);

-- Location: FF_X43_Y33_N11
\r1|i[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \fd|s~clkctrl_outclk\,
	d => \r1|i~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \r1|i\(0));

-- Location: LCCOMB_X43_Y33_N0
\r1|i~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \r1|i~1_combout\ = (!\keys[1]~input_o\ & (!\r1|i\(2) & (\r1|i\(0) $ (\r1|i\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \keys[1]~input_o\,
	datab => \r1|i\(0),
	datac => \r1|i\(1),
	datad => \r1|i\(2),
	combout => \r1|i~1_combout\);

-- Location: FF_X43_Y33_N1
\r1|i[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \fd|s~clkctrl_outclk\,
	d => \r1|i~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \r1|i\(1));

-- Location: LCCOMB_X43_Y33_N26
\r1|l~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \r1|l~0_combout\ = (!\keys[1]~input_o\ & (\r1|i\(1) & (!\r1|i\(2) & \r1|i\(0))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \keys[1]~input_o\,
	datab => \r1|i\(1),
	datac => \r1|i\(2),
	datad => \r1|i\(0),
	combout => \r1|l~0_combout\);

-- Location: FF_X43_Y33_N3
\r1|l[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \fd|s~clkctrl_outclk\,
	asdata => \r1|l~0_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \r1|l\(0));

-- Location: LCCOMB_X43_Y33_N2
\mul|ld[0]~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \mul|ld[0]~0_combout\ = (\keys[1]~input_o\ & (\r0|l\(0) & ((!\keys[0]~input_o\)))) # (!\keys[1]~input_o\ & (((\r1|l\(0) & \keys[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \keys[1]~input_o\,
	datab => \r0|l\(0),
	datac => \r1|l\(0),
	datad => \keys[0]~input_o\,
	combout => \mul|ld[0]~0_combout\);

-- Location: LCCOMB_X43_Y33_N24
\r1|l~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \r1|l~1_combout\ = (!\keys[1]~input_o\ & (\r1|i\(1) & !\r1|i\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \keys[1]~input_o\,
	datac => \r1|i\(1),
	datad => \r1|i\(2),
	combout => \r1|l~1_combout\);

-- Location: FF_X43_Y33_N25
\r1|l[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \fd|s~clkctrl_outclk\,
	d => \r1|l~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \r1|l\(1));

-- Location: LCCOMB_X43_Y33_N30
\r0|l~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \r0|l~1_combout\ = (!\keys[0]~input_o\ & (!\r0|i\(2) & ((\r0|i\(1)) # (\r0|i\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \r0|i\(1),
	datab => \keys[0]~input_o\,
	datac => \r0|i\(0),
	datad => \r0|i\(2),
	combout => \r0|l~1_combout\);

-- Location: FF_X43_Y33_N31
\r0|l[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \fd|s~clkctrl_outclk\,
	d => \r0|l~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \r0|l\(1));

-- Location: LCCOMB_X43_Y33_N20
\mul|ld[1]~1\ : cycloneive_lcell_comb
-- Equation(s):
-- \mul|ld[1]~1_combout\ = (\keys[1]~input_o\ & (((\r0|l\(1) & !\keys[0]~input_o\)))) # (!\keys[1]~input_o\ & (\r1|l\(1) & ((\keys[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \keys[1]~input_o\,
	datab => \r1|l\(1),
	datac => \r0|l\(1),
	datad => \keys[0]~input_o\,
	combout => \mul|ld[1]~1_combout\);

-- Location: LCCOMB_X43_Y33_N12
\r0|l~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \r0|l~2_combout\ = (!\keys[0]~input_o\ & (\r0|i\(1) & !\r0|i\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \keys[0]~input_o\,
	datac => \r0|i\(1),
	datad => \r0|i\(2),
	combout => \r0|l~2_combout\);

-- Location: FF_X43_Y33_N13
\r0|l[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \fd|s~clkctrl_outclk\,
	d => \r0|l~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \r0|l\(2));

-- Location: LCCOMB_X43_Y33_N22
\r1|l~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \r1|l~2_combout\ = (!\keys[1]~input_o\ & (!\r1|i\(2) & ((\r1|i\(0)) # (\r1|i\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \keys[1]~input_o\,
	datab => \r1|i\(0),
	datac => \r1|i\(1),
	datad => \r1|i\(2),
	combout => \r1|l~2_combout\);

-- Location: FF_X43_Y33_N23
\r1|l[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \fd|s~clkctrl_outclk\,
	d => \r1|l~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \r1|l\(2));

-- Location: LCCOMB_X43_Y33_N18
\mul|ld[2]~2\ : cycloneive_lcell_comb
-- Equation(s):
-- \mul|ld[2]~2_combout\ = (\keys[1]~input_o\ & (\r0|l\(2) & ((!\keys[0]~input_o\)))) # (!\keys[1]~input_o\ & (((\r1|l\(2) & \keys[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \keys[1]~input_o\,
	datab => \r0|l\(2),
	datac => \r1|l\(2),
	datad => \keys[0]~input_o\,
	combout => \mul|ld[2]~2_combout\);

-- Location: LCCOMB_X43_Y33_N8
\r1|l~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \r1|l~3_combout\ = (!\keys[1]~input_o\ & !\r1|i\(2))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \keys[1]~input_o\,
	datad => \r1|i\(2),
	combout => \r1|l~3_combout\);

-- Location: FF_X43_Y33_N9
\r1|l[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \fd|s~clkctrl_outclk\,
	d => \r1|l~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \r1|l\(3));

-- Location: FF_X43_Y33_N15
\r0|l[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \fd|s~clkctrl_outclk\,
	asdata => \r0|l~3_combout\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \r0|l\(3));

-- Location: LCCOMB_X43_Y33_N14
\mul|ld[3]~3\ : cycloneive_lcell_comb
-- Equation(s):
-- \mul|ld[3]~3_combout\ = (\keys[1]~input_o\ & (((\r0|l\(3) & !\keys[0]~input_o\)))) # (!\keys[1]~input_o\ & (\r1|l\(3) & ((\keys[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010010100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \keys[1]~input_o\,
	datab => \r1|l\(3),
	datac => \r0|l\(3),
	datad => \keys[0]~input_o\,
	combout => \mul|ld[3]~3_combout\);

ww_leds(0) <= \leds[0]~output_o\;

ww_leds(1) <= \leds[1]~output_o\;

ww_leds(2) <= \leds[2]~output_o\;

ww_leds(3) <= \leds[3]~output_o\;
END structure;


