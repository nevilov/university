LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;
entity leds_and_buttons is
port ( osc : in std_logic;
	keys : in std_logic_vector (0 to 1);
	leds : out std_logic_vector (3 downto 0)
);
end leds_and_buttons;
architecture l_a_b of leds_and_buttons is
signal clk : std_logic;
signal Q0, Q1 : std_logic_vector (3 downto 0) := "0000";
component f_divider is
port ( osc : in std_logic;
	clk : out std_logic
);
end component;

component rg0 is
port ( clk: in std_logic;
	e : in std_logic;
	Q : out std_logic_vector (3 downto 0)
);
end component;
component rg1 is
port ( clk : in std_logic;
e : in std_logic;
Q : out std_logic_vector (3 downto 0)
);
end component;
component multiplexer is
port (
	rg0: in std_logic_vector (3 downto 0);
	rg1: in std_logic_vector(3 downto 0);
	keys: in std_logic_vector (0 to 1);
	leds: out std_logic_vector(3 downto 0)
);
end component;
begin
	fd  : f_divider port map (osc, clk);
	r0 : rg0 port map (osc, keys(0), Q0); --r0  : rg0 port map (clk, keys(0), Q0);
	r1 : rg1 port map (osc, keys(1), Q1); --r1  : rg1 port map (clk, keys(1), Q1);
	mul : multiplexer port map (Q0, Q1, keys, leds);
end;
