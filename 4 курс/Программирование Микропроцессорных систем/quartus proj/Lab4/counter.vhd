library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity counter_serial is
port( 
	c: in std_logic;
	Q: out std_logic_vector(0 to 5)
);
end counter_serial;

architecture c_s of counter_serial is
signal a: std_logic_vector(0 to 3) := "0000"; -- Сигналы с выходов конъюкторов
signal b: std_logic_vector(0 to 5) := "000000"; -- Сигналы с выходов тригеров

component jk
port ( 
	J: in std_logic;
	K: in std_logic;
	C: in std_logic;
	Q: out std_logic
);
end component;

begin
	ff_1: jk port map ('1','1', c, b(0));
	ff_2: jk port map (b(0), b(0), c, b(1));
	ff: for i in 0 to 3 generate

	ff_3_to_6: jk port map (a(i), a(i), c, b(i+2));
	end generate;
	a(0) <= b(0) and b(1);
	a(1) <= a(0) and b(2);
	a(2) <= a(1) and b(3);
	a(3) <= a(2) and b(4);

	Q<= b;
end;
