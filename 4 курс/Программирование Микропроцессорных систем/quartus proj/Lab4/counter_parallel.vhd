LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;

entity counter_parallel is
	port (	c:in std_logic;
				Q:out std_logic_vector (0 to 4)
				);
	end counter_parallel;

architecture c_p of counter_parallel is
	signal a: std_logic_vector (0 to 4) := "00000";
	signal b: std_logic_vector (0 to 4) := "00000";
	signal s: std_logic := '1';
	component jk
		port (	J:in std_logic;
					K:in std_logic;
					C:in std_logic;
					Q:out std_logic
					);
		end component;
	begin
	ff: for i in 0 to 4 generate
		ff_0_to_4 : jk port map (a(i), a(i), c, b(i));
		end generate;
	s <= b(4) and b(3) and b(2) and (not b(1)) and (not b(0));
	
	a(0) <= not s;
	
	a(1) <= b(0);
	
	a(2) <= 
		(b(1) and b(0)) or
		(b(4) and (not b(3)) and b(1) and b(0)) or 
		(b(4) and b(3) and (not b(2)) and b(1) and b(0)) or
		(b(4) and b(3) and b(2) and (not b(1)) and (not b(0)));
		
	a(3) <= 
		((not b(3)) and b(2) and b(1) and b(0)) or
		((not b(4)) and b(3) and b(2) and b(1) and b(0)) or
		(b(4) and b(3) and b(2) and (not b(1)) and (not b(0)))
		;
		
	a(4) <= 
		((not b(4)) and b(3) and b(2) and b(1) and b(0)) or
		(b(4) and b(3) and b(2) and (not b(1)) and (not b(0)));
	
	Q <= b;
end;
