library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity jk is
port ( 
	J: in std_logic;
	K: in std_logic;
	C: in std_logic;
	Q: out std_logic
);
end jk;

architecture a of jk is
signal s: std_logic := '0'; --Для корректной работы

begin
	process (C, J, K)
		begin
			if(rising_edge(C)) then
				if(J = '0' and K='0') then
					s <= s;
				elsif(J = '0' and K='1') then
					s <= '0';
				elsif(J = '1' and K='0') then
					s <= '1';
				elsif(J = '1' and K='1') then
					s <= not s;
				end if;
			end if;
	end process;
	Q <= s;
end;
	
		