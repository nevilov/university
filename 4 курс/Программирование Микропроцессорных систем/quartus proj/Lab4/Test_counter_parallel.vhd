LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;

entity Test_counter_parallel is
end Test_counter_parallel;

architecture Behavior of Test_counter_parallel is
	component counter_parallel is
		port (	c:in std_logic;
					Q:out std_logic_vector (0 to 4)
		);
	end component;
	signal c: std_logic;
	signal Q: std_logic_vector (0 to 4);
	begin
	p1 : counter_parallel port map (c => c, Q => Q);
	C <= 	'1',
			'0' after 50 ns,
			'1' after 100 ns,
			'0' after 150 ns,
			'1' after 200 ns,
			'0' after 250 ns,
			'1' after 300 ns,
			'0' after 350 ns,
			'1' after 400 ns,
			'0' after 450 ns,
			'1' after 500 ns,
			'0' after 550 ns,
			'1' after 600 ns,
			'0' after 650 ns,
			'1' after 700 ns,
			'0' after 750 ns,
			'1' after 800 ns,
			'0' after 850 ns,
			'1' after 900 ns,
			'0' after 950 ns,
			
			'1' after 1000 ns,
			'0' after 1050 ns,
			'1' after 1100 ns,
			'0' after 1150 ns,
			'1' after 1200 ns,
			'0' after 1250 ns,
			'1' after 1300 ns,
			'0' after 1350 ns,
			'1' after 1400 ns,
			'0' after 1450 ns,
			'1' after 1500 ns,
			'0' after 1550 ns,
			'1' after 1600 ns,
			'0' after 1650 ns,
			'1' after 1700 ns,
			'0' after 1750 ns,
			'1' after 1800 ns,
			'0' after 1850 ns,
			'1' after 1900 ns,
			'0' after 1950 ns,
			
			'1' after 2000 ns,
			'0' after 2050 ns,
			'1' after 2100 ns,
			'0' after 2150 ns,
			'1' after 2200 ns,
			'0' after 2250 ns,
			'1' after 2300 ns,
			'0' after 2350 ns,
			'1' after 2400 ns,
			'0' after 2450 ns,
			'1' after 2500 ns,
			'0' after 2550 ns,
			'1' after 2600 ns,
			'0' after 2650 ns,
			'1' after 2700 ns,
			'0' after 2750 ns,
			'1' after 2800 ns,
			'0' after 2850 ns,
			'1' after 2900 ns,
			'0' after 2950 ns,
			'1' after 3000 ns;
end Behavior;