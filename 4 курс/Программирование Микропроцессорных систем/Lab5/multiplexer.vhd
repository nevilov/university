LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;
entity multiplexer is
port( rg0 : in std_logic_vector (3 downto 0);
	rg1 : in std_logic_vector (3 downto 0);
	keys : in std_logic_vector (0 to 1);
	leds : out std_logic_vector (3 downto 0)
);
end multiplexer;

architecture m of multiplexer is
	signal ld : std_logic_vector (3 downto 0) := "0000";
	begin
		process(keys, rg0, rg1, ld)
		begin
		if (keys = "01") then
			ld <= rg0;
		elsif (keys = "10") then
			ld <= rg1;
		elsif(keys = "00" or keys = "11") then
			ld <= "0000";
		end if;
	end process;
	leds <= ld;
end;
