LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;

entity rg1 is
	port 	(	clk : in std_logic;
				e   : in std_logic;
				Q   : out std_logic_vector (3 downto 0) := "0000"
			);
end rg1;

architecture r of rg1 is
	signal l : std_logic_vector (3 downto 0) := "0000";
	
begin
	process (clk, e)
		variable i : integer := 0;
		begin
		if (rising_edge(clk)) then
			if (e = '0') then
				i := i + 1;
				case i is
					when 1 =>
					l <= "1000";
					when 2 =>
					l <= "1100";
					when 3 =>
					l <= "1110";
					when 4 =>
					l <= "1111";
					when others =>
					l <= "0000";
					i := 0;
					end case;
			else
				i := 0;
				l <= "0000";
				end if;
			end if;
		end process;
	Q <= l;
end;