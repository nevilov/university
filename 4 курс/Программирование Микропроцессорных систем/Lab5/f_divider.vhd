LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;
	entity f_divider is
	port ( osc : in std_logic;
		clk : out std_logic
	);
	end f_divider;

architecture f_d of f_divider is
signal rg : std_logic_vector (0 to 25) := "00000000000000000000000000";
signal s : std_logic;
begin
	process (osc)
	begin
	if (rising_edge(osc)) then
		if (rg = "0010111110101111000010000000") then
			rg <= "00000000000000000000000000";
			s <= '1';
		else
			rg <= rg + '1';
			s <= '0';
		end if;
	end if;
	end process;
	clk <= s;
end;
