LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE ieee.std_logic_unsigned.ALL;

entity test_leds_and_buttons is
	end test_leds_and_buttons;

architecture behavior of test_leds_and_buttons is
	component leds_and_buttons
	port	(	osc  : in std_logic;
				keys : in std_logic_vector (0 to 1);
				leds : out std_logic_vector (3 downto 0)
			);
	end component;
	signal osc : std_logic;
	signal keys : std_logic_vector (0 to 1);
	signal leds : std_logic_vector (3 downto 0);
	begin
	p1 : leds_and_buttons port map (osc => osc, keys => keys, leds => leds);
	keys <= 	"01",
				"10" after 1000 ns;
	osc <= 	'1',
				'0' after 50 ns,
				'1' after 100 ns,
				'0' after 150 ns,
				'1' after 200 ns,
				'0' after 250 ns,
				'1' after 300 ns,
				'0' after 350 ns,
				'1' after 400 ns,
				'0' after 450 ns,
				'1' after 500 ns,
				'0' after 550 ns,
				'1' after 600 ns,
				'0' after 650 ns,
				'1' after 700 ns,
				'0' after 750 ns,
				'1' after 800 ns,
				'0' after 850 ns,
				'1' after 900 ns,
				'0' after 950 ns,
				'1' after 1000 ns,
				'0' after 1050 ns,
				'1' after 1100 ns,
				'0' after 1150 ns,
				'1' after 1200 ns,
				'0' after 1250 ns,
				'1' after 1300 ns,
				'0' after 1350 ns,
				'1' after 1400 ns,
				'0' after 1450 ns,
				'1' after 1500 ns,
				'0' after 1550 ns,
				'1' after 1600 ns,
				'0' after 1650 ns,
				'1' after 1700 ns,
				'0' after 1750 ns,
				'1' after 1800 ns,
				'0' after 1850 ns,
				'1' after 1900 ns,
				'0' after 1950 ns,
				'1' after 2000 ns;
end behavior;