﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace TruthTable
{
    class Program
    {
        static void Main(string[] args)
        {
           Console.BackgroundColor = ConsoleColor.Blue;
            bool a = false, b = false, c = false, d = false;
            string table = "";
            Console.WriteLine("| A | B | C | D |");
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    for (int k = 0; k < 2; k++)
                    {
                        for (int l = 0; l < 2; l++)
                        {
                            Console.WriteLine($"| {a} | {b} | {c} | {d} |");
                            table += $"| {a} | {b} | {c} | {d} | \n";

                            d = !d;
                        }

                        c = !c;
                    }

                    b = !b;
                }

                a = !a;
            }

            table = table.Replace("true", "1");
            table = table.Replace("false", "0");
            Console.WriteLine(table);
        }
    }
}
