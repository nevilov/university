﻿% Вычислить: W = (3+ i)+(6+i)+(9+i)+….+(m+i).

implement main
    open core

class predicates
    findSum : (integer Arg, integer H, integer I, integer M, integer Result [out]).
clauses
    findSum(Arg, H, I, M, 0) :-
        Arg > M,
        !.

    findSum(Arg, H, I, M, Result) :-
        R1 = Arg + I,
        findSum(R1, H, I, M, Res),
        Result = Res + Result.
    run() :-
        console::init(),
        stdio::write("Введите  i, M"),
        findSum(3, 3, stdio::read(), stdio::read(), Result),
        stdIO::write(Result),
        findSum(3, 3, stdio::read(), stdio::read(), Result1).

end implement main

goal
    console::runUtf8(main::run).
