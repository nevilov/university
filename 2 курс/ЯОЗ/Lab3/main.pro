﻿% Заданы три числа a, b и c. Определить, сколько из них равны максимальному элементу.

implement main
    open core

class predicates
    incrementCondition : (integer M, integer V, integer R [out]).
    max : (integer A, integer B, integer C, integer Res [out]).
    max : (integer A, integer B, integer Res [out]).
    findMaxElCount : (integer A, integer B, integer C, integer Result [out]).

clauses
    findMaxElCount(A, B, C, Result) :-
        max(A, B, C, MaxEl),
        incrementCondition(MaxEl, A, R1),
        incrementCondition(MaxEl, B, R2),
        incrementCondition(MaxEl, C, R3),
        Result = R1 + R2 + R3.

    incrementCondition(MaxEl, V, 1) :-
        MaxEl = V,
        !.
    incrementCondition(_, _, 0).

    max(A, B, C, Res) :-
        max(A, B, M1),
        max(M1, C, Res).

    max(A, B, A) :-
        A > B,
        !.
    max(_, B, B).

    run() :-
        console::init(),
        stdio::write("Введите 3 числа"),
        stdio::nl,
        findMaxElCount(stdio::read(), stdio::read(), stdio::read(), Res),
        stdio::write("Количество максимальных элементов = ", Res),
        max(stdio::read(), stdio::read(), stdio::read(), Max1),
        stdio::nl.

end implement main

goal
    console::runUtf8(main::run).
