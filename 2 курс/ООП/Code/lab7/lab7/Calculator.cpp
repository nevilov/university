#include "Calculator.h"
#include <math.h>
#include <stdexcept>

using namespace std;

class Exception : public runtime_error {
public:
	Exception()
		: runtime_error("�������������� ������\n")
	{
	}
};


Calculator::Calculator(int firstDigit, int secondDigit) {
	this->firstDigit = firstDigit;
	this->secondDigit = secondDigit;
}

double Calculator::Calculate(char op) {
	switch (op)
	{
	case '+':
		return firstDigit + secondDigit;
	case '-':
		return firstDigit - secondDigit;
	case '*':
		return firstDigit * secondDigit;
	case '/':
		return firstDigit / secondDigit;
	default:
		cout << "�������������� �������� " << op << endl;
		break;
	}
}
double Calculator::CalculateSpecificExpression() {
	double result;
	try {
		if (firstDigit == 0) {
			throw Exception();
		}

		double innerExp = cos(1 / firstDigit);

		if (innerExp <= 0) {
			throw Exception();
		}

		result = log(innerExp);
	}
	catch (runtime_error e) {
		cout << "��������� ������ " << e.what() <<endl;
		return NULL;
	}
	return result;
}

