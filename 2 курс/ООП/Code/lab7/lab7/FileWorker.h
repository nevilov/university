#pragma once
#include <iostream>
#include <fstream>
#include <string> 


using namespace std;

class FileWorker {
	string fileName;
	ofstream writeStream;
public:
	FileWorker(string fileName);
	void SaveResultToFile(int result);
};