#include <iostream>
#include <cmath>
#include <stdexcept>
using namespace std;

class Calculator {
	double firstDigit;
	double secondDigit;
public:
	Calculator(int firstDigit, int secondDigit);
	double Calculate(char op);
	double CalculateSpecificExpression();
};