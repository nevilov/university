#include "DMatr.h"
#include <cstdlib>

using namespace std;

double** arrayDoubleGenerator(unsigned int dim1, unsigned int dim2);
int operator+(Dmatr matr1);
bool operator==(Dmatr matr1, Dmatr matr2);

int main() {
    setlocale(LC_ALL, "Ru");
	const int cols = 5;
	const int rows = 5;
    double** matrix = arrayDoubleGenerator(rows, cols);
    double** matrix2 = arrayDoubleGenerator(rows, cols);

	Dmatr dmatr(matrix, rows, cols);
	Dmatr dmatr2(matrix2, rows, cols);

    dmatr.Print();
    dmatr2.Print();

    cout << "�������� ������� ������� ��������� ������������� ���������� !. �������� ����� " << !dmatr << endl;
    cout << "��������� ����� ������������� ������������ ����� " << dmatr.operator*(dmatr2) << endl;

    cout << "������� � ������� ������� ��� ��������, ������� ������ 30" << endl;
    (dmatr >> 30).Print();
    cout << "������� � ������ ������� ��� ��������, ������� ������ 30" << endl;
    (dmatr2 << 30).Print();

    cout << "��������� ������ ������������� ��������. �������� ����� " << operator==(dmatr, dmatr2) << endl;

	cout << "������� 1" << endl;
	dmatr.Print();

    cout << "����� ��������� ������ ������������� �������� = " << operator+(dmatr) << endl;
}

double** arrayDoubleGenerator(unsigned int dim1, unsigned int dim2) {
    double** ptrary = new double* [dim1];
    for (int i = 0; i < dim1; i++) {
        ptrary[i] = new double[dim2];
    }

    for (int i = 0; i < dim1; i++)
    {
        for (int j = 0; j < dim2; j++)
        {
            ptrary[i][j] = rand() % 50;
        }
    }

    return ptrary;
}


int operator+(Dmatr matr1) {
	int sum = 0;

	for (int i = 0; i < matr1.rows; ++i) {
		for (int j = 0; j < matr1.cols; ++j) {
			sum += matr1.matr[i][j];
		}
	}

	return sum;
}

bool operator==(Dmatr matr1, Dmatr matr2) {
	if (matr1.cols != matr1.cols) {
		return false;
	}

	if (matr1.rows != matr1.rows) {
		return false;
	}

	for (int i = 0; i < matr1.rows; ++i) {
		for (int j = 0; j < matr1.rows; ++j) {
			if (matr1.matr[i][j] != matr2.matr[i][j]) {
				return false;
			}
		}
	}

	return true;
}