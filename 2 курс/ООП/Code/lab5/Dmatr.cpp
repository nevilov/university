#include "DMatr.h"

Dmatr::Dmatr(double** matr, int rows, int cols) {
	this->matr = matr;
	this->cols = cols;
	this->rows = rows;
}

Dmatr::~Dmatr() {
	cout << "���������� ������ Dmatr ��� ������" << endl;
}

bool Dmatr::operator!() {
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) {
			if (matr[i][j] == 0) {
				return true;
			}
		}
	}

	return false;
}

double Dmatr::operator*(Dmatr matr) {
	double sum = 0;
	
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < rows; ++j) {
			for (int k = 0; k < matr.rows; ++k) {
				for (int f = 0; f < matr.cols; ++f) {
					sum += this->matr[i][j] + matr.matr[k][f];
				}
			}
		}
	}

	return sum;
}

void Dmatr::Print() {
	cout << endl;
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; j++) {
			cout << matr[i][j] << "\t";
		}
	}
	cout << endl;
}

Dmatr Dmatr::operator<<(int num) {
	Dmatr matrix(matr, rows, cols);
	
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; j++) {
			if (matrix.matr[i][j] < num) {
				matrix.matr[i][j] = 0;
			}
		}
	}

	return matrix;
}

Dmatr Dmatr::operator>>(int num) {
	Dmatr matrix(matr, rows, cols);

	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; j++) {
			if (matrix.matr[i][j] > num) {
				matrix.matr[i][j] = 0;
			}
		}
	}

	return matrix;
}