#include "DMatr.h"

int operator+(Dmatr matr1) {
	int sum = 0;

	for (int i = 0; i < matr1.rows; ++i) {
		for (int j = 0; j < matr1.cols; ++j) {
			sum += matr1.matr[matr1.rows][matr1.cols];
		}
	}

	return sum;
}

bool operator==(Dmatr matr1, Dmatr matr2) {
	if (matr1.cols != matr1.cols) {
		return false;
	}

	if (matr1.rows != matr1.rows) {
		return false;
	}
		
	for (int i = 0; i < matr1.rows; ++i) {
		for (int j = 0; j < matr1.rows; ++j) {
			if (matr1.matr[i][j] != matr2.matr[i][j]) {
				return false;
			}
		}
	}

	return true;
}