#pragma once
#include <iostream>
using namespace std;

class Dmatr {
	double** matr;
	int cols;
	int rows;
public:
	Dmatr(double** matr, int rows, int cols);

	~Dmatr();

	void Print();
	
	// ������������� ���������, �������� ������� ������� ���������
	bool operator!();

	// ������� ������������� �������, ����������� ����� ��������� �������
	friend int operator+(Dmatr matr1);
	
	// �������� ����� ������, ����������� ����� ������������� ����������� ������������ ���� ������
	double operator*(Dmatr matr1);

	// �������� ������������� �������, ����������� ��������� ���� ��������
	friend bool operator==(Dmatr matr1, Dmatr matr2);

	// �������� ��� �������� ������� ������ num
	Dmatr operator>>(int num);

	// �������� ��� �������� ������� ������ num
	Dmatr operator<<(int count);
};
