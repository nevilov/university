#pragma once
#include <iostream>
#include "BaseMatrix.h"

using namespace std;

class DoubleMatrix : public BaseMatrix {
private:
	double** matr;
	int columns;
	int rows;
public:
	DoubleMatrix();
	~DoubleMatrix();
	DoubleMatrix(int columns, int rows, double** matr);
	double** ArrayGenerator(unsigned int dim1, unsigned int dim2);
	void PrintMatrix() override;
	void FindOftenElement() override;
};