#pragma once
#include <iostream>
#include "BaseMatrix.h"

using namespace std;

class IntegerMatrix : public BaseMatrix
{
private:
	int** matr;
	int columns;
	int rows;
public:
	IntegerMatrix();
	~IntegerMatrix();
	IntegerMatrix(int columns, int rows, int** matr);
	int** ArrayGenerator(unsigned int dim1, unsigned int dim2);
	void PrintMatrix() override;
	void FindOftenElement() override;
};
