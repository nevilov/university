#include "CharMatrix.h"
using namespace std;

CharMatrix::CharMatrix() {
	matr = NULL;
}

CharMatrix::CharMatrix(int rows, int columns, char** matr) {
	this->matr = ArrayGenerator(rows, columns);
	this->rows = rows;
	this->columns = columns;

	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < columns; ++j) {
			this->matr[i][j] = matr[i][j];
		}
	}

	this->matr = matr;
}

char** CharMatrix::ArrayGenerator(unsigned int dim1, unsigned int dim2) {
	char** ptrary = new char* [dim1];
	for (int i = 0; i < dim1; i++) {
		ptrary[i] = new char[dim2];
	}

	for (int i = 0; i < dim1; i++)
	{
		for (int j = 0; j < dim2; j++)
		{
			ptrary[i][j] = (((i + j) + 1) * 100) - i;
		}
	}

	return ptrary;
}


void CharMatrix::PrintMatrix()
{
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < columns; ++j) {
			cout << this->matr[i][j] << "\t";
		}
		cout << endl;
	}
}

CharMatrix::~CharMatrix() {
	return;
}

void CharMatrix::FindOftenElement() {
	char oftenElement = 'f';
	int hightReptitions = 0;

	for (int i = 0; i < rows; ++i) {
		for (int j = 0, numberOfRepetitions = 0; j < columns; ++j) {
			int comp = matr[i][j];
			for (int k = i; k < rows; ++k) {
				for (int m = j; m < columns; ++m) {
					if (comp == matr[k][m]) {
						numberOfRepetitions++;
					}
				}
			}

			if (numberOfRepetitions > hightReptitions) {
				hightReptitions = numberOfRepetitions;
				oftenElement = comp;
			}
			numberOfRepetitions = 0;
		}
	}
	cout << "����� ������ ������� " << oftenElement << " ���������� ���������� " << hightReptitions << endl;
}