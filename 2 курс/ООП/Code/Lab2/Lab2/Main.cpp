#include <iostream>
#include "BaseMatrix.h"
#include "IntegerMatrix.h"
#include "CharMatrix.h"
#include "DoubleMatrix.h"
#include <cstdlib>
#include <iomanip>
using namespace std;

int** arrayIntGenerator(unsigned int dim1, unsigned int dim2);
char** arrayCharGenerator(unsigned int dim1, unsigned int dim2);
double** arrayDoubleGenerator(unsigned int dim1, unsigned int dim2);

int main() {
    setlocale(LC_ALL, "Ru");

	BaseMatrix* matr = new IntegerMatrix(3, 5, arrayIntGenerator(3, 5));
    cout << "����������� ������������� �������" << endl;
    matr->PrintMatrix();
    matr->FindOftenElement();

    BaseMatrix* charMatrix = new CharMatrix(3, 5, arrayCharGenerator(3, 5));
    cout << "����������� ���������� �������" << endl;
    charMatrix->PrintMatrix();
    charMatrix->FindOftenElement();

    BaseMatrix* doubleMatrix = new DoubleMatrix(3, 5, arrayDoubleGenerator(3, 5));
    cout << "����������� ���������� �������" << endl;
    doubleMatrix->PrintMatrix();
    doubleMatrix->FindOftenElement();
}

int** arrayIntGenerator(unsigned int dim1, unsigned int dim2) {
    int** ptrary = new int* [dim1];
    for (int i = 0; i < dim1; i++) {
        ptrary[i] = new int[dim2];
    }

    for (int i = 0; i < dim1; i++)
    {
        for (int j = 0; j < dim2; j++)
        {
            ptrary[i][j] = (((i * j) + 1) * 10)- j;
        }
    }

    return ptrary;
}

char** arrayCharGenerator(unsigned int dim1, unsigned int dim2) {
    char** ptrary = new char* [dim1];
    for (int i = 0; i < dim1; i++) {
        ptrary[i] = new char[dim2];
    }

    for (int i = 0; i < dim1; i++)
    {
        for (int j = 0; j < dim2; j++)
        {
            ptrary[i][j] = (((i * 5) + 1) * 10) + j*5 + 5;
        }
    }
    ptrary[0][0] = 'P';

    return ptrary;
}

double** arrayDoubleGenerator(unsigned int dim1, unsigned int dim2) {
    double** ptrary = new double* [dim1];
    for (int i = 0; i < dim1; i++) {
        ptrary[i] = new double[dim2];
    }

    for (int i = 0; i < dim1; i++)
    {
        for (int j = 0; j < dim2; j++)
        {
            ptrary[i][j] = ((((i * 5) + 1) * 10) + j * 5 + 5)/3/1.5;
        }
    }

    return ptrary;
}