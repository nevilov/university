#pragma once
#include <iostream>
using namespace std;

class BaseMatrix {
public:
	virtual void FindOftenElement();
	virtual void PrintMatrix();
};