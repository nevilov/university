#include "IntegerMatrix.h"
using namespace std;

IntegerMatrix::IntegerMatrix(){
	matr = NULL;
}

IntegerMatrix::IntegerMatrix(int rows, int columns, int** matr) {
	this->matr = ArrayGenerator(rows, columns);
	this->rows = rows;
	this->columns = columns;

	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < columns; ++j) {
			this->matr[i][j] = matr[i][j];
		}
	}

	this->matr = matr;
}

int** IntegerMatrix::ArrayGenerator(unsigned int dim1, unsigned int dim2) {
	int** ptrary = new int* [dim1];
	for (int i = 0; i < dim1; i++) {
		ptrary[i] = new int[dim2];
	}

	for (int i = 0; i < dim1; i++)
	{
		for (int j = 0; j < dim2; j++)
		{
			ptrary[i][j] = (((i + j) + 1) * 100) - i;
		}
	}

	return ptrary;
}


void IntegerMatrix::PrintMatrix()
{
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < columns; ++j) {
			cout << this->matr[i][j] << "\t";
		}
		cout << endl;
	}
}

IntegerMatrix::~IntegerMatrix(){
	return;
}

void IntegerMatrix::FindOftenElement() {
	int oftenElement = INT_MIN;
	int hightReptitions = 0;

	for (int i = 0; i < rows; ++i) {
		for (int j = 0, numberOfRepetitions = 0; j < columns; ++j) {
			int comp = matr[i][j];
			for (int k = i; k < rows; ++k) {
				for (int m = j; m < columns; ++m) {
					if (comp == matr[k][m]) {
						numberOfRepetitions++;
					}
				}
			}

			if (numberOfRepetitions > hightReptitions) {
				hightReptitions = numberOfRepetitions;
				oftenElement = comp;
			}
			numberOfRepetitions = 0;
		}
	}
	cout << "����� ������ ������� "<< oftenElement <<" ���������� ���������� "<< hightReptitions <<endl;
}