#pragma once
#include <iostream>
#include "BaseMatrix.h"

using namespace std;

class CharMatrix : public BaseMatrix
{
private:
	char** matr;
	int columns;
	int rows;
public:
	CharMatrix();
	~CharMatrix();
	CharMatrix(int columns, int rows, char** matr);
	char** ArrayGenerator(unsigned int dim1, unsigned int dim2);
	void PrintMatrix() override;
	void FindOftenElement() override;
};
