#pragma once
#include <iostream>
using namespace std;

class Fuel {
protected:
	string type;
	double weight;
public:
	Fuel();
	Fuel(string type, double weight);
	~Fuel();
	void PrintFuelInformation();
};