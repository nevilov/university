#pragma once
#include <iostream>
#include "Fuel.h"
#include "Oxidant.h"

class Flint : public Fuel, public Oxidant {
public:
	Flint(string fuelType, double fuelWeight, string oxidantType,  double oxidantVolume);
};