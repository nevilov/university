#include "Flint.h"
#include "Oxidant.h"
#include "Fuel.h"
#include <iostream>
#include <Windows.h>
using namespace std;

int main() {
	setlocale(LC_ALL, "Ru");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	string fuelType, oxidantType;
	double fuelWeight, oxidantVolume;

	cout << "������� ��� �������" << endl;
	cin >> fuelType;
	cout << "������� ��� �������" << endl;
	cin >> fuelWeight;

	cout << "������� ��� ����������" << endl;
	cin >> oxidantType;
	cout << "������� ��� �������" << endl;
	cin >> oxidantVolume;

	cout << "������� ������ �������" << endl;
	Fuel* fuel = new Fuel(fuelType, fuelWeight);
	fuel->PrintFuelInformation();

	cout << "������� ������ ����������" << endl;
	Oxidant* oxidant = new Oxidant(oxidantType, oxidantVolume);
	oxidant->PrintOxidantInformation();

	cout << "������� ����� ������, ������� ��������� ������� � ����������" << endl;
	Flint* flint = new Flint(fuelType, fuelWeight, oxidantType, oxidantVolume);
	flint->PrintFuelInformation();
	flint->PrintOxidantInformation();
}