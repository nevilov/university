#pragma once
#include <iostream>
using namespace std;

class Oxidant {
protected:
	string type;
	double volume;
public:
	Oxidant();
	Oxidant(string type, double volume);
	~Oxidant();
	void PrintOxidantInformation();
};