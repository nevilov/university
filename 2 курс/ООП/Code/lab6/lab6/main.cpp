#include "ShiftGeneric.cpp"
#include <stdlib.h>

int main() {
	setlocale(LC_ALL, "Ru");

	int* intArray = new int[10];
	double* doubleArray = new double[10];
	char* charArray = new char[10];

	for (int i = 0; i < 10; ++i) {
		intArray[i] = rand() % 10;
		doubleArray[i] = rand() % 50 / 2;
		charArray[i] = rand() % 10;
	}

	ShiftGeneric<int> intShiftGeneric(intArray, 10);
	ShiftGeneric<double> doubleShiftGeneric(doubleArray, 10);
	ShiftGeneric<char> charShiftGeneric(charArray, 10);

	cout << "��������� ���������� ������� � ����� int" << endl;
	intShiftGeneric.Print();
	intShiftGeneric.ShiftElements(10);
	intShiftGeneric.Print();

	cout << "��������� ���������� ������� � ����� double" << endl;
	doubleShiftGeneric.Print();
	doubleShiftGeneric.ShiftElements(10);
	doubleShiftGeneric.Print();

	cout << "��������� ���������� ������� � ����� char" << endl;
	charShiftGeneric.Print();
	charShiftGeneric.ShiftElements(10);
	charShiftGeneric.Print();
}