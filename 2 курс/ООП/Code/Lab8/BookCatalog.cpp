#include "BookCatalog.h"
#include <string>

BookCatalog::BookCatalog() {
}

BookCatalog::BookCatalog(multiset<BookCatalogItem> data) {
	_data = data;
}

void BookCatalog::AddBook(BookCatalogItem item) {
	_data.insert(item);
}

void BookCatalog::RemoveBookByName(string name) {
	multiset<BookCatalogItem> ::iterator it = find_if(_data.begin(), _data.end(),
		[&](BookCatalogItem const& p) { return p.name == name; });
	if (it != _data.end()) {
		_data.erase(it);
	}
}

BookCatalog BookCatalog::SearchBookByName(string name) {
	BookCatalog catalog;
	multiset<BookCatalogItem> ::iterator it = _data.begin();
	while (it != _data.end()) {
		if ((*it).name.compare(name) == 0) {
			catalog.AddBook(*it);
		}
		it++;
	}

	return catalog;
}

BookCatalog BookCatalog::GetBooksByAuthorName(string name) {
	BookCatalog catalog;
	multiset<BookCatalogItem> ::iterator it = _data.begin();
 	while (it != _data.end()) {
		if ((*it).authorName.compare(name) == 0) {
			catalog.AddBook(*it);
		}
		it++;
	}

	return catalog;
}

void BookCatalog::PrintCatalog() {
	multiset <BookCatalogItem> ::iterator it = _data.begin();
	 
	for (int i = 1; it != _data.end(); i++, it++) {
		cout << (*it).authorName << " | "
			<< (*it).name << " | "
			<< (*it).branchNumber << " | "
			<< (*it).shelfNumber << " | "
			<< (*it).standNumber << " | " << endl;;
	}
}