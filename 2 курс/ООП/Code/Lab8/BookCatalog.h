#pragma once
#include <set>
#include "BookCatalogItem.h"
#include <iostream>
#include <string>

using namespace std;

class BookCatalog {
	multiset<BookCatalogItem> _data;

public:
	BookCatalog();
	BookCatalog(multiset<BookCatalogItem> _data);
	void AddBook(BookCatalogItem book);
	void RemoveBookByName(string name);
	BookCatalog GetBooksByAuthorName(string authorName);
	void PrintCatalog();
	BookCatalog SearchBookByName(string name);
};