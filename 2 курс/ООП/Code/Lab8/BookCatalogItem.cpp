#pragma once
#include "BookCatalogItem.h"

BookCatalogItem::BookCatalogItem(string authorName, string name, string branchNumber, string standNumber, string shelfNumber) {
	this->authorName = authorName;
	this->name = name;
	this->branchNumber = branchNumber;
	this->standNumber = standNumber;
	this->shelfNumber = shelfNumber;
}

bool BookCatalogItem::operator<(const BookCatalogItem& rhs) const noexcept
{
	return this->authorName < rhs.authorName;
}

BookCatalogItem::BookCatalogItem() {

}
