#include <iostream>
#include "BookCatalog.h"
#include "BookCatalogItem.h"
#include <Windows.h>
using namespace std;


int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	setlocale(LC_ALL, "Ru");

	BookCatalog catalog;	
	catalog.AddBook(BookCatalogItem("Pushkin", "����", "1a", "2b", "6a"));
	catalog.AddBook(BookCatalogItem("Pushkin", "����������� �����", "2a", "2b", "6a"));
	catalog.AddBook(BookCatalogItem("Pushkin", "Pieces", "3a", "2b", "6a"));

	while (true) {
		cout << "����� ��������" << endl;
		cout << "1 - �������� �������" << endl;
		cout << "2 - ������� �������" << endl;
		cout << "3 - �������� ��� ����� ������������� ������" << endl;
		cout << "4 - ����� ����� �� �����" << endl;
		cout << "5 - ������� ���� �������" << endl;
		cout << "0 - �����" << endl;
		char op;
		cin >> op;

		if (op == '0') {
			break;
		}

		switch (op)
		{
			case '1': {
				BookCatalogItem item;
				cout << "��� ������" << endl;
				cin >> item.authorName;
				cout << "�������� ������������" << endl;
				cin >> item.name;
				cout << "����� ������" << endl;
				cin >> item.branchNumber;
				cout << "����� ������" << endl;
				cin >> item.standNumber;
				cout << "����� �����" << endl;
				cin >> item.shelfNumber;
				break;
			}
			case '2': {
				cout << "������� �������� ����� ������� ���������� �������" << endl;
				string name;
				cin >> name;
				catalog.RemoveBookByName(name);
				break;
			}
			case '3': {
				cout << "������� ��� ������" << endl;
				string authorName;
				cin >> authorName;
				catalog
					.GetBooksByAuthorName(authorName)
					.PrintCatalog();

				break;
			}
			case '4': {
				cout << "������� ��� �����" << endl;
				string bookName;
				cin >> bookName;
				catalog
					.SearchBookByName(bookName)
					.PrintCatalog();
				break;
			}
			case '5': {
				catalog.PrintCatalog();
			}
		default: 
			break;
		}
	}
}
