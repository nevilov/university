#pragma once
#include <iostream>
using namespace std;

class BookCatalogItem {
public:
	string authorName;
	string name;
	string branchNumber;
	string standNumber;
	string shelfNumber;
	BookCatalogItem(string authorName, string name, string branchNumber, string standNumber, string shelfNumber);
	BookCatalogItem();
	bool operator<(const BookCatalogItem& rhs) const noexcept;
};