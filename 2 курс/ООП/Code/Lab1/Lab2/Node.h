#pragma once
#include <iostream>
#include "Student.h"
using namespace std;
#ifndef Node_H
#define Node_H

class Node
{
public:
	Node* next;
	Student student;

	Node();
};

#endif