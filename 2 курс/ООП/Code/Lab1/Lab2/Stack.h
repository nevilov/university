#pragma once
#include <iostream>
#include "Student.h"
#include "Node.h"
using namespace std;
#ifndef Stack_H
#define Stack_H

class Stack
{
private:
	Node* top;

public:
	Stack();
	Stack(Student std);
	void AddStudent(Student student);
	Student Pop();
	void Display();
	Stack FindStudentWhereAvgBallGreaterThanFour();
	int Count();

};

#endif