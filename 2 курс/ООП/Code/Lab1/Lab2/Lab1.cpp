﻿#include <iostream>
#include "Stack.h"
#include "Student.h"

int main()
{
	setlocale(LC_ALL, "ru");

	int estimation1[5] = { 5, 5, 5, 5, 5 };
	int estimation2[5] = { 4, 5, 4, 5, 5 };
	int estimation3[5] = { 5, 3, 5, 5, 4 };
	int estimation4[5] = { 4, 5, 4, 5, 4 };

	Student student1("Ваня", estimation1);
	Student student2("Даня", estimation2);
	Student student3("Саня", estimation3);
	Student student4("Никита", estimation4);

	cout << "Создание стека и заполнение его объектами студентов" << endl;
	Stack* stack = new Stack(student1);
	stack->AddStudent(student2);
	stack->AddStudent(student3);
	stack->AddStudent(student4);
	
	cout << "Распечатаем стек" << endl;
	stack->Display();

	cout << "Извлечем 1 объект из стека" << endl;
	stack->Pop();

	cout << "Распечатаем стек" << endl;
	stack->Display();

	cout << "Найдем и распечатаем студентов, у которых бал выше 4" << endl;
	Stack stackWithStudentsWhereAvgBallGreaterThanFour = stack->FindStudentWhereAvgBallGreaterThanFour();
	stackWithStudentsWhereAvgBallGreaterThanFour.Display();
}

