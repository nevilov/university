#include "Stack.h"
using namespace std;


	Stack::Stack()
	{
		top = NULL;
	}

	Stack::Stack(Student std)
	{
		top = new Node;
		top->student = std;
	}

	void Stack::AddStudent(Student_H::Student student)
	{
		Node* newElement = new Node();
		newElement->student = student;
		newElement->next = top;
		top = newElement;
	}

	Student Stack::Pop()
	{
		if (top != NULL)
		{
			Student studentOnTop = top->student;
			Node* newTopElement = top->next;
			delete top;
			top = newTopElement;
			cout << "������� ������� ������� " << studentOnTop.name << endl;
			return studentOnTop;
		}
	}

	void Stack::Display()
	{
		Node* topElement = top;

		if (top != NULL) {
			while (true) 
			{
				cout << "������� " << topElement->student.name << " �����: ";

				for (int i = 0; i < 5; ++i) {
					cout << topElement->student.estimation[i] << " ";
				}
				cout << endl;

				if (topElement->next == NULL) {
					break;
				}
				topElement = topElement->next;
			}
		}
		else {
			cout << "�������� ����������, ��� ��������� � �����" << endl;
		}
	}

	Stack Stack::FindStudentWhereAvgBallGreaterThanFour() 
	{
		Node* topElement = top;
		Stack newStack;
		double avgEstimation = 0;
		int studentsCounterWhereAvgBallGreaterThanFour = 0;

		while (true)
		{
			Student student = topElement->student;
			double avgEstimation = 0;
			for (int i = 0; i < 5; ++i) {
				avgEstimation += student.estimation[i];
			}
			avgEstimation /= 5;

			if (avgEstimation >= 4) {
				newStack.AddStudent(student);
				++studentsCounterWhereAvgBallGreaterThanFour;
			}

			if (topElement->next == NULL) {
				break;
			}

			topElement = topElement->next;
		}

		return newStack;
	}

	int Stack::Count()
	{
		int count = 0;
		Node* topElement = top;
		while (true) {
			++count;
			if (topElement->next == NULL) {
				break;
			}
			topElement = topElement->next;
		}

		return count;
	}