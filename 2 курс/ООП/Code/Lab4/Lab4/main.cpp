#include <iostream>
#include "Vector.h"
#include "Number.h"

using namespace std;

int main() {
	setlocale(0, "Ru");
	int* elements = new int[5];
	for (int i = 0; i < 5; ++i) {
		elements[i] = i;
	}
	Vector vector(elements, 5);
	Number num(2);
	cout << "����� ��������� ������� " << num.GetNumber() << endl;
	FindElementsHigherThanNumber(vector, num)->PrintElements();
}

Vector* FindElementsHigherThanNumber(Vector vector, Number number) {
	int count = vector.count;
	int* elements = vector.elements;
	int* elementsHigherThanNumber = new int();
	int higherCounter = 0;
	for (int i = 0; i < count; ++i) {
		if (elements[i] > number.number) {
			elementsHigherThanNumber[higherCounter] = elements[i];
			higherCounter++;
		}
	}

	Vector* sortedVector = new Vector(elementsHigherThanNumber, higherCounter);

	return sortedVector;
}