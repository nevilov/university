#pragma once
#include "Number.h"
#include <iostream>
using namespace std;
#ifndef Vector_HEADER
#define Vector_HEADER

class Number;

class Vector
{
	int* elements = new int();
	int count;

public:
	Vector(int* elements, int count);
	~Vector();
	int GetCout();
	int* GetElements();
	void PrintElements();
	friend Vector* FindElementsHigherThanNumber(Vector vector, Number number);
};

#endif