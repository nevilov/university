#pragma once
#include "Vector.h";
#ifndef Number_HEADER
#define Number_HEADER

class Vector;

class Number {
	int number;
public:
	Number(int number);
	~Number();
	int GetNumber();
	friend Vector* FindElementsHigherThanNumber(Vector vector, Number number);
};
#endif