﻿using System.Collections.Generic;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnostics.Windows.Configs;

namespace DotNetPerformance
{
    [MemoryDiagnoser]
    [RyuJitX64Job, LegacyJitX86Job]
    public class Summator
    {
        private List<int> _list = new List<int>
        {
            2,23,3,1,3,124,214,34,352,523,234,4,652,326,2436,45326,14,46324563,4525713
        };

        [Benchmark]
        public int SumListElementByForeachLoop()
        {
            int sum = default;
            foreach (var elem in _list)
            {
                sum += elem;
            }

            return sum;
        }

        [Benchmark]
        public int SumListElementByCollectionLibraryForeachLoop()
        {
            int sum = default;
            _list.ForEach(a => sum += a);
            return sum;
        }

        [Benchmark]
        public int SumListElementByForLoop()
        {
            int sum = default;
            for (int i = 0; i < _list.Count; ++i)
            {
                sum += _list[i];
            }
            return sum;
        }
    }
}