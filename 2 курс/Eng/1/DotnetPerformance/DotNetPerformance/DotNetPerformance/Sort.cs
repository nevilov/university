﻿using System;
using System.Collections.Generic;
using BenchmarkDotNet.Attributes;
using Iced.Intel;

namespace DotNetPerformance
{
    [MemoryDiagnoser]
    [RyuJitX64Job, LegacyJitX86Job]
    public class Sort
    {
        private static int[] _array = new int[]
        {
            2,23,3,1,3,124,214,34,352,523,234,4,652,326,2436,45326,14,46324563,4525713
        };

        [Benchmark]
        public void SortArrayClassBehavior() => Array.Sort(_array);

        [Benchmark]
        public void SortByHoarAlgorithm()
        {
            qSort(0, _array.Length-1);
        }

        private void qSort(int start, int end)
        {
            if (start < end)
            {
                int temp = partition(_array, start, end);

                qSort(start, temp - 1);
                qSort(temp, end);
            }
        }

        private int partition(int[] A, int start, int end)
        {
            int pivot = A[(start + end) / 2];
            int i = start;
            int j = end;

            while (i <= j)
            {
                while (A[i] < pivot)
                    i++;
                while (A[j] > pivot)
                    j--;
                if (i <= j)
                {
                    int temp = A[i];
                    A[i] = A[j];
                    A[j] = temp;

                    i++;
                    j--;
                }
            }

            return i;
        }
    }
}
