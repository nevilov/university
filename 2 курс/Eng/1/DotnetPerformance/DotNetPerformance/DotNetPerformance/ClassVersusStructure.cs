﻿using System;
using System.Collections.Generic;
using System.Linq;
using BenchmarkDotNet.Attributes;

namespace DotNetPerformance
{
    [MemoryDiagnoser]
    [RyuJitX64Job, LegacyJitX86Job]
    public class ClassVersusStructure
    {
        [Benchmark]
        public void ProcessingJournalistClass()
        {
            JournalistClass journalist = new JournalistClass(
                name: "Andrew Inkensky Second",
                salary: 1000,
                Gender.Male,
                code: 1,
                subCompany: "GoldCaso");

            Console.WriteLine(journalist.Name);
        }

        [Benchmark]
        public void ProcessingJournalistStructure()
        {
            JournalistStruct journalist = new JournalistStruct(
                name: "Angelina Dipsky",
                salary: 1000,
                Gender.Female,
                code: 2,
                subCompany: "BBC");

            Console.WriteLine(journalist.Name);
        }

    }

    public class JournalistClass
    {
        public string Name { get; private set; }
        public decimal Salary { get; private set; }
        public Gender Gender { get; private set; }
        public int Code { get; private set; }
        public string SubCompany { get;private set; }

        public JournalistClass(string name
            , decimal salary
            , Gender gender
            , int code
            , string subCompany)
        {
            Name = name;
            Salary = salary;
            Gender = gender;
            Code = code;
            SubCompany = subCompany;
        }
    }

    public struct JournalistStruct
    {
        public string Name { get; private set; }
        public decimal Salary { get; private set; }
        public Gender Gender { get; private set; }
        public int Code { get; private set; }
        public string SubCompany { get; private set; }

        public JournalistStruct(string name
            , decimal salary
            , Gender gender
            , int code
            , string subCompany)
        {
            Name = name;
            Salary = salary;
            Gender = gender;
            Code = code;
            SubCompany = subCompany;
        }
    }

    public enum Gender
    {
        Male, Female
    }
}