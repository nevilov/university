﻿using System;
using System.Text;
using BenchmarkDotNet.Attributes;

namespace DotNetPerformance
{
    [MemoryDiagnoser]
    [RyuJitX64Job, LegacyJitX86Job]
    public class Concat
    {
        public int VariableToConcatenate { get; set; } = 10;
        public string FirstVariableToConcatenate { get; set; } = "Messi had";
        public string SecondVariableToConcatenate { get; set; } = "in Barcelona";

        [Benchmark]
        public string ConcatByConcatenation()
        {
            var a = FirstVariableToConcatenate + " " + VariableToConcatenate + " " + SecondVariableToConcatenate;
            return a;
        }

        [Benchmark]
        public string ConcatByInterpolation()
        {
            return $"{FirstVariableToConcatenate} {VariableToConcatenate} {SecondVariableToConcatenate}";
        }

        [Benchmark]
        public string ConcatByStringBuilder()
        {
            StringBuilder builder = new();
            builder
                .Append(FirstVariableToConcatenate)
                .Append(" ")
                .Append(VariableToConcatenate)
                .Append(" ")
                .Append(SecondVariableToConcatenate);

            return builder.ToString();
        }

        [Benchmark]
        public string ConcatByCompositeFormat()
        {
            return String.Format("{0} {1} {2}", FirstVariableToConcatenate, VariableToConcatenate, SecondVariableToConcatenate);
        }
    }
}