package Introduction

fun toJSON(collection: Collection<Int>): String
{
    var sb: StringBuilder = StringBuilder();
    sb.append("[");
    var iterator: Iterator<Int> = collection.iterator();
    while (iterator.hasNext()) {
        var element = iterator.next();
        sb.append(element);
        if (iterator.hasNext()) {
            sb.append(", ");
        }
    }
    sb.append("]");
    return sb.toString();
}


fun main(args: Array<String>) {
    println(joinOptions(ArrayList("a", "b", "c")))
}

fun joinOptions(
    list: ArrayList<String>,
    separator: String = ", "
): String{
    var sb = StringBuilder();
    sb.append('[')
    var iterator = list.iterator();
    while (iterator.hasNext()){
        sb.append(iterator.next());
        if(iterator.hasNext()){
            sb.append(", ");
        }
    }
    sb.append(']')

    return sb.toString();
}

